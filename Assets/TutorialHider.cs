﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHider : MonoBehaviour {

    void Start()
    {
        if (FindObjectOfType<TutorialPopup>() == null)
        {
            gameObject.SetActive(false);
        }
    }
}
