﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EssenceBox : MonoBehaviour {
    public Text essenceText;

	void Update () {
        essenceText.text =Mathf.RoundToInt(GameManager.instance.playerEssence).ToString();
	}
}
