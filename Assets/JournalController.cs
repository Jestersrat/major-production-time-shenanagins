﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalController : MonoBehaviour {

    public GameObject textHolder;
    
    public void SwapStory(GameObject target)
    {
        foreach (Transform child in textHolder.transform)
        {
            child.gameObject.SetActive(false);
        }
        target.SetActive(true);
    }

}
