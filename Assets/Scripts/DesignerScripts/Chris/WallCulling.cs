﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCulling : MonoBehaviour
{
    public bool objectCulled = false;
    public GameObject[] objectToCull;
    public float zFlipPoint=100;
    private float lastZ;
    private GameObject mainCamera;

    void Start()
    {
        mainCamera = Camera.main.gameObject;
        lastZ = mainCamera.transform.position.z;
    }


    void Update()
    {
        if (!objectCulled)
        {
            if (mainCamera.transform.position.z > zFlipPoint)
            {
                CullObjects();
            }
        }
        else if (objectCulled)
        {
            if (mainCamera.transform.position.z < zFlipPoint)
            {
                ShowObjects();
            }
        }
    }

    void CullObjects()
    {
        for (int i = 0; i < objectToCull.Length; i++)
        {
            objectToCull[i].SetActive(false);
        }
        objectCulled = true;
    }

    void ShowObjects()
    {
        for (int i = 0; i < objectToCull.Length; i++)
        {
            objectToCull[i].SetActive(true);
        }
        objectCulled = false;
    }
}          	

