﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorSwitch : MonoBehaviour
{
    public bool doorOpen = false;
    public GameObject theDoor;
    public GameObject doorSwitch;

    void OnTriggerStay(Collider other)
    {
        if (doorOpen == false && other.tag == "Player")
        {
            doorOpen = true;
            theDoor.SetActive(false);
            if (doorSwitch != null)
            {
                doorSwitch.SetActive(false);
            }
        }
    }

}
