﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayToNight : MonoBehaviour
{

    public bool lerping=false;
    public float lerpRate = 1;
    public Color targetColor = new Color(0f, 0f, .5f);
    public Color startColor;
    public float timer=0;

    void Start()
    {
        startColor = GetComponent<Light>().color;
    }
    void OnTriggerEnter(Collider other)
    {
        lerping = true;
    }           

    void Update()
    {
        if (lerping)
        {
            timer += Time.deltaTime * lerpRate;
            GetComponent<Light>().color = Color.Lerp(startColor, targetColor,timer);
            if (timer >= 1)
            {
                lerping = false;
                this.enabled = false;
            }
        }
    }
}




