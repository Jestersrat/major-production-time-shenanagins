﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHighlighter : MonoBehaviour {
    public bool mouseOver = false;
    //public float hoverTime = 0.5f;
    public bool highlighted = false;
   // public float timer = 0;
    private string defaultShader;
    private Shader shader;
    void Start()
    {
        shader = Shader.Find("Outlined/Silhouetted Diffuse");
        if (GetComponent<Renderer>() != null)
        {
            defaultShader = GetComponent<Renderer>().material.shader.name;
        }
        else
        {
            defaultShader = GetComponentInChildren<Renderer>().material.shader.name;
        }
    }

    void Update()
    {
        if (!highlighted)
        {
            if (mouseOver == true)
            {
                highlighted = true;
                if (GetComponent<Renderer>() != null)
                {
                    GetComponent<Renderer>().material.shader = shader;
                }
                else
                {
                    GetComponentInChildren<Renderer>().material.shader = shader;
                }
            }
        }
    }

    private void OnMouseEnter()
    {
        mouseOver = true;
    }

    private void OnMouseExit()
    {
        mouseOver = false;
        if (highlighted)
        {
            if (GetComponent<Renderer>() != null)
            {
                GetComponent<Renderer>().material.shader = Shader.Find(defaultShader);
            }
            else
            {
                GetComponentInChildren<Renderer>().material.shader = Shader.Find(defaultShader);
            }
            highlighted = false;
        }

    }
}
