﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//// Just to make sure all objectives have required implementations
//public interface ObjectiveFunctions
//{
//    void SetUp();

//    bool CheckComplete();
//    string QuestText();

//    /// <summary>
//    /// Cleans up objective and stops tracking progress
//    /// </summary>
//    void CleanUpObjective();

//    void UpdateObjectiveText(Text text);
//}

public abstract class ObjectiveBase : ScriptableObject//, ObjectiveFunctions
{
    public bool completed = false;
    public string objectiveName;
    public string objectiveDescription;
    [Tooltip("Displays arrow to objective")]
    public bool showArrow = false;
    public bool removeOnReveal = true;

    public abstract void SetUp();
    //{
    //    Debug.LogError(name + "failed to set up");
    //}

    public abstract bool CheckComplete();
    //{
    //    Debug.Log("You shouldnt see this");
    //    return true;
    //}
    public abstract string QuestText();
    //{
    //    Debug.Log("You shouldnt see this either");
    //    return objectiveDescription;
    //}

    /// <summary>
    /// Cleans up objective and stops tracking progress
    /// </summary>
    public abstract void CleanUpObjective();
    //{
    //    Debug.LogError(name + "failed to clean up");
    //}

    public abstract void UpdateObjectiveText(Text text);
    //{
    //    Debug.LogError(name + "doesn't have an implementation for UpdateObjectiveText");
    //}

    public virtual ObjectiveBase MakeCopy()
    {
        var copy = Instantiate(this);

        copy.name = this.name;

        return copy;
    }


}



//public object[][] objectives;
//public bool killTargets;
//public Unit[] targets;
//public bool escape;
//public bool survive;
//public int surviveLength;
//[Tooltip("If the player must reach a point or points to complete the objective")]
//public bool reachPoints;
//public int pointsReached;
//public int pointsNeeded;
//[Tooltip("If there are more than 1 objective to the level (eg, get an object, escape the level")]
//public bool objectiveChain;
//public int completedObjectives;
//public int chainLength;
//[Tooltip("If the objective is to gather a set amount of essense")]
//public bool gatherEssense;
//public int essenceTarget;