﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "BasicReward", menuName = "Mission Objectives/Rewards/Basic", order = 1)]
public abstract class RewardBase : ScriptableObject
{
    //public int expReward = 0;
    //public int essenceReward = 0;

    public abstract void ApplyRewards();
    //{
    //    GameManager.instance.AddEssence(essenceReward, Vector3.zero);

    //    foreach(var unit in GameManager.instance.missionUnits)
    //    {
    //        unit.level.currentExp += expReward;
    //    }
    //}
}
