﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CreateAssetMenu(fileName = "R Dialogue", menuName = "Mission Objectives/Rewards/Dialogue", order = 1)]
public class RewardDialogue : RewardBase
{
    public string dialogueBlockName;

    public override void ApplyRewards()
    {
        var flowcartObject = GameObject.FindGameObjectWithTag("Flowchart");
        
        if (flowcartObject)
        {
            var flowcart = flowcartObject.GetComponent<Flowchart>();

            flowcart.ExecuteBlock(dialogueBlockName);
        }
        else
            Debug.LogError("Missing flowchart from " + name);
    }
}
