﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum EndMissionType
{
    Restart,
    KeepChanges
}

[CreateAssetMenu(fileName = "BasicReward", menuName = "Mission Objectives/Rewards/EndLevel", order = 1)]
public class EndLevelReward : RewardBase
{
    [SerializeField]
    public EndMissionType endType;

    public override void ApplyRewards()
    {
        if(endType == EndMissionType.Restart)
        {
            // End mission without applying rewards
            MissionObjectives.instance.EndMission();
        }
        else
        {
            // End mission keeping changes and applying rewards
            MissionObjectives.instance.EndMission(true);
        }
    }
}
