﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum SetType
{
    MissionSet,
    OptionalSet
}

[CreateAssetMenu(fileName = "R Reward objective set", menuName = "Mission Objectives/Rewards/Objective Set", order = 1)]
public class RewardObjectiveSet : RewardBase
{
    public ObjectiveSet objectiveSet;

    [SerializeField]
    public SetType setType;

    [Tooltip("Replaces all existing objectives if true")]
    public bool replaceAllExisting = false;

    public override void ApplyRewards()
    {
        if (!objectiveSet)
            Debug.LogError(name + " missing objective set");

        if (replaceAllExisting)
        {
            MissionObjectives.instance.ClearAllLists();
        }

        var setCopy = objectiveSet.MakeCopy();

        if(setType == SetType.MissionSet)
        {
            MissionObjectives.instance.missionObjectives.Add(setCopy);
        }
        else
        {
            MissionObjectives.instance.optionalObjectives.Add(setCopy);
        }
    }
}
