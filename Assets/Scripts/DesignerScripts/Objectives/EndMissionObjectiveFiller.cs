﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndMissionObjectiveFiller : MonoBehaviour {
    public ObjectiveBase quest;
    public bool optional;
    public Text objectiveText;
    public Text rewardText;
    public Image checkBox;
    public Sprite complete;
    public Sprite fail;

    public void FillUI()
    {
        if (quest.completed)
        {
            checkBox.sprite = complete;
            rewardText.color = Color.green;
            rewardText.text = "Completed";
        }
        else
        {
            checkBox.sprite = fail;
            rewardText.text = "";
            rewardText.color = Color.grey;
        }
        objectiveText.text = quest.objectiveName;
        //rewardText.text=quest.rewards
    }
}
