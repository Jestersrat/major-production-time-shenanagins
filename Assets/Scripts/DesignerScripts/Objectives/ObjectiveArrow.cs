﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveArrow : MonoBehaviour
{
    public Unit unit;

    public CellInfo tile;

    public bool removeOnReveal = true;

    public static Transform arrowHolder;

    public Transform arrowParent;

    public Vector3 offSet = new Vector3();

    ObjectiveBase objective;

	// Use this for initialization
	void Start ()
    {
        if (!arrowHolder)
            arrowHolder = new GameObject("Objective Arrow Holder").transform;

        arrowParent.parent = arrowHolder;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(unit)
        {
            tile = unit.CurrentTile;
        }

        if (tile)
        {
            if (tile.fog.revealed && removeOnReveal || objective.completed)
            {
                arrowParent.transform.parent = arrowHolder;
                arrowParent.gameObject.SetActive(false);
                unit = null;
                tile = null;

            }
            else
            {
                arrowParent.transform.parent = tile.transform;
                arrowParent.transform.position = tile.transform.position + offSet;
            }
        }
	}

    public void SetUp(bool removeOnReveal, ObjectiveBase objective, float offset = 0)
    {
        this.removeOnReveal = removeOnReveal;

        this.objective = objective;

        offSet.y = offset;

        arrowParent.gameObject.SetActive(true);
    }
}
