﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CheckPointTrigger : MonoBehaviour
{
    public bool reached = false;

    public ObjectiveCheckPoint objectiveRef;

	// Use this for initialization
	void Awake ()
    {
        reached = false;

        if (objectiveRef)
        {

            objectiveRef.checkPoint = this;

            var collider = GetComponent<Collider>();

            // Sets to ignore raycast layer
            gameObject.layer = 2;

            if (!collider)
            {
                Debug.LogError("No collider component on " + name);
            }

            collider.isTrigger = true;
        }
        else
        {
            Debug.LogError(name + " missing objective ref");
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            reached = true;
            objectiveRef.CheckComplete();
        }
    }
}
