﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndMissionFiller : MonoBehaviour
{
    public GameObject unitPrefab;
    public GameObject objectivePrefab;
    public Text success;
    public Text context;
    private GameObject missionEndUnits;
    private GameObject missionEndObjectives;

    public void Awake()
    {
        missionEndUnits = transform.Find("MissionEndUnits").gameObject;
        missionEndObjectives = transform.Find("MissionEndObjectives").gameObject;
    }

    public void Start()
    {
        //gameObject.SetActive(false);
    }

    public void Initialize()
    {
        foreach (Unit unit in GameManager.instance.missionUnits)
        {
            EndMissionUnitFiller fill = Instantiate(unitPrefab, missionEndUnits.transform).GetComponent<EndMissionUnitFiller>();
            fill.unit = unit;
            fill.FillUI();
        }
        for (int i = 0; i < MissionObjectives.instance.missionObjectives.Count; i++)
        {
            foreach (ObjectiveBase objective in MissionObjectives.instance.missionObjectives[i].objectives)
            {
                EndMissionObjectiveFiller fill = Instantiate(objectivePrefab, missionEndObjectives.transform).GetComponent<EndMissionObjectiveFiller>();
                fill.quest = objective;
                fill.FillUI();
            }
        }
        for (int i = 0; i < MissionObjectives.instance.optionalObjectives.Count; i++)
        {
            foreach (ObjectiveBase objective in MissionObjectives.instance.optionalObjectives[i].objectives)
            {
                EndMissionObjectiveFiller fill = Instantiate(objectivePrefab, missionEndObjectives.transform).GetComponent<EndMissionObjectiveFiller>();
                fill.quest = objective;
                fill.optional = true;
                fill.FillUI();
            }
        }
        if (!ObjectivesCanvas.instance.applyChanges)
        {
            success.text = "<color=red>Failure</color>";
            context.text = "";
        }
        else
        {
            success.text = "Success";
        }
        ObjectivesCanvas.instance.canvas.sortingOrder = 5;
    }

    public void UpdateGameData()
    {
        //update the units stored by the game manager on game end here;
    }

}
