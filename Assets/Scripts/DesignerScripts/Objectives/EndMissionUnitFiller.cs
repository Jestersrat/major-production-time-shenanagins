﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndMissionUnitFiller : MonoBehaviour {
    public Unit unit;
    public Image portrait;
    public Text xpGain;
    public Text stats;
    public GameObject levelUp;
    public Text status;
    public Unit campUnit;

    public void FillUI()
    {
        for (int i = 0; i < GameManager.instance.units.Count; i++)
        {
            if (GameManager.instance.units[i].unitSaveID == unit.unitSaveID)
            {
                campUnit = GameManager.instance.units[i];
            }
        }
        stats.text = "";

        if (campUnit != null)
        {
            Debug.Log("Camp Unit found");
            stats.text += Mathf.RoundToInt(unit.damageDealt - campUnit.damageDealt).ToString();
            stats.text +="\n" + Mathf.RoundToInt(unit.damageTaken - campUnit.damageTaken).ToString();
            stats.text +="\n"+ (unit.kills - campUnit.kills).ToString();
            xpGain.text = (unit.level.currentExp - campUnit.level.currentExp)+"XP";
        }
        else
        {
            Debug.Log("Camp Unit not found");
            stats.text = Mathf.RoundToInt(unit.damageDealt).ToString();
            stats.text = "\n" + Mathf.RoundToInt(unit.damageTaken).ToString();
            stats.text ="\n" + (unit.kills).ToString();
            xpGain.text = (unit.level.expValue).ToString();
        }
        portrait.sprite = unit.unitIcon;
        if (unit.daysInjured > 0)
        {
            status.text = "Injured for" + unit.daysInjured + "days";
        }
        else
        {
            status.text = "";
        }
    }
}
