﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectiveEvents : UnityEvent<ObjectiveBase> { }

public class MissionObjectives : MonoBehaviour
{
    public static MissionObjectives instance;

    public List<ObjectiveSet> missionObjectives = new List<ObjectiveSet>();

    public List<ObjectiveSet> optionalObjectives = new List<ObjectiveSet>();

    public List<ObjectiveBase> persistantOptionals = new List<ObjectiveBase>();

    //List<ObjectiveSet> completedObjectives = new List<ObjectiveSet>();

    int missionIndex = 0;

    // Used to check if multiple objectives share the same id
    public HashSet<int> targetID = new HashSet<int>();

    EndMissionFiller endMission;

    [SerializeField]
    List<ObjectiveArrow> arrowPool = new List<ObjectiveArrow>();

    public GameObject objectiveArrowPrefab;

    public bool waitForPlot = false;

    void Awake()
    {
        endMission = GetComponentInChildren<EndMissionFiller>(true);
        if (instance)
            Debug.LogError("Multiple Mission Objectives detected");

        instance = this;
    }

	void Start ()
    {
        if (!objectiveArrowPrefab)
        {
            objectiveArrowPrefab = (Resources.Load("Prefabs/Objective Arrow") as GameObject);
            //objectiveArrowPrefab.SetActive(false);
        }

        for (int i = 0; i < 10; i++)
        {
            var arrow = MakeNewArrow();
            var p = arrow.arrowParent;
            p.parent = ObjectiveArrow.arrowHolder;
            p.gameObject.SetActive(false);
        }

        List<ObjectiveSet> missionsCopy = new List<ObjectiveSet>();
        foreach(var oSet in missionObjectives)
        {
            if (oSet)
                missionsCopy.Add(oSet.MakeCopy());
        }
        missionObjectives = missionsCopy;

        missionObjectives[missionIndex].SetUpSet();

        List<ObjectiveSet> optionalsCopy = new List<ObjectiveSet>();
        foreach (var oSet in optionalObjectives)
        {
            if (oSet)
            {
                var copy = oSet.MakeCopy();
                copy.SetUpSet();
                optionalsCopy.Add(copy);
            }
        }

        optionalObjectives = optionalsCopy;
    }

    void Update()
    {
        UpdateProgress();
    }

    /// <summary>
    /// 
    /// </summary>
    public void UpdateProgress()
    {
        if (missionIndex >= missionObjectives.Count)
            return;

        ObjectivesCanvas.instance.Reset();

        var text = ObjectivesCanvas.instance.GetText();

        // Stop executing if no text
        if (!text)
            return;

        text.fontStyle = FontStyle.Bold;

        text.text = missionObjectives[missionIndex].setName;

        foreach (var objective in missionObjectives[missionIndex].objectives)
        {
            text = ObjectivesCanvas.instance.GetText();
            objective.UpdateObjectiveText(text);
        }

        // Displays optional Header if there are option objectives
        if (missionObjectives[missionIndex].optionals.Count > 0 || persistantOptionals.Count > 0)
        {
            text = ObjectivesCanvas.instance.GetText();
            text.fontStyle = FontStyle.Italic;
            text.text = "Optional";
        }

        foreach (var objective in missionObjectives[missionIndex].optionals)
        {
            text = ObjectivesCanvas.instance.GetText();
            objective.UpdateObjectiveText(text);
        }
        
        foreach (var set in optionalObjectives)
        {
            foreach (var objective in set.objectives)
            {
                text = ObjectivesCanvas.instance.GetText();
                objective.UpdateObjectiveText(text);
            }
        }

        foreach (var objective in persistantOptionals)
        {
            if(objective.completed)
            {
                continue;
            }

            text = ObjectivesCanvas.instance.GetText();
            objective.UpdateObjectiveText(text);
        }
    }

    /// <summary>
    /// Returns true if set passed in is completed
    /// Also cleans up set if true
    /// </summary>
    /// <param name="oSet"></param>
    /// <returns></returns>
    public void CheckObjectives()
    {
        if (missionIndex >= missionObjectives.Count)
            missionIndex = 0;

        UpdateProgress();

        if (missionObjectives[missionIndex].CheckSetCompletion())
        {
            ++missionIndex;
            if (missionIndex < missionObjectives.Count)
            {
                missionObjectives[missionIndex].SetUpSet();
            }
            else
            {
                EndMission(true);
            }
        }

        foreach(var set in optionalObjectives)
        {
            if(set.CheckSetCompletion())
            {
                set.CleanUpSet();
            }
        }

        for (int i = persistantOptionals.Count - 1; i > 0; i--)
        {
            if (persistantOptionals[i].completed)
            {
                persistantOptionals[i].CleanUpObjective();
                persistantOptionals.RemoveAt(i);
            }
        }
    }

    public void EndMissionFungus()
    {
        waitForPlot = false;
        EndMission(true);
    }

    // Ends mission and applies rewards
    public void EndMission(bool rewards = false)
    {
        if(waitForPlot)
        {
            return;
        }
        if (rewards)
        {
            foreach (var set in missionObjectives)
            {
                // Apply rewards handles checking completion
                set.ApplyRewards();
            }

            foreach (var set in optionalObjectives)
            {
                set.ApplyRewards();
            }
        }

        ObjectivesCanvas.instance.applyChanges = rewards;

        //Debug.Log("trying to apply rewards");
        if (endMission!=null /*&& endMission.gameObject*/)
        {
            endMission.gameObject.SetActive(true);
            endMission.Initialize();
        }
    }

    public void ClearAllLists()
    {
        missionIndex = 0;

        for (int i = missionObjectives.Count - 1; i > 0; i--)
        {
            missionObjectives[i].CleanUpSet(true);
            missionObjectives.RemoveAt(i);
        }

        for (int i = optionalObjectives.Count - 1; i > 0; i--)
        {
            optionalObjectives[i].CleanUpSet(true);
            optionalObjectives.RemoveAt(i);
        }

        for (int i = persistantOptionals.Count - 1; i > 0; i--)
        {
            persistantOptionals.RemoveAt(i);
        }
    }

    public ObjectiveArrow MakeNewArrow()
    {
        var newArrow = Instantiate(objectiveArrowPrefab);

        var arrow = newArrow.GetComponentInChildren<ObjectiveArrow>();

        arrowPool.Add(arrow);

        return arrow;
    }

    public static ObjectiveArrow GetFreeArrow()
    {
        if (instance)
        {
            foreach (var arrow in instance.arrowPool)
            {
                if (!arrow.arrowParent.gameObject.activeInHierarchy)
                {
                    return arrow;
                }
            }

            return instance.MakeNewArrow();
        }

        return null;
    }
}
