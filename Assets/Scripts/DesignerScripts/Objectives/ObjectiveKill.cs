﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "KillObjective", menuName = "Mission Objectives/Objectives/Kill", order = 1)]
public class ObjectiveKill : ObjectiveBase
{
    public bool killAllRemaining = false;

    public int killRequirement = 1;

    int killCount = 0;

    [Tooltip("Zero means not in use")]
    public int ID = 0;
    
    public override void SetUp()
    {
        if(ID == 0)
            Debug.LogError("Error: " + name + " has ID of 0");

        if (!MissionObjectives.instance.targetID.Add(ID))
        {
            Debug.LogError("Error: " + name + " has the same ID as another objective");
        }

        completed = false;
        
        if(killAllRemaining)
        {
            int counter = 0;
            foreach(var enemy in TurnManager.instance.enemyUnits)
            {
                if(!enemy.isDead)
                {
                    counter++;
                }
            }

            killRequirement = counter;
        }

        UnitManager.OnUnitDeath.AddListener(OnUnitDeath);


        if(showArrow)
        {
            foreach(var unit in GameObject.FindObjectsOfType<Unit>())
            {
                if(unit.objectiveID.Contains(ID))
                {
                    var arrow = MissionObjectives.GetFreeArrow();
                    arrow.unit = unit;

                    arrow.SetUp(removeOnReveal, this, unit.headPos);
                }
            }
        }
    }

    public override void CleanUpObjective()
    {
        UnitManager.OnUnitDeath.RemoveListener(OnUnitDeath);
    }

    void OnUnitDeath(Unit e)
    {
        if(killAllRemaining && e.unitType == UnitType.Hostile)
        {
            killCount++;
        }
        else if (e.objectiveID.Contains(ID))
        {
            killCount++;
        }

        CheckComplete();
    }

    public override bool CheckComplete()
    {
        completed = (killCount >= killRequirement);

        if (completed)
        {
            MissionObjectives.instance.CheckObjectives();

            // Removes listener on meeting requirements
            // Prevents displaying of kills beyond requirement
            CleanUpObjective();
        }

        return completed;
    }

    public override string QuestText()
    {
        return objectiveDescription;
    }

    public override void UpdateObjectiveText(Text text)
    {
        text.text = "- " + objectiveName + " " + killCount + "/" + killRequirement;
    }
}
