﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Check Point", menuName = "Mission Objectives/Objectives/Check Point", order = 1)]
public class ObjectiveCheckPoint : ObjectiveBase
{
    public CheckPointTrigger checkPoint;

    public bool waitForPlot = false;

    public override bool CheckComplete()
    {
        completed = checkPoint.reached;

        if (completed)
        {
            MissionObjectives.instance.waitForPlot = waitForPlot;
            MissionObjectives.instance.CheckObjectives();
        }

        return completed;
    }

    public override void CleanUpObjective()
    {
        if(checkPoint)
        Destroy(checkPoint.gameObject);
    }

    public override string QuestText()
    {
        throw new NotImplementedException();
    }

    public override void SetUp()
    {
        if(showArrow && checkPoint)
        {
            var arrow = MissionObjectives.GetFreeArrow();
            arrow.tile = GridManager.GetClosestTile(checkPoint.transform.position);

            arrow.SetUp(removeOnReveal, this, 2);
        }
    }

    public override void UpdateObjectiveText(Text text)
    {
        text.text = "- " + objectiveName + " " + ((completed) ? 1 : 0) + "/" + 1;
    }

    public override ObjectiveBase MakeCopy()
    {
        var copy = base.MakeCopy() as ObjectiveCheckPoint;

        //var checkRef = copy as ObjectiveCheckPoint;
        //checkRef.checkPoint.objectiveRef = checkRef;
        if (checkPoint)
        {
            copy.checkPoint = checkPoint;
            copy.checkPoint.objectiveRef = copy;
        }
        else
        {
            Debug.LogError(name + " requires CheckPointTrigger gameobject in scene");
            var checkPoint = FindObjectOfType<CheckPointTrigger>();
            if (checkPoint)
            {
                checkPoint.objectiveRef = copy;
                copy.checkPoint = checkPoint;
            }
        }

        return copy;
    }
}
