﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "Protect", menuName = "Mission Objectives/Objectives/Protect", order = 1)]
public class ObjectiveProtect : ObjectiveBase
{
    public int ID = 0;

    [Header("Objective fails if units mark death count reaches fail number")]
    public int failCondition = 1;

    public RewardBase onFailTrigger = null;

    int deathCount = 0;

    public override void SetUp()
    {
        if (!MissionObjectives.instance.targetID.Add(ID))
        {
            Debug.LogError("Error: " + name + " has the same ID as another objective");
        }

        UnitManager.OnUnitDeath.AddListener(OnUnitDeath);

        if (showArrow)
        {
            foreach (var unit in GameObject.FindObjectsOfType<Unit>())
            {
                if (unit.objectiveID.Contains(ID))
                {
                    var arrow = MissionObjectives.GetFreeArrow();
                    arrow.unit = unit;
                    arrow.SetUp(removeOnReveal, this, unit.headPos);
                }
            }
        }
    }

    void OnUnitDeath(Unit e)
    {
        if (e.objectiveID.Contains(ID))
        {
            deathCount++;
        }

        CheckComplete();
    }

    public override bool CheckComplete()
    {
        completed = !(deathCount >= failCondition);

        // completed = fail
        if(deathCount >= failCondition)
        {
            if(onFailTrigger)
            {
                onFailTrigger.ApplyRewards();
            }
        }

        return completed;
    }

    public override string QuestText()
    {
        throw new NotImplementedException();
    }

    public override void CleanUpObjective()
    {
        UnitManager.OnUnitDeath.RemoveListener(OnUnitDeath);
    }

    public override void UpdateObjectiveText(Text text)
    {
        text.text = "- " + objectiveName + " " + (failCondition - deathCount) + "/" + failCondition;
    }
}
