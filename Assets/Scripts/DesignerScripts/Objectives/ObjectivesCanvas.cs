﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjectivesCanvas : MonoBehaviour
{
    public static ObjectivesCanvas instance;

    public Canvas canvas;

    public List<Text> textPool = new List<Text>();
    public int objectiveTextSize = 14;

    public bool applyChanges = false;
	// Use this for initialization
	void Start ()
    {
        if(instance)
        {
            Debug.LogError("Multiple instances of objectivesCanvas detected");
        }

        instance = this;

        if (!canvas)
            canvas = GetComponentInChildren<Canvas>();

        canvas.gameObject.SetActive(true);

        var text = GetComponentInChildren<Text>();

        if (!text)
            Debug.LogError(name + " was unable to find an active text component");

        textPool.Add(text);

        for (int i = 0; i < 10; i++)
        {
            var newText = GetNewText();
            newText.gameObject.SetActive(false);
        }

        text.gameObject.SetActive(false);
    }


    Text GetNewText()
    {
        var newText = Instantiate(textPool[0]);

        newText.name = textPool[0].name;
        //The false at the end makes it not rescale the object.
        newText.transform.SetParent(textPool[0].transform.parent,false);
        newText.fontSize = objectiveTextSize;
        textPool.Add(newText);

        return newText;
    }

    /// <summary>
    /// Clears text and disables text game objects
    /// </summary>
    public void Reset()
    {
        foreach(var text in textPool)
        {
            if(text)
            {
                text.text = "";

                text.fontStyle = FontStyle.Normal;

                text.gameObject.SetActive(false);
            }
        }

        //for (int i = textPool.Count - 1; i >= 0; i--)
        //{
        //    if (textPool[i])
        //    {
        //        textPool[i].text = "";

        //        textPool[i].fontStyle = FontStyle.Normal;

        //        textPool[i].gameObject.SetActive(false);
        //    }
        //    else
        //        textPool.RemoveAt(i);
        //}
    }

    /// <summary>
    /// Enables and returns next inactive text from pool
    /// Instantiates a new copy if required
    /// </summary>
    /// <returns></returns>
    public Text GetText()
    {
        foreach (var text in textPool)
        {
            if (text)
            {
                if (!text.IsActive())
                {
                    text.gameObject.SetActive(true);
                    return text;
                }
            }
            else
                return null;
        }

        return GetNewText();
    }

    public void EndLevel()
    {
        if (applyChanges)
        {
            foreach (Unit unit in TurnManager.instance.playerUnits)
            {
                Unit campUnit = null;
                foreach (Unit camp in GameManager.instance.units)
                {
                    if (camp.unitSaveID == unit.unitSaveID)
                    {
                        campUnit = camp;
                        break;
                    }
                }
                if (campUnit != null)
                {
                    campUnit.damageDealt = unit.damageDealt;
                    campUnit.damageTaken = unit.damageTaken;
                    campUnit.kills = unit.kills;
                    campUnit.level.currentExp += unit.level.currentExp;
                }
                else
                {
                    Debug.Log("unit is not in the campain unit management, new units arent yet supported");
                }
            }
        }
        SceneManager.LoadScene("MissionSelect");
    }
}
