﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Survive", menuName = "Mission Objectives/Objectives/Survive", order = 1)]
public class ObjectiveSurvive : ObjectiveBase
{
    [Tooltip("Number of turns to survive for")]
    public int turns = 0;

    int turnCounter = 0;

    public override bool CheckComplete()
    {
        completed = turnCounter >= turns;

        if (completed)
            MissionObjectives.instance.CheckObjectives();

        return completed;
    }

    public override void CleanUpObjective()
    {
        TurnManager.instance.OnEndTurn.RemoveListener(OnTurnChange);
    }

    public override string QuestText()
    {
        throw new NotImplementedException();
    }

    void OnTurnChange(UnitType e)
    {
        if (e == UnitType.Hostile)
        {
            turnCounter++;

            if (turnCounter >= turns)
                CheckComplete();
        }
    }

    public override void SetUp()
    {
        TurnManager.instance.OnEndTurn.AddListener(OnTurnChange);
    }

    public override void UpdateObjectiveText(Text text)
    {
        text.text = "- " + objectiveName + " " + turnCounter + "/" + turns;
    }
}
