﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Objective Set", menuName = "Mission Objectives/Objective Set", order = 1)]
public class ObjectiveSet : ScriptableObject
{
    public string setName = "";

    public List<ObjectiveBase> objectives;
    public List<ObjectiveBase> optionals;
    public bool optionalArePersistant = false;

    public List<RewardBase> rewards;
    public List<RewardBase> optionalRewards;
    public bool immediateRewards = false;
    public bool immediateoptionalRewards = false;



    /// <summary>
    /// Sets up objectives and enables tracking of completion
    /// </summary>
    public void SetUpSet()
    {

        //List<ObjectiveBase> objectivesCopy = new List<ObjectiveBase>();

        foreach (var objective in objectives)
        {
            objective.completed = false;
            objective.SetUp();

        }
        
        foreach (var objective in optionals)
        {
            objective.completed = false;
            objective.SetUp();

        }

    }

    /// <summary>
    /// Cleans up objectives and disables any further progress
    /// Adds optional objectives to persistant list if marked as persistant to continue tracking
    /// pass in true boolean to force cleanup regardless if persistant state
    /// </summary>
    public void CleanUpSet(bool forceClean = false)
    {
        foreach (var objective in objectives)
        {
            objective.CleanUpObjective();
        }

        foreach (var objective in optionals)
        {
            if (optionalArePersistant && !forceClean)
                MissionObjectives.instance.persistantOptionals.Add(objective);
            else
                objective.CleanUpObjective();
        }

        if(immediateRewards || immediateoptionalRewards)
        {
            ImmediateRewards();
        }
    }

    /// <summary>
    /// Returns true if all objectives in list are completed
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    bool CheckObjectiveList(List<ObjectiveBase> list)
    {
        foreach(var objective in list)
        {
            if (!objective.completed)
                return false;
        }

        return true;
    }

    /// <summary>
    /// Checks for completion and cleans up if completed
    /// returns true if completed
    /// </summary>
    /// <returns></returns>
    public bool CheckSetCompletion()
    {
        bool completed = CheckObjectiveList(objectives);

        if(completed)
        {
            CleanUpSet();
        }

        return completed;
    }

    /// <summary>
    /// Checks objective status and applies rewards
    /// </summary>
    public void ApplyRewards()
    {
        if(CheckObjectiveList(objectives))
        {
            foreach(var reward in rewards)
            {
                reward.ApplyRewards();
            }
        }

        if (CheckObjectiveList(optionals))
        {
            foreach (var reward in optionalRewards)
            {
                reward.ApplyRewards();
            }
        }
    }

    public void ImmediateRewards()
    {
        if (CheckObjectiveList(objectives) && immediateRewards)
        {
            //for (int i = rewards.Count -1; i > 0; i--)
            //{
            //    rewards[i].ApplyRewards();
            //    rewards.RemoveAt(in)
            //}

            foreach (var reward in rewards)
            {
                reward.ApplyRewards();
            }

            rewards.Clear();
        }


        if (CheckObjectiveList(optionals) && immediateoptionalRewards)
        {
            foreach (var reward in optionalRewards)
            {
                reward.ApplyRewards();
            }

            rewards.Clear();
        }
    }

    public ObjectiveSet MakeCopy()
    {
        ObjectiveSet copy = Instantiate(this);

        copy.name = this.name;

        List<ObjectiveBase> objectivesCopy = new List<ObjectiveBase>();

        foreach(var objective in objectives)
        {
            if (objective)
                objectivesCopy.Add(objective.MakeCopy());
            else
                Debug.LogError(name + " has an empty objective slot");

        }

        copy.objectives = objectivesCopy;

        List<ObjectiveBase> optionalCopy = new List<ObjectiveBase>();

        foreach (var objective in optionals)
        {
            optionalCopy.Add(objective.MakeCopy());
        }

        copy.optionals = optionalCopy;

        return copy;
    }
}
