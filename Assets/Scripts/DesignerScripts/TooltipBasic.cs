﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TooltipBasic : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler {

    [TextArea(3, 10)]
    public string tooltipInfo;
    private bool countDown = false;
    private float timer = 0;
    // Use this for initialization
    void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (countDown)
        {
            timer += Time.deltaTime;
            if (timer > TooltipStatic.instance.hoverTime)
            {
                gameObject.SendMessage("UpdateInfo", SendMessageOptions.DontRequireReceiver);
                TooltipStatic.instance.ShowTooltip(tooltipInfo);
                countDown = false;
            }
        }
	}

    public void ShowTip()
    {
        countDown = true;
    }
    public void HideTip()
    {
        countDown = false;
        timer = 0;
        TooltipStatic.instance.HideToolTip();
    }
    private void OnMouseEnter()
    {
        if (!TooltipStatic.instance.active)
        {
            ShowTip();
        }
    }
    private void OnMouseExit()
    {
        HideTip();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!TooltipStatic.instance.active)
        {
            ShowTip();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HideTip();
    }
}
