﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TooltipBasic))]
public class ExamplePlugin : MonoBehaviour {
    private TooltipBasic toolTip;
	// Use this for initialization
	void Start () {
        toolTip = GetComponent<TooltipBasic>();
	}

    public void UpdateInfo()
    {
        toolTip.tooltipInfo = "It has been " + Time.timeSinceLevelLoad + " seconds since the level was loaded";

    }
}
