﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TooltipBasic))]
public class StatusEffectTooltip : MonoBehaviour {
    private TooltipBasic toolTip;
    public StatusEffects status;
    // Use this for initialization
    void Start ()
    {
        toolTip = GetComponent<TooltipBasic>();
	}

    private void Update()
    {
        if (status.duration == 0)
        {
            gameObject.SetActive(false);
        }
    }

    void UpdateInfo()
    {
        toolTip.tooltipInfo = status.description;
        toolTip.tooltipInfo +="\nDuration:"+ (status.duration-1);
    }
}
