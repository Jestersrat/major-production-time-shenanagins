﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TooltipBasic))]
public class UITooltip : MonoBehaviour , IPointerEnterHandler , IPointerExitHandler {
    private TooltipBasic toolTip;

	bool shown = false;

    // Use this for initialization
    void Start ()
	{
        toolTip = GetComponent<TooltipBasic>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        toolTip.ShowTip();
		shown = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		if (shown)
			StartCoroutine(Bandaid());
	}

	IEnumerator Bandaid()
	{
		if (!shown)
			yield break;

		yield return new WaitForSecondsRealtime(2f);
		toolTip.HideTip();
		shown = false;
	}
}
