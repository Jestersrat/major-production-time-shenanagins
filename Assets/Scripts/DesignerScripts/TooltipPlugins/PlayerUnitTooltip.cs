﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TooltipBasic))]
public class PlayerUnitTooltip : MonoBehaviour {
    private TooltipBasic toolTip;
    private Unit unit;
    // Use this for initialization
    void Start()
    {
        toolTip = GetComponent<TooltipBasic>();
        if (GetComponent<Unit>() != null)
        {
            unit = GetComponent<Unit>();
        }
        else { unit = GetComponent<PlayerUnitUI>().unit; }
    }
    void UpdateInfo()
    {
        toolTip.tooltipInfo = "Health : " + unit.stats.HP + "\n Damage :" + unit.stats.damageRange.min + "-" + unit.stats.damageRange.max + "\n MoveSpeed : " + unit.stats.moveSpeed ;

    }
}
