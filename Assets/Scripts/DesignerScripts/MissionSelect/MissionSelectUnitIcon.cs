﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MissionSelectUnitIcon : MonoBehaviour {

    public Unit unit;
    public Text currentLevel;
    public Text unitName;
    private Button button;
    private Vector2 startScale;
    private float scaleMult = 1.2f;

    void Start ()
        {
        startScale = GetComponent<RectTransform>().sizeDelta;
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
        button.image.sprite = unit.unitIcon;
        currentLevel.text = unit.level.unitLevel.ToString();
        unitName.text = unit.unitName;
        }

    void OnClick()
    {
        MissionSelectUnitDetails.instance.DisplayUnit(unit);
        GameManager.instance.selectedUnit = unit;
        if (MissionInfo.instance.active)
        {
            if (!GameManager.instance.missionUnits.Contains(unit))
            {
                for (int i = 0; i < SquadSelectButtons.instance.Count; i++)
                {
                    if (SquadSelectButtons.instance[i].unit==null && SquadSelectButtons.instance[i].button.interactable)
                    {
                        SquadSelectButtons.instance[i].AddUnit(unit);
                        IncreaseSize();
                        break;
                    }
                }
            }
        }
    }

    void Update()
    {
        currentLevel.text = unit.level.unitLevel.ToString();
    }

    public void IncreaseSize(bool moveToFront = true)
    {
        GetComponent<RectTransform>().sizeDelta *= scaleMult;
        if (moveToFront)
        {
            transform.SetAsFirstSibling();
        }

    }

    public void ResetSize()
    {
        GetComponent<RectTransform>().sizeDelta = startScale;

    }


}
