﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionSelectIconPool : MonoBehaviour {

    public static List<MissionSelectIconPool> iconPool = null;

    public Button button;

    public AbilityBase ability;

    public Text chargesText;

    public GameObject cdBackground;
    //private Image cdBackgroundFill;

    TooltipBasic toolTip;

    public Color selectedColor;

    RectTransform rectTrans;


    List<RectTransform> childRects = new List<RectTransform>();

    // Use this for initialization
    void Awake()
    {
        if (iconPool == null)
        {
            iconPool = new List<MissionSelectIconPool>();

            int count = 0;
            foreach (var icon in transform.parent.GetComponentsInChildren<MissionSelectIconPool>())
            {
                iconPool.Add(icon);
                ++count;
            }


        }
        //cdBackgroundFill = cdBackground.transform.Find("CFill").GetComponent<Image>();
        toolTip = GetComponent<TooltipBasic>();

        button = GetComponent<Button>();
        button.interactable = false;

        rectTrans = GetComponent<RectTransform>();

        foreach (var rect in GetComponentsInChildren<RectTransform>())
        {
            if (rect != rectTrans)
            {
                childRects.Add(rect);
            }
        }

        gameObject.SetActive(false);
    }

    public static void ShowAbilities(Unit unit)
    {
        foreach(MissionSelectIconPool icon in iconPool)
        {
            icon.gameObject.SetActive(false);
        }
        if (unit)
        {
            if (unit.abilityList.Count > iconPool.Count)
                AddExtra();

            for (int i = 0; i < unit.abilityList.Count; ++i)
            {
                iconPool[i].ability = unit.abilityList[i];
                iconPool[i].button.image.sprite = unit.abilityList[i].icon;
                iconPool[i].toolTip.tooltipInfo = "<b>" + iconPool[i].ability.name + "</b> \n" + "<b>Cost</b> " + iconPool[i].ability.apCost + "AP";
                if (iconPool[i].ability.coolDown > 0)
                {
                    iconPool[i].toolTip.tooltipInfo += " <b>Cooldown </b>:" + iconPool[i].ability.coolDown;

                }
                if (iconPool[i].ability.damage != 0)
                {
                    if (iconPool[i].ability.damageMod == AbilityBase.DamageMod.Modifier)
                    {
                        iconPool[i].toolTip.tooltipInfo += "\n<b>Damage Modifier</b>:" + iconPool[i].ability.damage;
                    }
                    else
                    {
                        iconPool[i].toolTip.tooltipInfo += "\n<b>Damage</b>:" + Mathf.Abs(iconPool[i].ability.damage);
                    }
                }
                iconPool[i].toolTip.tooltipInfo += "\n<b>Accuracy Modifier</b>" + iconPool[i].ability.accuracy;
                if (iconPool[i].ability.range.max == 0)
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Range</b>: Self";
                }
                else if (iconPool[i].ability.range.min > 0)
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Range</b>:" + iconPool[i].ability.range.min + " - " + iconPool[i].ability.range.max;
                }
                else
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Range</b>:" + iconPool[i].ability.range.max;
                }
                switch (iconPool[i].ability.areaOfEffect)
                {
                    case AbilityBase.AreaOfEffect.AreaOfEffect:
                        iconPool[i].toolTip.tooltipInfo += "\n<b>AoE</b>:" + iconPool[i].ability.aoeSize;
                        break;
                    case AbilityBase.AreaOfEffect.Chaining:
                        iconPool[i].toolTip.tooltipInfo += "\n<b>Chaining</b>:" + iconPool[i].ability.bounces + " targets \n <b>Chain Range</b>:" + iconPool[i].ability.bounceRange;
                        break;
                    default:
                        break;
                }
                if (!iconPool[i].ability.hasCharges)
                {
                    iconPool[i].chargesText.text = "";
                }
                else
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Charges Left</b>:" + iconPool[i].ability.charges;
                    iconPool[i].chargesText.text = iconPool[i].ability.charges.ToString();
                }
                iconPool[i].toolTip.tooltipInfo += "\n" + iconPool[i].ability.description;
                iconPool[i].gameObject.SetActive(true);
                iconPool[i].transform.parent.GetComponent<HorizontalLayoutGroup>().CalculateLayoutInputHorizontal();
            }
        }
    }

    public static void HideAbilities()
    {
        for (int i = 0; i < iconPool.Count; ++i)
        {
            if (iconPool[i].isActiveAndEnabled)
                iconPool[i].gameObject.SetActive(false);
            else
                break;
        }
    }

    public static void AddExtra()
    {
        var icon = Instantiate(iconPool[0].gameObject);

        icon.transform.SetParent(iconPool[0].transform.parent);

        iconPool.Add(icon.GetComponent<MissionSelectIconPool>());

        icon.SetActive(false);
    }

}
