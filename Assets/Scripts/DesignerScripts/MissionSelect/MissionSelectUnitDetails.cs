﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionSelectUnitDetails : MonoBehaviour {
    public static MissionSelectUnitDetails instance;

    public Unit currentUnit;
    public GameObject unitHolder;
    private bool active;
    private Image backPanel;
    public Text hp;
    public Text armor;
    public Text dodge;
    public Text moveSpeed;
    public Text damage;
    public Text accuracy;
    public Text crit;
    public Text unitName;
    public Text unitClass;
    public Slider xpSlider;
    public Text xpText;
    public Button levelup;
    public Text skillPoints;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        backPanel = GetComponent<Image>();
        ShowHide(false);
    }

    public void DisplayUnit(Unit unit)
    {
        if (unit != currentUnit)
        {
            currentUnit = unit;
            foreach (Transform child in unitHolder.transform)
            {
                if (child.GetComponent<Animator>() != null)
                {
                    Destroy(child.gameObject);
                }
            }
            FillUI();
            Instantiate(unit.gameObject.transform.GetChild(0).gameObject, unitHolder.transform);
        }
        else { return; }
    }
    public void Update()
    {
        if (active)
        {
            if (Input.GetButtonDown("Fire2"))
            {
                ShowHide(false);
            }
            if (currentUnit != null)
            {
                skillPoints.text = currentUnit.level.skillPoints.ToString();
            }
        }
    }

    void ShowHide(bool toggle)
    {
        active = toggle;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(toggle);
        }
        if (!toggle)
        {
            currentUnit = null;
        }
        backPanel.enabled = toggle;

    }

    public void FillUI()
    {
        ShowHide(true);
        MissionSelectIconPool.ShowAbilities(currentUnit);
        hp.text = currentUnit.baseStats.maxHP.ToString();
        armor.text = currentUnit.baseStats.defence.ToString();
        dodge.text = currentUnit.baseStats.dodge.ToString();
        moveSpeed.text = currentUnit.baseStats.moveSpeed.ToString();
        damage.text = currentUnit.baseStats.damageRange.min + "-" + currentUnit.baseStats.damageRange.max;
        accuracy.text = currentUnit.baseStats.accuracy.ToString();
        crit.text = currentUnit.baseStats.critChance + "%  " + currentUnit.baseStats.critMod + "X";
        unitName.text = currentUnit.unitName;
        unitClass.text = "Level " + currentUnit.level.unitLevel +"<b> "+ currentUnit.unitClass+"</b>";
        xpSlider.maxValue = StaticValues.instance.expTable[currentUnit.level.unitLevel + 1];
        xpSlider.value = currentUnit.level.currentExp;
        xpText.text = currentUnit.level.currentExp + "/" + StaticValues.instance.expTable[currentUnit.level.unitLevel + 1];
        skillPoints.text = currentUnit.level.skillPoints.ToString();
        if (currentUnit.level.currentExp>= StaticValues.instance.expTable[currentUnit.level.unitLevel + 1])
        {
            if(GameManager.instance.playerEssence< StaticValues.instance.expTable[currentUnit.level.unitLevel + 1] * 0.5f)
            {
                levelup.interactable = false;
                levelup.GetComponent<TooltipBasic>().tooltipInfo ="Requires :" + StaticValues.instance.expTable[currentUnit.level.unitLevel + 1] * 0.5f + " <color=blue>Essence</color>";
               
            }
            else
            {
                levelup.interactable = true;
                levelup.GetComponent<TooltipBasic>().tooltipInfo = "Increase the units power for XP and Essense";
            }
        }
        else
        {
            levelup.GetComponent<TooltipBasic>().tooltipInfo = "Not enough XP to level up";
            levelup.interactable = false;
        }
        //Debug.Log(currentUnit.damageDealt);
        //Debug.Log(currentUnit.level.unitLevel + 1);       
    }
}
