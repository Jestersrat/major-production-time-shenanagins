﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitViewer : MonoBehaviour {
    private float currentSpeed = 0;
    private void OnMouseDrag()
    {
        if (Input.GetAxis("Mouse X")!=0)
        {
            currentSpeed += Input.GetAxis("Mouse X");
        }
        transform.Rotate(Vector3.down * currentSpeed );
    }
    private void OnMouseUp()
    {
        currentSpeed = 0;
    }
}
