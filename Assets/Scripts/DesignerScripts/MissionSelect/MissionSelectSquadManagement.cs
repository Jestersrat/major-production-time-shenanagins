﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionSelectSquadManagement : MonoBehaviour {
    public static MissionSelectSquadManagement instance;
    public GameObject unitPrefab;
    public List<MissionSelectUnitIcon> units;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        foreach (Unit unit in GameManager.instance.units)
        {
            AddUnit(unit);
        }
    }

    public void AddUnit(Unit newUnit)
    {
        GameObject prefab = Instantiate(unitPrefab, transform);
        units.Add(prefab.GetComponent<MissionSelectUnitIcon>());
        prefab.GetComponent<MissionSelectUnitIcon>().unit = newUnit;
    }
}
