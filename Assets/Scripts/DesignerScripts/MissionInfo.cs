﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MissionInfo : MonoBehaviour {
    public static MissionInfo instance=null;

    public bool active = false;
    public string levelToLoad;
	// Use this for initialization
	void Awake () {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance!=this)
        { Destroy(this); }
	}

    public Text missionName;
    public Text description;
    public Text trauma;
    public Text stability;
    public Text difficulty;
    public Text rewards;
    public Text warning;
    public Button startButton;
    public MissionSelector targetMission;
    public void FillInfo(MissionSelector mission)
    {
        Reset();
        active = true;
        GetComponent<Image>().enabled = true;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
        levelToLoad = mission.levelName;
        missionName.text = mission.missionName;
        description.text = mission.description;
        trauma.text = "Trauma Level: " + mission.trauma;
        stability.text = "Instability:" + mission.inStability;
        difficulty.text = "Difficulty" + mission.difficulty;
        rewards.text = mission.rewards;
        warning.text = "";
        targetMission = mission;
        for (int i = 0; i < 4; i++)
        {
            if (mission.requiredUnits.Length > i)
            {
                if (GameManager.instance.units.Contains(mission.requiredUnits[i]))
                {
                    SquadSelectButtons.instance[i].RequiredUnit(mission.requiredUnits[i]);
                }
                else
                {
                    warning.text += "Mission requires " + mission.requiredUnits[i].unitName;
                }
            }
            else if (mission.squadSize <= i)
            {
                SquadSelectButtons.instance[i].LockedSlot();
            }
            else
            {
                SquadSelectButtons.instance[i].EmptySlot();
            }
        }
        if (!mission.playedText && mission.playFlowChart != "")
        {
            mission.playedText = true;
            FungusController.instance.flowChart.ExecuteBlock(mission.playFlowChart);
        }
        startButton.interactable = (true);
    }

    void Update()
    {
        if (active && Input.GetButtonDown("Fire2"))
        {
            Reset();
        }
    }

    void Reset()
    {
        GetComponent<Image>().enabled = false;
        GameManager.instance.missionUnits.Clear();
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (MissionSelectUnitIcon icon in MissionSelectSquadManagement.instance.units)
        {
            icon.ResetSize();
        }
        foreach (SquadSelectButtons squad in SquadSelectButtons.instance)
        {
            squad.RemoveFromList();
        }
        active = false;
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public void AddToComplete()
    {
        if (!GameManager.instance.completedMissions.Contains(targetMission.levelName))
        {
            GameManager.instance.completedMissions.Add(targetMission.levelName);
            foreach (Unit unit in targetMission.gainUnits)
            {
                MissionSelectSquadManagement.instance.AddUnit(unit);
                GameManager.instance.InitialiseUnit(unit);
            }
        }
    }
}
