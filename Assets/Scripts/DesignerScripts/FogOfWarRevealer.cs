﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarRevealer : MonoBehaviour
{
    public bool revealed=false;
    public CellInfo hex;
    public LayerMask visionBlocking;
    public LayerMask gridLayerMask;
    public float gridSize = 1.5f;
    [HideInInspector]
    public Vector3 losTarget;
    //private bool refreshLoS=false;
    public bool beenRevealed=false;
    public GameObject fogLayer;
    public float visionRange=15;
	// Use this for initialization
	void Start ()
    {
        hex = GetComponent<CellInfo>();
        losTarget = transform.position + Vector3.up * 1f;
        TurnManager.instance.OnEndTurn.AddListener(OnEndTurn);
        OnEndTurn(UnitType.Hostile);
    }

    void OnEndTurn(UnitType unit)
    {
        if (unit != UnitType.Player)
        {
            HideTile(hex);
            if (hex.unit != null && hex.unit.tag == "Player")
            {
                Invoke("RefreshLos", 0.000001f);
            }
        }
    }

    void RefreshLos()
    {
        LineOfSight(hex.unit);
    }

    public void LineOfSight(Unit targetUnit)
    {
        float visionRange = gridSize * (targetUnit.stats.visionRange+Mathf.Floor(transform.position.y*2));
        foreach (Collider col in Physics.OverlapSphere(losTarget,visionRange,gridLayerMask))
        {
            CellInfo grid = col.GetComponent<CellInfo>();

            if (!grid.fog)
                grid.fog = grid.GetComponent<FogOfWarRevealer>();

            if (!grid.fog.revealed)
            {
                Vector3 target = grid.fog.losTarget;
                float distance = Vector3.Distance(target, losTarget);
                if (distance < visionRange)
                {
                    RaycastHit visionRay;

                    if (!Physics.Raycast(losTarget, target-losTarget, out visionRay, distance, visionBlocking))
                    {
                        //Debug.DrawLine(target, losTarget);
                        //grid.Value.obstacle.gameObject.GetComponent<Renderer>().material.color = Color.white;
                        grid.fog.revealed = true;
                        grid.fog.beenRevealed = true;
                        grid.fog.fogLayer.SetActive(false);
                        if (grid.unit != null)
                        {
                            ShowUnit(grid.unit);
                        }
                    }
                    else
                    {
                        //Debug.Log("Hidden by obstacle"+ visionRay.collider.gameObject.name);
                        //Debug.DrawLine(visionRay.point, transform.position + Vector3.up, Color.red);
                    }
                    //else if (!grid.Value.GetComponent<FogOfWarRevealer>().revealed)
                    //{
                    //    Debug.Log("I cannot see");
                    //    HideTile(grid.Value);
                    //}
                }
                //else if (!grid.Value.GetComponent<FogOfWarRevealer>().revealed)
                //{ HideTile(grid.Value); }
            }
        }
    }
    public void HideTile(CellInfo grid)
    {
        if (grid.obstacle == null)
        {
            fogLayer.SetActive(true);
            if (beenRevealed)
            {
                Color newColor = fogLayer.GetComponent<Renderer>().material.color;
                newColor.a = 0.5f;
                fogLayer.GetComponent<Renderer>().material.color = newColor;
            }
            grid.fog.revealed = false;
            if (grid.unit != null && grid.unit.tag != "Player")
            {
                HideUnit(grid.unit);
            }
        }
        else
        {
            fogLayer.SetActive(false);
        }
    }

    public void ShowUnit(Unit unit)
    {
        if (unit)
        {
            if (unit.gameObject.GetComponent<MeshRenderer>() != null)
            {
                unit.gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
            Renderer[] rends = unit.GetComponentsInChildren<Renderer>();
            foreach (Renderer rend in rends)
            {
                rend.enabled = true;
            }
            unit.beenRevealed = true;
        }
    }

    public void HideUnit(Unit unit)
    {
        if (unit)
        {
            if (unit.gameObject.GetComponent<MeshRenderer>() != null)
            {
                unit.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            Renderer[] rends = unit.GetComponentsInChildren<Renderer>();
            foreach (Renderer rend in rends)
            {
                rend.enabled = false;
            }
        }
    }

    public void ShowTile()
    {
        hex.fog.revealed = true;
        hex.fog.beenRevealed = true;
        hex.fog.fogLayer.SetActive(false);
        if (hex.obstacle != null)
        {
            hex.obstacle.gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (hex.unit != null)
        {
            ShowUnit(hex.unit);
        }
    }
}

