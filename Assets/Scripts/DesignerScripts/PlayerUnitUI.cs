﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUnitUI : MonoBehaviour
{
    public Unit unit;
    public Slider healthSlider;
    public Text unitName;
    //public GameObject defenceHolder;
    public GameObject APHolder;
    public Text hpText;
    public GameObject statusHolder;

    public Image portrait;

    public HorizontalLayoutGroup HLG;

    private Image[] aPIcons;
    private StatusEffectTooltip[] statuses;
    //private Image[] shields;

    Vector3 apIconScale;

    float hp;
    float maxHp;

    RectTransform rectTrans;

    public Color selectedColor;

    Vector2 baseSize;

    List<RectTransform> childRects = new List<RectTransform>();

	// Use this for initialization
	void Start ()
    {
        APHolder = APPipHider.instance.gameObject;
        healthSlider = GetComponentInChildren<Slider>();
        //portrait = GetComponentInChildren<Image>();
        aPIcons = new Image[APHolder.transform.childCount];
        for (int i = 0; i < aPIcons.Length; i++)
        {
            aPIcons[i] = APHolder.transform.GetChild(i).gameObject.transform.GetChild(0).GetComponent<Image>();
        }
        apIconScale = aPIcons[0].transform.localScale;
        statuses = new StatusEffectTooltip[statusHolder.transform.childCount];
        for (int i = 0; i < statuses.Length; i++)
        {
            statuses[i] = statusHolder.transform.GetChild(i).GetComponent<StatusEffectTooltip>();
            statuses[i].gameObject.SetActive(false);
        }
        //shields = new Image[defenceHolder.transform.childCount];
        //for (int i=0; i < shields.Length; i++)
        //{
        //    shields[i] = defenceHolder.transform.GetChild(i).GetComponent<Image>();
        //    shields[i].gameObject.SetActive(false);
        //}

        rectTrans = GetComponent<RectTransform>();

        baseSize = rectTrans.sizeDelta;

        foreach (var rect in GetComponentsInChildren<RectTransform>())
        {
            if (rect != rectTrans)
            {
                childRects.Add(rect);
            }
        }

        HLG = transform.parent.GetComponent<HorizontalLayoutGroup>();

        UnitManager.OnUnitSelected.AddListener(OnUnitChange);
    }

    // Update is called once per frame
    void Update ()
    {
        if(!unit)        
            Destroy(this.gameObject);

        UpdateHealth();
    }
    
    void OnUnitChange(Unit e)
    {
        rectTrans.sizeDelta = baseSize;

        rectTrans.localScale = Vector3.one;

        if (e == unit)
        {
            if (UnitManager.instance.CurrentUnit == unit)
            {
                
                rectTrans.sizeDelta = baseSize * 1.3f;

                rectTrans.localScale = rectTrans.localScale * 1.3f;

            }
        }

        HLG.spacing = 1;
        HLG.spacing = 0.5f;

    }

    void OnDestroy()
    {
        UnitManager.OnUnitSelected.RemoveListener(OnUnitChange);
    }

    public void Initialize(Unit newUnit)
    {
        unit = newUnit;
        healthSlider.maxValue = unit.stats.maxHP;
        healthSlider.value = unit.stats.HP;
        //unitName.text = unit.name;
        portrait.sprite = unit.unitIcon;
        unit.unitUI = this;

        if (statuses!=null)
        {
            foreach (StatusEffectTooltip stat in statuses)
            {
                stat.status = null;
                stat.gameObject.SetActive(false);
            }
        }

    }

    public void SelectUnit()
    {
        if (unit != null)
        {
            UnitManager.instance.CurrentUnit = unit;
            Camera.main.GetComponentInParent<RTSCamera>().Center(unit.gameObject);
        }
    }

    public void UpdateHealth()
    {
        hp =Mathf.CeilToInt(unit.stats.HP);
        maxHp = unit.stats.maxHP;
        healthSlider.maxValue = maxHp;
        healthSlider.value = hp;
        hpText.text = hp + "/" + maxHp;
        
        if (unit == UnitManager.instance.CurrentUnit)
        {
            int pathCost = 0;
            int ap = unit.stats.actionPoints;

            if (unit.potentialPath != null)
                pathCost = unit.GetMoveCost((int)unit.potentialPath.TotalCost);

            // change in future
            unit.remainingActionPoints = ap - pathCost;

            pathCost += unit.actionCost;
            APHolder.SetActive(true);
            for (int i = 0; i < 9; i++)
            {
                if (ap > i)
                {
                    aPIcons[i].color = Color.white;
                    if (pathCost > i)
                    {
                        aPIcons[i].color = Color.yellow;
                    }

                    if(pathCost - 1 == i)
                    {
                        aPIcons[i].transform.localScale = apIconScale * 1.5f;
                    }
                    else
                    {
                        aPIcons[i].transform.localScale = apIconScale;
                    }

                }
                else
                {
                    aPIcons[i].color = Color.grey;
                }
            }
        }


        foreach (StatusEffects status in unit.statuses)
        {
            bool exists = false;
            for (int i = 0; i < statusHolder.transform.childCount; i++)
            {
                if (statuses[i].status == status)
                {
                    exists = true;
                }
            }
            if (!exists)
            {
                for (int i = 0; i < statuses.Length; i++)
                {
                    if (!statuses[i].gameObject.activeInHierarchy)
                    {
                        statuses[i].gameObject.SetActive(true);
                        statuses[i].GetComponent<Image>().sprite = status.icon;
                        statuses[i].status = status;
                        break;
                    }
                }
            }
        }
    }
}
