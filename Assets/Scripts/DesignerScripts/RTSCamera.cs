﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCamera : MonoBehaviour
{
    public static RTSCamera instance;

    [Tooltip("Min and Max zoom levels of the game")]
    public float maxZoom=50, minZoom=5;
    [Tooltip("Sets the min and max rotation on the X axis")]
    public float minVertRotation = 0, maxVertRotation = 65;
    public Camera mainCamera;

    [Tooltip("How fast the camera zooms")]
    public float zoomSpeed = 5;
    [Tooltip("How fast the camera pans across the screen")]
    public float panSpeed = 1;
    [Tooltip("How fast the camera rotates")]
    public float rotationSpeed = 10.0f;
    private float mouseX,mouseY;
    [Tooltip("0 is Off, 1 is On. Inverts rotation of camera")]
    public int invertMouse = 0;

    [Tooltip("Used for positioning above terrain")]
    public LayerMask terrainLayer;

    float shakeTimer = 0;

    float shakeAmount = 0;

    bool pressed = false;

    float focusDistance = 0;

    public bool cameraLock=false;

    Transform originalParent;

    GameObject rotationHelper;

    void Start()
    {
        instance = this;

        panSpeed = PlayerPrefs.GetFloat("PanSpeed", panSpeed);
        rotationSpeed = PlayerPrefs.GetFloat("RotationSpeed", rotationSpeed);
        invertMouse = PlayerPrefs.GetInt("InvertMouse", invertMouse);
        //mainCamera = transform.GetChild(0).gameObject;
        mainCamera = Camera.main;



        originalParent = transform.parent;
        rotationHelper = new GameObject("Rotational helper");
        rotationHelper.transform.parent = originalParent;
    }

    void Update()
    {
        if(shakeTimer > 0)
        {
            mainCamera.transform.localPosition = Random.insideUnitSphere * Random.Range(0, shakeAmount);
            shakeTimer -= Time.unscaledDeltaTime;
        }
        else if(shakeTimer < 0)
        {
            shakeTimer = 0;
            mainCamera.transform.localPosition = Vector3.zero;
        }
    }

    Vector3 point;

    void LateUpdate()
    {
        if (UnitManager.instance.waitingForDialog || cameraLock)
        {
            return;
        }
        //Moves the camera up and down along the y axis, inbetween the max and minimum camera height.
        if (Input.GetAxisRaw("Mouse ScrollWheel")!=0)
        {
            //transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y - Input.GetAxisRaw("Mouse ScrollWheel") * zoomSpeed, minZoom, maxZoom), transform.position.z);
            var input = Input.GetAxisRaw("Mouse ScrollWheel");
            if ((transform.position.y < maxZoom && transform.position.y > minZoom))// || (transform.position.y > minZoom && transform.position.y > maxZoom && input > 0) || (transform.position.y < minZoom && transform.position.y < maxZoom && input < 0))
                transform.position += mainCamera.transform.forward * input * zoomSpeed;
            if (transform.position.y > minZoom && transform.position.y > maxZoom && input > 0)
                transform.position += mainCamera.transform.forward * input * zoomSpeed;
            if (transform.position.y < minZoom && transform.position.y < maxZoom && input < 0)
                transform.position += mainCamera.transform.forward * input * zoomSpeed;
        }
        //If horizontal movement keys (A,D, <-,->) have been pressed since last frame, the camera will pan up or down.
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            transform.Translate(Vector3.right * Input.GetAxisRaw("Horizontal") * panSpeed);
        }
        //If the vertical movement keys (W,S , arrow keys) have been pressed since last frame, the camera will move horizontally
        if (Input.GetAxisRaw("Vertical") != 0)
        {
            transform.Translate(Vector3.forward * Input.GetAxisRaw("Vertical") * panSpeed);
        }
        //If the Fire3 Button(Default middle mouse/left shift) has been pressed since last frame, and the mouse has moved, the camera will rotate.
        if (Input.GetButton("Fire3"))
        {
            if (!pressed)
            {
                pressed = true;
                LayerMask mask = LayerMask.GetMask("Terrain");
                RaycastHit hit;
                if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, Mathf.Infinity, mask))
                {
                    Debug.DrawLine(mainCamera.transform.position, hit.point, Color.green, 10);

                    point = hit.point;
                    focusDistance = Vector3.Distance(point, mainCamera.transform.position);
                }
                else
                {
                    point = mainCamera.transform.forward * focusDistance;
                }
            }


            if (Input.mousePosition.x != mouseX)
            {
                var cameraRotationY = -(Input.mousePosition.x - mouseX) * rotationSpeed * Time.deltaTime;
                if (invertMouse == 0)
                {
                    cameraRotationY *= -1;
                }
                //gameObject.transform.Rotate(0, cameraRotationY, 0);
                gameObject.transform.RotateAround(point, Vector3.up, cameraRotationY);
            }
            if (Input.mousePosition.y != mouseY)
            {
                var cameraRotationX = (Input.mousePosition.y - mouseY) * rotationSpeed * 0.5f * Time.deltaTime;
                if (invertMouse == 0)
                {
                    cameraRotationX *= -1;
                }
                var targetRotation = mainCamera.transform.eulerAngles.x + cameraRotationX;
                if (targetRotation > minVertRotation && targetRotation < maxVertRotation)
                {
                    mainCamera.transform.Rotate(cameraRotationX, 0, 0);
                }
            }
        }
        else if(pressed)
        {
            pressed = false;
        }
        mouseX = Input.mousePosition.x;
        mouseY = Input.mousePosition.y;
    }

    public void InvertMouseLook(bool onOrOff)
    {
        if (onOrOff)
        {
            invertMouse = 1;
            PlayerPrefs.SetInt("InvertMouse", 1);
        }
        else
        {
            invertMouse = 0;
            PlayerPrefs.SetInt("InvertMouse", 0);
        }
    }

    public void SetRotateSpeed(float speed)
    {
        rotationSpeed = speed;
    }
    public void SetPanSpeed(float speed)
    {
        panSpeed = speed;
    }

    public void Center(GameObject target)
    {
        // made this a flat value to prevent the weird zoom things, seems functional
        gameObject.transform.position = target.transform.position - mainCamera.transform.forward * 15f;
        //Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(.5f, .5f));
        //RaycastHit hit;
        //LayerMask mask = LayerMask.GetMask("Terrain");
        //if(Physics.Raycast(ray, out hit, mask))
        //{
        //          Debug.Log(hit.distance);
        //          Debug.Log(hit.collider);
        //	gameObject.transform.position = target.transform.position - mainCamera.transform.forward * hit.distance;
        //}


        //Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(0.5f, 0.5f, 0f));
        //RaycastHit hit;
        //if (Physics.Raycast(ray,out hit,100f,terrainLayer))
        //{
        //    Debug.Log("Hit");
        //    transform.position = new Vector3( target.transform.position.x- hit.point.x , transform.position.z,  target.transform.position.z- hit.point.z);
        //}
        //Debug.Log("Unit selected, camera will move to it when i get some maths done");
        //script to move camera so it now centres over the unit goes here.
    }

    /// <summary>
    /// Shakes camera for "duration" seconds and shake between 0 and "amount"
    /// </summary>
    /// <param name="duration"></param>
    /// <param name="amount"></param>
    public void CameraShake(float duration , float amount)
    {
        if (StaticValues.instance.cameraShake)
        {
            shakeTimer = duration;
            shakeAmount = amount;
        }
    }

    IEnumerator TimeSlow(float duration, float timeScale)
    {
        if (StaticValues.instance.timeSlow)
        {
            Time.timeScale = timeScale;
            yield return new WaitForSecondsRealtime(duration);
            while (Time.timeScale < 1)
            {
                Time.timeScale += Time.unscaledDeltaTime;
                yield return null;
            }
            Time.timeScale = 1;
        }
    }

    public void SlowTime(float duration, float timeScale)
    {
        StartCoroutine(TimeSlow(duration, timeScale));
    }
}
