﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseParticles : MonoBehaviour {
    public static MouseParticles instance;

    public bool active = false;
    public Vector3 displacement;
    public ParticleSystem particles;
    // Use this for initialization
    private void Awake()
    {
        particles = GetComponent<ParticleSystem>();
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 target = ray.GetPoint(4) - displacement;
            transform.position = target;
        }
    }
}
