﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APPipHider : MonoBehaviour {
    public static APPipHider instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else { Destroy(gameObject); }
    }
	// Update is called once per frame
	void Update () {
        if (UnitManager.instance.CurrentUnit == null)
        {
            gameObject.SetActive(false);
        }	
	}
}
