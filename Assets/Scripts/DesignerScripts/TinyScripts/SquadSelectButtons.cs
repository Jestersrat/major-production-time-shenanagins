﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SquadSelectButtons : MonoBehaviour
{
    public static List<SquadSelectButtons> instance = null;
    public Sprite emptySlot;
    public Sprite lockedSlot;
    public Unit unit;
    public Button button;
    public bool requiredUnit;
    void Start()
    {
        if (instance == null)
        {
            instance = new List<SquadSelectButtons>();

            int count = 0;
            foreach (var icon in transform.parent.GetComponentsInChildren<SquadSelectButtons>())
            {
                instance.Add(icon);
                ++count;
            }
        }
        gameObject.SetActive(false);
    }

    public void RemoveFromList()
    {
        GameManager.instance.missionUnits.Remove(unit);
        for (int i = 0; i < MissionSelectSquadManagement.instance.units.Count; i++)
        {
            if (unit == MissionSelectSquadManagement.instance.units[i].unit)
            {
                MissionSelectSquadManagement.instance.units[i].ResetSize();
                break;
            }
        }
        GetComponent<Image>().sprite = emptySlot;
        unit = null;
    }
    private void OnEnable()
    {
        button.onClick.AddListener(OnClick);
    }

    private void OnDisable()
    {
        button.onClick.RemoveListener(OnClick);

    }

    void OnClick()
    {
        if (unit != null)
        {
            RemoveFromList();
        }
    }

    public void RequiredUnit(Unit lockedUnit)
    {
        unit = lockedUnit;
        requiredUnit = true;
        button.interactable = false;
        GetComponent<Image>().sprite = unit.unitIcon;
        GameManager.instance.missionUnits.Add(unit);
        foreach(MissionSelectUnitIcon icon in MissionSelectSquadManagement.instance.units)
        {
            if (icon.unit == unit)
            {
                icon.IncreaseSize();
            }
        }
    }

    public void EmptySlot()
    {
        GetComponent<Image>().sprite = emptySlot;
        requiredUnit = false;
        button.interactable = true;
    }

    public void LockedSlot()
    {
        GetComponent<Image>().sprite = lockedSlot;
        requiredUnit = false;
        button.interactable = false;
    }

    public bool AddUnit(Unit newUnit)
    {
        if (!GameManager.instance.missionUnits.Contains(newUnit))
        {
            unit = newUnit;
            GetComponent<Image>().sprite = unit.unitIcon;
            GameManager.instance.missionUnits.Add(unit);
            return true;
        }
        else if (requiredUnit)
        {
            return true;
        }
        GetComponent<Image>().sprite = emptySlot;
        return false;
    }
}
