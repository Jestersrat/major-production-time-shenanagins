﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipDeadSpiders : MonoBehaviour {
    private Unit unit;
    public float spinDuration=0.5f;
    public Quaternion targetRotation= new Quaternion(0,0,0.5f,0);
    private float startTime;
    private bool hasDied;
	// Use this for initialization
	void Start () {
        unit = GetComponent<Unit>();
	}
	
	// Update is called once per frame
	void Update () {
        if (unit.isDead && !hasDied)
        {
            startTime = Time.time;
            hasDied = true;
            foreach(Transform child in transform)
            {
                var coroutine = Flip(child);
                StartCoroutine(coroutine);
            }
        }
        if (hasDied && Time.time >= startTime + spinDuration * 2)
        {
            Destroy(this);
        }
	}

    private IEnumerator Flip(Transform target)
    {
        var startPos = target.transform.rotation;
        while (Time.time < startTime + spinDuration)
        {
            target.rotation = Quaternion.Lerp(startPos, targetRotation, (Time.time - startTime) / spinDuration);
            yield return null;
        }
        target.rotation = targetRotation;
    }
}
