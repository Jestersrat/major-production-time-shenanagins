﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobAndFlicker : MonoBehaviour {

    public bool flicker=false;
    public Light lightSource;
    public bool bob = false;
    public float bobSpeed;
    public float bobDistance;
    public float flickerSpeed;
    public Vector2 flickerMinMax;
    private Vector3 startPos;
    public bool bobOnSwiping = false;

    void OnMouseOver()
    {
        if (!bobOnSwiping)
        {
            bobOnSwiping = true;
            StartCoroutine(BobOnSwipe());
        }
    }

    void Update()
    {
        if (flicker)
        {
            lightSource.intensity = flickerMinMax[0] + Mathf.PingPong(Time.time * flickerSpeed, flickerMinMax[1]-flickerMinMax[0]);
        }
        if (bob)
        {
            transform.position += transform.up * Mathf.PingPong(Time.time * bobSpeed, bobDistance);
        }
    }

    private IEnumerator BobOnSwipe()
    {
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime;
            transform.position += transform.up *Mathf.Lerp(0,bobDistance,i*bobSpeed);
            yield return null;
        }

        bobOnSwiping = false;
    }
}
