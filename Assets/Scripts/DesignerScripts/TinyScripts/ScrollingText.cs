﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingText : MonoBehaviour
{
    private Text text;
    public float fadeSpeed = 1;
    public float moveSpeed = 1;
    private Camera mainCamera=null;
    public Vector3 baseScale;
    public bool sway;
    private bool popText = false;

    // Use this for initialization
    void Awake () {
        text = GetComponent<Text>();
        mainCamera = Camera.main;
	}
    void OnEnable()
    {
        transform.LookAt(mainCamera.transform.forward*10000);
        popText = true;
    }

    //void OnDestroy()
    //{
    //    PoolMananger.instance.pooledTexts.Remove(gameObject);
    //}

    // Update is called once per frame
    void Update () {
        var color = text.color;
        if (popText)
        {
            StartCoroutine(PopText());
            popText = false;
        }
        if (color.a > 0)
        {
            text.color = new Color(color.r, color.g, color.b, color.a-(fadeSpeed * Time.deltaTime));
            //transform.position = new Vector3(transform.position.x, transform.position.y , transform.position.z + moveSpeed * Time.deltaTime);
            transform.Translate(-transform.forward * Time.deltaTime * moveSpeed, Space.Self);
            if(sway)
            {
                transform.localPosition+=new Vector3(0.05f - Mathf.PingPong(Time.time*0.3f, 0.1f),0, 0);
            }
        }
        else {
            GetComponent<RectTransform>().localScale = baseScale;
            gameObject.SetActive(false);
        }
    }

    private IEnumerator PopText()
    {
        float timer = 0;
        Vector3 startScale = transform.localScale;
        while (timer < 0.2)
        {
            transform.localScale = startScale *(1 + 3 * Mathf.PingPong(Time.time,0.1f));
            timer += Time.deltaTime;
            yield return null;
        }
        transform.localScale = startScale;

    }
}
