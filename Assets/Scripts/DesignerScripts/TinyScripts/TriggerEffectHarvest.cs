﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ExtraTrigger", menuName = "Status Effects/Status Triggers/Harvest", order = 1)]
public class TriggerEffectHarvest : TriggerEffects {

    public float essenseMultiplier;
    public float maxEssense;
    public bool restoreCharges;
    public bool restoreAllAbilities;
    public int[] restoredAbilities;
    public bool refreshCooldowns;
    public int refreshAmount;
    public bool targetCaster = false;

    public override void Trigger(Unit user, Unit target)
    {
        if (!targetCaster)
        {
            user = target;
        }
        if (essenseMultiplier != 0)
        {
            float val = Mathf.Clamp(target.level.essenceValue * essenseMultiplier, 0, user.stats.damageRange.max);
            if(val > maxEssense*user.stats.damageRange.max)
            {
                val = maxEssense * user.stats.damageRange.max;
            }
            GameManager.instance.AddEssence(val, target.transform.position);
            MessageLog.instance.AddEvent(user.name + " drains<color=blue>" + val + "</color> essence");
        }
        if (restoreCharges)
        {
            if (restoreAllAbilities)
            {
                for (int i = 0; i < user.abilities.Count;i++)
                {
                    if (user.abilities[i].hasCharges && user.abilities[i].charges<user.abilities[i].maxCharges)
                    {
                        user.abilities[i].charges += 1;
                    }
                }
            }
            else
            {
                for (int i = 0; i < restoredAbilities.Length; i++)
                {
                    if (user.abilities[restoredAbilities[i]].hasCharges && user.abilities[restoredAbilities[i]].charges < user.abilities[restoredAbilities[i]].maxCharges)
                    {
                        user.abilities[restoredAbilities[i]].charges += 1;
                    }
                }
            }
        }
        if (refreshCooldowns)
        {
            for (int i = 0; i < user.abilities.Count; i++)
            {
                if (user.abilities[i].coolDownCounter > 0)
                {
                    user.abilities[i].coolDownCounter -= refreshAmount;
                    if (user.abilities[i].coolDownCounter < 0)
                    {
                        user.abilities[i].coolDownCounter = 0;
                    }
                }
            }
        }
    }

}
