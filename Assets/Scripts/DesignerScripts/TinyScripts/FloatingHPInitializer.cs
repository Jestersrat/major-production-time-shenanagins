﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingHPInitializer : MonoBehaviour
{
    private Unit unit = null;
    public FloatingHealthBars bar=null;
    public GameObject worldCanvas;
    public Vector3 barPos = Vector3.up * 3;
    public float scale = 1;
	// Use this for initialization
	void Start () {
        worldCanvas = GameObject.Find("WorldCanvas");
        if (worldCanvas == null)
        {
            this.enabled = false;
            return;
        }
        if (bar == null)
        {
            Debug.Log("here");
            GameObject barObj= Resources.Load("Prefabs/FloatingHealthbar") as GameObject;
            bar = barObj.GetComponent<FloatingHealthBars>();
        }
        bar = Instantiate(bar, worldCanvas.transform);
        bar.barPos = barPos;
        bar.gameObject.transform.localScale *= scale;
        unit = GetComponent<Unit>();
        bar.unit = unit;
    }
	
}
