﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ExtraTrigger", menuName = "Status Effects/Status Triggers/AddStatus", order = 1)]
public class TriggerExtraStatus : TriggerEffects {

    public StatusEffects[] status;
    public AbilityBase[] abilities;

    public override void Trigger(Unit user, Unit target)
    {
        if (triggerTarget == TriggerTarget.User)
        {
            for (int i = 0; i < status.Length; i++)
            {
                user.statuses.Add(status[i]);
            }
            for (int i=0; i < abilities.Length;i++)
            {
                abilities[i].Execute(user, user.CurrentTile);
            }
        }
        else
        {
            for (int i = 0; i < status.Length; i++)
            {
                target.statuses.Add(status[i]);
            }
        }
    }
}
