﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleHider : MonoBehaviour {
    private ParticleSystem part;
    public bool destroy = false;
    private float minimumLifeTime = 0;
	// Use this for initialization
	void Start () {
        part = GetComponent<ParticleSystem>();
	}

    // Update is called once per frame
    void LateUpdate()
    {
        if (minimumLifeTime >= 0.5f)
        {
            if (destroy && !part.IsAlive())
            {
                Destroy(gameObject);
            }
            else if (!part.IsAlive())
            {
                gameObject.SetActive(false);
            }
        }
        minimumLifeTime += Time.deltaTime;
    }
}
