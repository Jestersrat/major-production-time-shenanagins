﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FungusSkipButton : MonoBehaviour {
    private Button button;
    private FungusController fungus;
	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
	}
	
    void OnEnable()
    {
        if (button == null)
        {
            button = GetComponent<Button>();
        }
        fungus = FindObjectOfType<FungusController>();
        button.onClick.AddListener(OnClick);
    }

    void OnDisable()
    {
        button.onClick.RemoveListener(OnClick);
    }

    void OnClick()
    {
        Debug.Log("Click");
        fungus.SkipDialog();
    }

}
