﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeObjectOverTurns : MonoBehaviour {
    public int startTurn, endTurn;
    public Color targetColor;
    public Color startColor;
    private ColorPicker colorPicker;
    private Renderer rend;
	// Use this for initialization
	void Start () {
        TurnManager.instance.OnEndTurn.AddListener(OnEndTurn);
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        colorPicker = GetComponent<ColorPicker>();
    }

    void OnEndTurn(UnitType unit)
    {
        if (unit == UnitType.Player)
        {
            int tn = TurnManager.instance.turnNumber;
            if(tn>=startTurn && tn <= endTurn)
            {
                Color currentColor = Color.Lerp(startColor, targetColor, (((float)tn - startTurn) / ((float)endTurn - startTurn)));
                rend.material.color = currentColor;
            }
            if (tn == endTurn)
            {
                colorPicker.emisColor = targetColor;
                colorPicker.emission = 2;
                colorPicker.pulse = true;
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
