﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckboxValueSetter : MonoBehaviour {
    [Tooltip("The name of the value in the Player Prefs file")]
    public string valueName;
    private bool playerPrefsSetting;
    private int temp;
    void Start()
    {
        temp = PlayerPrefs.GetInt(valueName, 1);
        if (temp == 0)
        {
            playerPrefsSetting = false;
        }
        else
        {
            playerPrefsSetting = true;
        }
        GetComponent<Toggle>().isOn = playerPrefsSetting;
        GetComponent<Toggle>().onValueChanged.AddListener(SetPlayerPrefs);
    }

    void SetPlayerPrefs(bool val)
    {
        if (val)
        {
            PlayerPrefs.SetInt(valueName, 1);
        }
        else
        {
            PlayerPrefs.SetInt(valueName, 0);

        }
    }
}
