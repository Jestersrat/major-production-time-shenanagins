﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitActivator : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        GetComponent<Agent>().Activated = true;
        Destroy(this);
	}
	
}
