﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveOnDeath : MonoBehaviour
{
    private float dissolve = 0;
    public float dissolveSpeed=0.5f;
    public bool keepAfter;
    public GameObject[] unParents;
    public Animator[] animators;
    private Renderer rend;
    private Material tempMaterial;
    static readonly int material_Dissolve = Shader.PropertyToID("_Dissolve_Amount");
    Transform tParent;
    public bool isBase = false;
	// Use this for initialization
	void Start ()
    {
        rend = GetComponent<Renderer>();
        tempMaterial = new Material(rend.sharedMaterial);

        if (transform.parent.parent)
        {
            if (transform.parent.parent.tag == "Enemy" || transform.parent.parent.tag == "Player")
                tParent = transform.parent.parent;
        }
        else
        {
            tParent = transform.parent;
        }
        if (isBase && tParent != null)
        {
            tParent.GetComponent<Unit>().baseRenderer = rend;

            if (tParent.tag == "Enemy")
            {
                rend.material.SetColor("_Emmisive_Color", Color.red);
            }
            else
            {
                rend.material.SetColor("_Emmisive_Color", Color.blue);
            }
        }

        //rb = GetComponent<Rigidbody>();

        //if (!rb)
        //    rb = gameObject.AddComponent<Rigidbody>();

        //rb.isKinematic = true;
    }
	
    public void Dissolve()
    {
        StartCoroutine(DissolveObj());
    }

    private IEnumerator DissolveObj()
    {
        transform.SetParent(null);
        foreach (Animator anim in animators)
        {
            anim.enabled = false;
        }

        bool socksKnocked = false;
        while (dissolve < 1)
        {
            tempMaterial.SetFloat(material_Dissolve, dissolve += Time.deltaTime * dissolveSpeed*0.5f);
            rend.sharedMaterial = tempMaterial;
            if (dissolve > 0.1)
            {
                socksKnocked = true;
                foreach (GameObject tran in unParents)
                {
                    tran.transform.SetParent(null);
                    if (tran.GetComponent<Rigidbody>() != null)
                    {
                        tran.GetComponent<Collider>().enabled = true;
                        tran.GetComponent<Rigidbody>().isKinematic = false;
                    }
                }
            }
            yield return null;
        }
        if (!keepAfter)
        {

            Destroy(gameObject);
        }
        else if(tParent!=null)
        {
            tParent.gameObject.SetActive(false);
        }
    }
}
