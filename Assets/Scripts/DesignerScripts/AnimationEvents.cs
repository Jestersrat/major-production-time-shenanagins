﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour {

    public GameObject[] turnOnOne;
    public GameObject[] turnOnZero;
    private Animator anim;
    [Tooltip("put in a whole number to represent the % chance that the rarer of the two animations will play")]
    public int[] rareAnimChance;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void HideObject(int value)
    {
        foreach (GameObject target in turnOnOne)
        {
            if (value == 1)
            {
                target.SetActive(true);
            }
            else
            {
                target.SetActive(false);
            }
        }
        foreach (GameObject target in turnOnZero)
        {
            if (value == 0)
            {
                target.SetActive(true);
            }
            else
            {
                target.SetActive(false);
            }
        }
    }

    public void UpdateBlend(int blendType)
    {
        float random = Random.Range(0, 100);
        if (random > rareAnimChance[blendType])
        {
            anim.SetFloat("IdleBlend", 1f);
        }
        else
        {
            anim.SetFloat("IdleBlend", 0);
        }


    }
}
