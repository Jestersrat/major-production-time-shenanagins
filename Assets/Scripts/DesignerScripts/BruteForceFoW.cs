﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BruteForceFoW : MonoBehaviour {
    public FogOfWarRevealer foW;
	// Use this for initialization
	void Start () {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other as BoxCollider)
        {
            if (other.tag == "Player")
            {
                foW.LineOfSight(other.GetComponent<Unit>());
            }
            else if (other.tag == "Enemy")
            {
                if (foW.revealed == true)
                {
                    foW.ShowUnit(other.GetComponent<Unit>());
                }
                else
                {
                    foW.HideUnit(other.GetComponent<Unit>());
                }

            }
        }
    }
}
