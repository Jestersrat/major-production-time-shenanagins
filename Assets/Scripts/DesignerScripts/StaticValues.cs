﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticValues : MonoBehaviour {

    public static StaticValues instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        var prefabs = Resources.LoadAll("Units", typeof(GameObject));

        UnitPrefabs = new GameObject[prefabs.Length];

        for (int i = 0; i < prefabs.Length; i++)
        {
            UnitPrefabs[i] = prefabs[i] as GameObject;
        }
        
    }

    public enum ColorCode
    {
        Health,
        Mana,
        Physical,
        Fire,
        Lightning,
        Ice,
        Water,
        Magic,
        Essence,
        Time,
        Poison
    }

    public float[] expTable = new float[] { 150, 300, 600, 1200, 2000, 2800, 3800, 5000, 10000000000000 };

    public float unitMoveSpeed = 5;
    [Tooltip("0=Health , 1=Mana, 2=Physical,3=Fire,4=Lightning,5=Ice,6=Water,7=Magic,8=Essence,9=Time , 10=Poison")]
    public Color[] GameColors;
 //   Cursor.SetCursor(StaticValues.instance.Cursors[1], Vector2.zero, CursorMode.Auto);

    public Texture2D[] Cursors;

    [Tooltip("Abilities a unit can have, in order for the save/load system to manage them properly, hopefully")]
    public GameObject[] UnitPrefabs;

    public float playerEssense = 0;

    public bool airHorn = false;
    public bool cameraShake = true;
    public bool timeSlow = true;
}
