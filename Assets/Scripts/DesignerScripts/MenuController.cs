﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class MenuController : MonoBehaviour
{
    [Tooltip ("Assign any UI elements you wish players to be able to resized to this array")]
    public GameObject[] inGameGUI;
    [Tooltip("Assign the AudioMixer,found in the Audio folder in the base project to this")]
    public AudioMixer masterMixer;
    public GameObject mainMenu;

    private GameObject canvas;
    private float canvasScale;

    [SerializeField]
    private string formLink= "https://docs.google.com/forms/d/e/1FAIpQLSff91PgTpEgeQKGtupPbaUXDc0czSFZxzxSXIHthlVoXzCmpg/formResponse";


    void Start()
    {
        if (transform.parent.GetComponent<Canvas>() != null)
        {
            canvas = transform.parent.gameObject;
        }
        else {Debug.Log( "Menucontroller needs to be a child of the canvas it controls"); }
        SetMasterVolume(PlayerPrefs.GetFloat("MasterVolume", 1));
        SetEffectsVolume(PlayerPrefs.GetFloat("EffectsVolume", 1));
        SetMusicVolume(PlayerPrefs.GetFloat("MusicVolume",1));
        SetSpeechVolume(PlayerPrefs.GetFloat("SpeechVolume", 1));
        MoveSpeed(PlayerPrefs.GetFloat("MoveSpeed", 3));
        if (mainMenu != null)
        {
            mainMenu.SetActive(false);
        }

    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += LevelWasLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= LevelWasLoaded;
    }
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && mainMenu!=null)
        {
            mainMenu.SetActive(!mainMenu.activeInHierarchy);
        }
    }

    private void LevelWasLoaded(Scene scene , LoadSceneMode mode)
    {
        if (mainMenu!=null && mainMenu.activeInHierarchy)
        {
            mainMenu.SetActive(false);
        }
    }

    public void MoveSpeed(float speed)
    {
        StaticValues.instance.unitMoveSpeed = speed * speed;
    }

    public void EndTurn()
    {
        TurnManager.instance.EndTurn();
    }

    public void ReloadScene()
    {
        var n = SceneManager.GetActiveScene().name;
        //SceneManager.UnloadSceneAsync(n);
        SceneManager.LoadSceneAsync(n);
    }

    public void HidePanel(GameObject target)
    {
        canvasScale = canvas.GetComponent<RectTransform>().localScale.x;
        RectTransform rect = target.GetComponent<RectTransform>();
        Vector2 hideDir = target.GetComponent<UIHideDirection>().direction;
        if (hideDir.x != 0)
        {
            rect.position = new Vector3((rect.position.x+rect.rect.width*hideDir.x * canvasScale), rect.position.y, rect.position.z);
        }
        if (hideDir.y != 0)
        {
            rect.position = new Vector3(rect.position.x , (rect.position.y + rect.rect.height * hideDir.y * canvasScale) , rect.position.z);
        }
        target.GetComponent<UIHideDirection>().direction *= -1;
    }

    public void RotateUIElement(GameObject target)
    {
        target.transform.localScale *= -1f;
    }

    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void OpenWebsite(string site)
    {
        Application.OpenURL(site);
    }
    /// <summary>
    /// Turns the selected game object off if it was on, and on if it was off.
    /// </summary>
    /// <param name="target"></param>
    public void ToggleOnOff(GameObject target)
    {
        target.SetActive(!target.activeInHierarchy);
    }

    /// <summary>
    /// Turns selected Game Object on
    /// </summary>
    /// <param name="target"></param>
    public void TurnOn(GameObject target)
    {
        target.SetActive(true);
    }

    /// <summary>
    /// Turns selected Game Object off
    /// </summary>
    /// <param name="target"></param>
    public void TurnOff(GameObject target)
    {
        target.SetActive(false);
    }

    /// <summary>
    /// Exits the Application. In order to test this it must be in a build.
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Changes the size of any UI elements which are in the inGameGUI array
    /// </summary>
    /// <param name="size"></param>
    public void ChangeUISize(float size)
    {
        if (inGameGUI.Length == 0)
        {
            Debug.Log("No UI designated to resize, please assign some to the menu controller");
        }
        for (int i=0; i < inGameGUI.Length; i++)
        {
            inGameGUI[i].transform.localScale = new Vector3(size, size, size);
        }
    }

    /// <summary>
    /// Changes all game volume
    /// </summary>
    /// <param name="volume"></param>
    public void SetMasterVolume(float volume)
    {
        float dB;
        if (volume != 0)
        {
            dB = 20.0f * Mathf.Log10(volume);
        }
        else
        {
            dB = -144.0f;
        }
        masterMixer.SetFloat("MasterVolume", dB);
    }

    /// <summary>
    /// Changes Music Volume
    /// </summary>
    /// <param name="volume"></param>
    public void SetMusicVolume(float volume)
    {
        float dB;
        if (volume != 0)
        {
            dB = 20.0f * Mathf.Log10(volume);
        }
        else
        {
            dB = -144.0f;
        }
        masterMixer.SetFloat("MusicVolume", dB);
    }

    /// <summary>
    /// Changes Effect Volume
    /// </summary>
    /// <param name="volume"></param>
    public void SetEffectsVolume(float volume)
    {
        float dB;
        if (volume != 0)
        {
            dB = 20.0f * Mathf.Log10(volume);
        }
        else
        {
            dB = -144.0f;
        }
        masterMixer.SetFloat("EffectsVolume", dB);
    }
    /// <summary>
    /// Changes Speech Volume
    /// </summary>
    /// <param name="volume"></param>
    public void SetSpeechVolume(float volume)
    {
        float dB;
        if (volume != 0)
        {
            dB = 20.0f * Mathf.Log10(volume);
        }
        else
        {
            dB = -144.0f;
        }
        masterMixer.SetFloat("SpeechVolume", dB);
    }


    public void ToggleAirHorn(bool horn)
    {
        StaticValues.instance.airHorn = horn;
    }


    public void TimeSlow(bool toggle)
    {
        StaticValues.instance.timeSlow = toggle;
    }

    public void ScreenShake(bool toggle)
    {
        StaticValues.instance.cameraShake = toggle;
    }
    [HideInInspector]
    public string bugDescription;
    [HideInInspector]
    public string bugRepeat;

    public void SetBugDescription(string text)
    {
        bugDescription = text;
    }
    public void SetBugRepeate(string text)
    {
        bugRepeat = text;
    }

    public void SubmitBug()
    {
        StartCoroutine(Post());
    }

    IEnumerator Post()
    {
        WWWForm formData = new WWWForm();
        formData.AddField("entry.1554307690", bugDescription);
        formData.AddField("entry.618634289", bugRepeat);
        formData.AddField("entry.661613423", "Version:" + GameManager.instance.gameVersion + "Scene:" + SceneManager.GetActiveScene().name);
        byte[] rawData = formData.data;
        WWW www = new WWW(formLink, rawData);


        yield return www;
        if (www.error != "")
        {
            Debug.Log(www.error);
        }
    }
}

//target form = https://docs.google.com/forms/d/e/1FAIpQLSff91PgTpEgeQKGtupPbaUXDc0czSFZxzxSXIHthlVoXzCmpg/formResponse
//question 1 field name = entry.1554307690;
//question 2 field name = entry.618634289;
