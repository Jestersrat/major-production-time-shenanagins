﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrisonFreeUnits : MonoBehaviour {
    public Unit[] units;
    public UnitSpawner[] unitSpawners;
    public PlayerUIFiller pUI;
    public GameObject[] models;
    private bool activate = false;
	// Use this for initialization
	void Start () {
        pUI = FindObjectOfType<PlayerUIFiller>();
	}
	
	// Update is called once per frame
	void Update () {
        activate = true;
        foreach (Unit unit in units)
        {
            if (!unit.isDead)
            {
                activate = false;
            }
        }
        if (activate)
        {
            foreach (GameObject model in models){
                model.SetActive(false);
            }
            foreach (UnitSpawner spawner in unitSpawners)
            {
                spawner.Activate();
            }
            pUI.ReInitialize();
            Destroy(gameObject);
        }
	}
}
