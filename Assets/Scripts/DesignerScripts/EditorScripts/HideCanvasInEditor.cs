﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;


[ExecuteInEditMode]
public class HideCanvasInEditor : MonoBehaviour {
    public GameObject[] objectsToHide;

    void Awake()
    {
        if (EditorApplication.isPlaying)
        {
            for (int i = 0; i < objectsToHide.Length; i++)
            {
                objectsToHide[i].SetActive(true);
            }
            Destroy(this);
        }
    }
    void Update()
    {
        if(!EditorApplication.isPlaying)
        {
            for (int i = 0; i < objectsToHide.Length; i++)
            {
                objectsToHide[i].SetActive(false);
            }
        }
    }
}
#endif