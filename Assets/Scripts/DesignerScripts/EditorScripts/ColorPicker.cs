﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script allows changing colors of an object at runtime and in editor.
/// </summary>
[ExecuteInEditMode]
public class ColorPicker : MonoBehaviour {
    public Color emisColor = Color.black;
    public Color matColor = Color.white;
    [Tooltip("Commits the edits you make, in editor")]
    public bool commitEdit = true;
    public float emission = 1.5f;
    [Tooltip("Whether the emmision values will pulse, will only work when commit edit is not set to true")]
    public bool pulse = false;
    [Tooltip("Minimum value color will pulse to, if enabled")]
    public float minimum = 0.5f;
    public float pulseSpeed = 1;
    private Renderer rend;
    private Material tempMaterial;
	// Use this for initialization
	void Awake () {
        rend = GetComponent<Renderer>();
        tempMaterial = new Material(rend.sharedMaterial);
        tempMaterial.SetColor("_EmissionColor", emisColor*emission);
        tempMaterial.color = matColor;
        rend.sharedMaterial = tempMaterial;
	}

	void Update () {
        if (pulse)
        {
            tempMaterial.SetColor("_EmissionColor", emisColor * Mathf.Clamp(Mathf.PingPong(Time.time*pulseSpeed, emission),minimum,emission));
            rend.sharedMaterial = tempMaterial;
        }
        if (commitEdit)
        {
            tempMaterial.SetColor("_EmissionColor", emisColor * emission);
            tempMaterial.color = matColor;
            rend.sharedMaterial = tempMaterial;
        }
    }
}
