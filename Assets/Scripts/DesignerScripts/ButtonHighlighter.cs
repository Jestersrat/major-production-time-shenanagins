﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHighlighter : MonoBehaviour {

    public bool active=false;
    public bool grow;
    public float growSize;
    public float minPulseSize;
    private Vector3 baseSize;
    public bool pulse;
    public bool flash;
    public Color flashColor;
    private Color startColor;

    private Image image;

	// Use this for initialization
	void Start () {
        if (flash)
        {
            image = GetComponent<Image>();
            startColor = image.color;
        }
        if (grow)
        {
            baseSize = transform.localScale;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (active)
        {
            if (pulse)
            {
                transform.localScale=baseSize *Mathf.PingPong(Time.time, growSize);
            }
            if (flash)
            {

            }
        }
	}

    public void Activate(bool b)
    {
        active = b;
        if (active)
        {
            if (grow && !pulse)
            {
                transform.localScale *= growSize;
            }
        }
        else
        {
            if (grow)
            {
                transform.localScale = baseSize;
            }
            if (flash)
            {
                image.color = startColor;
            }
        }
    }
}
