﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailedUnitUI : MonoBehaviour {
    public Text hP,Damage, unitName, unitClass, move, armor, dodge, acc, crit;
    public Image portrait;
    private Unit tempUnit=null;
    private bool hidden=false;
	// Use this for initialization
	void Start () {
        ShowHide(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(UnitManager.instance.CurrentUnit!=null && hidden)
        {
            if (UnitManager.instance.CurrentUnit.tag == "Player" /*&& UnitManager.instance.CurrentUnit != tempUnit*/)
            {
                tempUnit = UnitManager.instance.CurrentUnit;
                ShowHide(true);
                FillUI(tempUnit);
            }
        }
        else if(UnitManager.instance.CurrentUnit == null & !hidden)
        {
            ShowHide(false);
        }
	}

    void ShowHide(bool b)
    {
        hidden = !b;
        GetComponent<Image>().enabled = b;
        for (int i=0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(b);
        }


    }

    void FillUI(Unit unit)
    {
        var stats = unit.stats;
        hP.text =stats.HP + "/"+stats.maxHP;
        Damage.text =stats.damageRange.min + " - " + stats.damageRange.max;
        unitName.text =unit.name;
        unitClass.text = unit.unitClass;
        move.text = unit.stats.moveSpeed.ToString();
        armor.text = stats.defence +"%";
        dodge.text =stats.dodge.ToString();
        acc.text =stats.accuracy.ToString();
        crit.text = stats.critChance+"% - " + stats.critMod+"X";
        portrait.sprite = unit.unitIcon;
    }

    public void ShowHide()
    {
        if (transform.localScale.y > 0)
        {
            transform.localScale= new Vector3(1, -1, 1);
        }
        else
        {
            transform.localScale = Vector3.one;

        }
    }
}
