﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class FungusController : MonoBehaviour
{
    public static FungusController instance;

    public Flowchart flowChart;

	// Use this for initialization
	void Start ()
    {
        if (instance)
            Debug.LogError("Multiple instances of FungusController detected");

        instance = this;

        flowChart = GetComponent<Flowchart>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //if (stage)
            //StartCoroutine(SkipBlock());
            //    stage.gameObject.SetActive(false);
            //flowChart.StopAllBlocks();
            //if (flowChart.SelectedBlock.ActiveCommand.IsExecuting)
            //{
            //    for (int i = flowChart.SelectedBlock.ActiveCommand.CommandIndex - 1; i < flowChart.SelectedBlock.CommandList.Count; i++)
            //    {
            //        Debug.Log(flowChart.SelectedBlock.CommandList[i].);

            //        if (i != flowChart.SelectedBlock.CommandList.Count - 1)
            //        {
            //            flowChart.SelectedBlock.ActiveCommand.Continue();
            //        }
            //        else
            //            flowChart.SelectedBlock.Stop();

            //        flowChart.SelectedBlock.ActiveCommand.Continue();
            //    }
            //}

            StartCoroutine(Skip());

        }
    }

    public void SkipDialog()
    {
        StartCoroutine(Skip());
    }

    IEnumerator Skip()
    {
        if (SayDialog.ActiveSayDialog != null && SayDialog.ActiveSayDialog.isActiveAndEnabled)
        {
            flowChart.SetBooleanVariable("skip", true);
            flowChart.SetBooleanVariable("Skip", true);
            if(UnitManager.instance)
            UnitManager.instance.DisableInputLock();
            yield return new WaitForSecondsRealtime(3);
            flowChart.SetBooleanVariable("skip", false);
            flowChart.SetBooleanVariable("Skip", false);
        }
    }
}
