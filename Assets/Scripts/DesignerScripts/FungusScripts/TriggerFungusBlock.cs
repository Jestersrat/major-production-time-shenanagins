﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[System.Serializable]
public enum TriggerType
{
    Collider,
    Objective,
    UnitDeath,
    TurnCounter
}

public class TriggerFungusBlock : MonoBehaviour
{

    public Flowchart flowChart;
    public Stage stage;
    public string blockName;
    public bool triggeronce = true;
    public TriggerType type;
    public int hideAfter;
    public GameObject otherObject;
    [Header("UnitDeaths")]
    public Unit[] units;

    [Header("TurnCounter")]
    public int turnNumber;

    [Header("Objective")]
    public ObjectiveBase[] objective;
    private bool triggered = false;

    void OnTriggerEnter(Collider other)
    {
        if (type == TriggerType.Collider) 
        {
            if (other.tag == "Player")
            {
                ExecuteBlock();
            }
        }
    }

    public void Start()
    {
        if (flowChart == null)
        {
            flowChart = FindObjectOfType<Flowchart>();
        }
        if (type == TriggerType.TurnCounter || hideAfter > 0)
        {
            TurnManager.instance.OnEndTurn.AddListener(OnEndTurn);
        }
    }

    public void ExecuteBlock()
    {
        if (triggeronce && !triggered || !triggeronce)
        {
            flowChart.ExecuteBlock(blockName);
            triggered = true;
        }
    }

    //private IEnumerator SkipBlock()
    //{
    //    int i = flowChart.SelectedBlock.ActiveCommand.ItemId;
    //    Debug.Log(i);
    //    while(i< flowChart.SelectedBlock.CommandList.Count)
    //    {
    //        flowChart.SelectedBlock.CommandList[i].Execute();
    //        i++;
    //        yield return null;
    //    }
    //    flowChart.SelectedBlock.Stop();
    //}


    void OnEndTurn(UnitType type)
    {
        if (type == UnitType.Player)
        {
            if (TurnManager.instance.turnNumber == turnNumber)
            {
                ExecuteBlock();
            }
            else if (TurnManager.instance.turnNumber == hideAfter)
            {
                gameObject.SetActive(false);
                if (otherObject != null)
                {
                    otherObject.SetActive(true);
                }
            }
        }
    }


    IEnumerator CheckDeaths()
    {
        bool complete = false;
        while (!complete)
        {
            for (int i = 0; i < units.Length; i++)
            {
                if (units[i] != null || !units[i].isDead)
                {
                    complete = false;
                    Debug.Log(complete);
                    break;
                }
                else { complete = true; }
            }
            if (complete)
            {
                ExecuteBlock();
                StopCoroutine(CheckDeaths());
            }
            yield return new WaitForSeconds(2.0f);
        }
        yield return null;
    }

    IEnumerator CheckObjective()
    {
        return null;
    }


}
