﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipStatic : MonoBehaviour
{
    public static TooltipStatic instance=null;
    //makes this script a static , accesible by all.
    private void Awake()
    {
        if (instance != this /*|| !instance.gameObject.activeInHierarchy*/)
        {
            instance = this;
        }
        //else if (instance != this)
        //{
        //    Destroy(gameObject);
        //}
    }
    private Vector2 toolTipSize = new Vector2(0, 0);
    private Vector3 hideyHole;
    public float paddingX = 10, paddingY = 10;
    public bool active = false;
    private Text toolTip;
    public float hoverTime=0.5f;
    public float cursorPixels=10;
    public bool staticTooltip=true;
    private RectTransform rect;
    private Vector2 anchorMax = Vector2.zero;
    private Vector2 anchorMin = Vector2.zero;

    // Use this for initialization
    void Start ()
	{
        // repeating this check in start, to allow for more resilient hand overs, incase of out of order awakes etc.
        if (instance != this /*|| !instance.gameObject.activeInHierarchy*/)
        {
            instance = this;
        }
        toolTip = GetComponentInChildren<Text>();
		toolTip.transform.parent.gameObject.layer = 2;
		toolTip.gameObject.layer = 2;
        rect = GetComponent<RectTransform>();
        hideyHole = rect.transform.position;
        cursorPixels *= transform.parent.localScale.x;
        transform.SetAsLastSibling();
        transform.position = hideyHole;
    }
    void Update()
    {
        if (instance!=this && !instance.gameObject.activeInHierarchy)
        {
            instance = this;
        }
    }

    public void ShowTooltip(string tooltipText)
    {
        toolTip.text = tooltipText;
        float xPos = 0, yPos = 0;
        toolTipSize= RegenerateScale();
        if (!staticTooltip) {
            if (Input.mousePosition.x >= Screen.width * 0.5f)
            {
                anchorMax.y = 1;
                anchorMin.y = 1;
                xPos = Input.mousePosition.x - toolTip.GetComponent<RectTransform>().sizeDelta.x * 0.5f - cursorPixels ;
            }
            else {
                anchorMin.y = 0;
                anchorMax.y = 0;
                xPos = Input.mousePosition.x + toolTip.GetComponent<RectTransform>().sizeDelta.x * 0.5f + cursorPixels ;
            }
            if (Input.mousePosition.y >= Screen.height * 0.5f)
            {
                anchorMax.x = 1;
                anchorMin.x = 1;
                yPos = Input.mousePosition.y - toolTip.GetComponent<RectTransform>().sizeDelta.y * 0.5f - cursorPixels ;
            }
            else {
                anchorMax.x = 0;
                anchorMin.x = 0;
                yPos = Input.mousePosition.y + toolTip.GetComponent<RectTransform>().sizeDelta.y * 0.5f + cursorPixels ;
            }
            rect.anchorMax = anchorMax;
            rect.anchorMin = anchorMin;
            rect.transform.position = new Vector3(xPos, yPos, 0);
        }
        else
        {
            rect.anchoredPosition = Vector3.zero;
        }
    }

    public void HideToolTip()
    {
        rect.transform.position = hideyHole;
    }


    public Vector2 RegenerateScale()
    {
        GetComponentInChildren<ContentSizeFitter>().SetLayoutHorizontal();
        GetComponentInChildren<ContentSizeFitter>().SetLayoutVertical();
        toolTipSize = new Vector2(0, 0);
        foreach (Transform child in transform)
        {
            if (toolTipSize.x < child.GetComponent<RectTransform>().sizeDelta.x)
            {
                toolTipSize.x = child.GetComponent<RectTransform>().sizeDelta.x;
            }
            toolTipSize.y = child.GetComponent<RectTransform>().sizeDelta.y;
        }
        toolTipSize.x += paddingX * 2;
        toolTipSize.y += paddingY * 2;

        GetComponent<RectTransform>().sizeDelta = toolTipSize;
        return toolTipSize;
    }
}
