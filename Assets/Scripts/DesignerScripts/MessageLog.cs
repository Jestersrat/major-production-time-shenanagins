﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageLog : MonoBehaviour {
    public static MessageLog instance=null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    
    private Text textLog;
    private List<string> eventLog = new List<string>();
    public int maxLines = 10;
    public float fadeTime = 10;
    public bool fade = true;
    public int sizePerLine=13;
    public float backgroundPadding = 5;
    private float timer;
    private ContentSizeFitter sizeFitter;
    public ContentSizeFitter childSizeFitter;
    private bool hidden = false;
    public GameObject hider;

    // Use this for initialization
    void Start ()
	{
        textLog = GetComponentInChildren<Text>();
        textLog.text = "";
        sizeFitter = GetComponent<ContentSizeFitter>();
        childSizeFitter = transform.Find("Text").GetComponent<ContentSizeFitter>();
        //textLog.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(200, sizePerLine * eventLog.Count);
        //GetComponent<RectTransform>().sizeDelta = new Vector2(300, sizePerLine * eventLog.Count + backgroundPadding);
    }
	
	// Update is called once per frame
	void Update () {
		if (eventLog.Count > 0 && fade)
        {
            if (timer>fadeTime)
            {
                TrimLog();
                timer = 0;
            }
            timer += Time.deltaTime;
        }
        if (eventLog.Count == 0 && !hidden) 
        {
            GetComponent<Image>().enabled = false;
            hider.SetActive(false);
        }
	}

    public void AddEvent(string message)
    {
        eventLog.Add( "<b>Turn " + TurnManager.instance.turnNumber + "</b>: "+message);
        GetComponent<Image>().enabled = true;
        hider.SetActive(true);
        if (eventLog.Count >= maxLines)
        {
            eventLog.RemoveAt(0);
        }
        if (!textLog)
        {
            textLog = GetComponentInChildren<Text>();
        }
        else
        {
            textLog.text = "";
            for (int i = eventLog.Count - 1; i >= 0; i--)
            {
                textLog.text += eventLog[i];
                textLog.text += "\n";
            }
        }
        childSizeFitter.SetLayoutVertical();
        sizeFitter.SetLayoutHorizontal();
        sizeFitter.SetLayoutVertical();

    }

    public void RefreshSize()
    {
        textLog.text = "";
        for (int i = eventLog.Count - 1; i >= 0; i--)
        {
            textLog.text += eventLog[i];
            textLog.text += "\n";
        }
        childSizeFitter.SetLayoutVertical();
        sizeFitter.SetLayoutHorizontal();
        sizeFitter.SetLayoutVertical();
    }


    private void TrimLog()
    {
        eventLog.RemoveAt(0);
        RefreshSize();
        //GetComponent<RectTransform>().sizeDelta=new Vector2(200, sizePerLine * eventLog.Count);
        //textLog.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(180, sizePerLine * eventLog.Count);
    }
}
