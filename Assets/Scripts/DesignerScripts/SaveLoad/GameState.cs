﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class GameState : MonoBehaviour {
    public static GameState instance;
    public string sceneName;
    public List<Unit> units = new List<Unit>();

    void OnEnable()
    {
        SceneManager.sceneLoaded += LevelWasLoaded;
    } 

    void OnDisable()
    {
        SceneManager.sceneLoaded -= LevelWasLoaded;
    }


    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else { Destroy(this); }
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5) && TurnManager.instance.currentTeam == UnitType.Player)
        {
            //UpdateData();
            SaveLoad.instance.SaveData();
        }
        if (Input.GetKeyDown(KeyCode.F9))
        {
            SaveLoad.instance.LoadData();
            SaveLoad.instance.isSceneLoading = true;
            SceneManager.LoadScene(SaveLoad.instance.allObjects.generalData.sceneName);
        }
    }

    private void LevelWasLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SaveLoad.instance != null && SaveLoad.instance.isSceneLoading)
        {
            AllObjects allObjects = SaveLoad.instance.allObjects;
            Unit[] units = FindObjectsOfType<Unit>();
            for (int i=0; i < units.Length; i++)
            {
                units[i].gameObject.SetActive(false);
                Destroy(units[i].gameObject);

            }

            foreach (LevelUnits unit in allObjects.levelUnits)
            {
                Unit newUnit = Instantiate(StaticValues.instance.UnitPrefabs[unit.prefabNumber]).GetComponent<Unit>();
                newUnit.stats = unit.unitStats;
                if (newUnit.stats.HP <= 0)
                {
                    newUnit.isDead = true;
                }
                newUnit.transform.position = unit.pos;
                Debug.Log(unit.rot);
                newUnit.transform.rotation = unit.rot;
                foreach (Transform child in newUnit.transform)
                {
                    child.transform.rotation = unit.rot;
                    //Debug.Log(unit.rot.ToString());
                }
            }
            TurnManager.instance.ReInitializeLists();
            SaveLoad.instance.isSceneLoading = false;
        }
    }

}
