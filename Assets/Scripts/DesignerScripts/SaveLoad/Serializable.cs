﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;


[Serializable]
public class GeneralData  {

    public string sceneName;

}

[Serializable]
public class LevelUnits
{
    public int prefabNumber;
    public Vector3 pos;
    public Quaternion rot;
    public Stats unitStats;
    public UnitType unitType;
    public int[] activeAbilities;
}

[Serializable]
public class PlayerUnits
{
    
}

[Serializable]
public class AllObjects
{
    public List<PlayerUnits> playerUnits;
    public List<LevelUnits> levelUnits;
    public GeneralData generalData;
    public AllObjects()
    {
        playerUnits = new List<PlayerUnits>();
        levelUnits = new List<LevelUnits>();
        generalData = new GeneralData();
    }
}
#region custom_serializers

sealed class Vector3SerializationSurrogate : ISerializationSurrogate
{
    // Method called to serialize a Vector3 object
    public void GetObjectData(System.Object obj,
                              SerializationInfo info, StreamingContext context)
    {

        Vector3 v3 = (Vector3)obj;
        info.AddValue("x", v3.x);
        info.AddValue("y", v3.y);
        info.AddValue("z", v3.z);
    }

    // Method called to deserialize a Vector3 object
    public System.Object SetObjectData(System.Object obj,
                                       SerializationInfo info, StreamingContext context,
                                       ISurrogateSelector selector)
    {

        Vector3 v3 = (Vector3)obj;
        v3.x = (float)info.GetValue("x", typeof(float));
        v3.y = (float)info.GetValue("y", typeof(float));
        v3.z = (float)info.GetValue("z", typeof(float));
        obj = v3;
        return obj;   // Formatters ignore this return value //Seems to have been fixed!
    }
}


sealed class QuaternionSerializationSurrogate : ISerializationSurrogate
{
    public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context)
    {
        Quaternion q4 = (Quaternion)obj;
        info.AddValue("x", q4.x);
        info.AddValue("y", q4.y);
        info.AddValue("z", q4.z);
        info.AddValue("w", q4.w);
    }

    public System.Object SetObjectData(System.Object obj,SerializationInfo info,StreamingContext context, ISurrogateSelector selector)
    {
        Quaternion q4 = (Quaternion)obj;
        q4.x = (float)info.GetValue("x", typeof(float));
        q4.y = (float)info.GetValue("y", typeof(float));
        q4.z = (float)info.GetValue("z", typeof(float));
        q4.w = (float)info.GetValue("w", typeof(float));
        obj = q4;
        return obj;
    }
}


#endregion
#region broken_custom_serializer


[System.Serializable]
public struct SerializableQuaternion//Doesnt currently work
{
    public float x;
    public float y;
    public float z;
    public float w;
    public SerializableQuaternion(float rX, float rY, float rZ, float rW)
    {
        x = rX;
        y = rY;
        z = rZ;
        w = rW;
    }

    public override string ToString()
    {
        return String.Format("[{0},{1},{2},{3}]", x, y, z, w);
    }
    /// <summary>
    /// Converts a string to a quaternian(If format is right)
    /// </summary>
    /// <param name="rValue"></param>
    public static implicit operator Quaternion(SerializableQuaternion rValue)
    {
        return new Quaternion(rValue.x, rValue.y, rValue.z, rValue.w);
    }

    public static implicit operator SerializableQuaternion(Quaternion rValue)
    {
        return new SerializableQuaternion(rValue.x, rValue.y, rValue.z, rValue.w);
    }
}


[System.Serializable]
public struct SerializableVector3 // Doesnt Currently Work
{
    public float x;
    public float y;
    public float z;

    public SerializableVector3(float rX, float rY, float rZ)
    {
        x = rX;
        y = rY;
        z = rZ;
    }

    public override string ToString()
    {
        return String.Format("[{0}, {1}, {2}]", x, y, z);
    }

    public static implicit operator Vector3(SerializableVector3 rValue)
    {
        return new Vector3(rValue.x, rValue.y, rValue.z);
    }
    public static implicit operator SerializableVector3(Vector3 rValue)
    {
        return new SerializableVector3(rValue.x, rValue.y, rValue.z);
    }
}

#endregion