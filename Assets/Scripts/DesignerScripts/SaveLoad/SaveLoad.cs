﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;//Allows creation of files on device
using UnityEngine.SceneManagement;

public class SaveLoad :MonoBehaviour{
    public static SaveLoad instance;
    public AllObjects allObjects;
    public bool isSceneLoading { get; set; }

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Destroying extra SaveLoad");
            Destroy(gameObject);
        }
    }

    public void SaveData()
    {
        if (!Directory.Exists("Saves"))
        {
            Directory.CreateDirectory("Saves");
        }
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector ss = UpdateFormatter();
        formatter.SurrogateSelector = ss;
        FileStream saveFile = File.Create("Saves/save.chr");
        AllObjects savedObjects = UpdateData();
        formatter.Serialize(saveFile, savedObjects);
        saveFile.Close();
        Debug.Log("Save Complete");
    }

    public void LoadData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector ss = UpdateFormatter();
        formatter.SurrogateSelector = ss;
        FileStream saveFile = File.Open("Saves/save.chr", FileMode.Open);
        AllObjects savedObjects = (AllObjects)formatter.Deserialize(saveFile);
        //Debug.Log(savedObjects.ToString());
        saveFile.Close();
        //foreach (LevelUnits lUnits in savedObjects.levelUnits)
        //{

        //}
        allObjects = savedObjects;
        Debug.Log("Load Complete");
    }

    public AllObjects UpdateData()
    {
        AllObjects objects = new AllObjects() ;
        objects.levelUnits = new List<LevelUnits>();
        List<Unit> levelCurrentUnits = new List<Unit>();
        levelCurrentUnits.AddRange(FindObjectsOfType<Unit>());
        //Debug.Log(levelCurrentUnits.Count);
        foreach (Unit unit in levelCurrentUnits)
        {
            LevelUnits levelUnits = new LevelUnits();
            levelUnits.unitStats = unit.stats;
            levelUnits.unitType = unit.unitType;
            levelUnits.pos = unit.transform.position;
            levelUnits.rot = unit.transform.GetChild(1).transform.rotation;
            for (int i = 0; i < StaticValues.instance.UnitPrefabs.Length; i++)
            {
                if (unit.unitName == StaticValues.instance.UnitPrefabs[i].GetComponent<Unit>().unitName)
                {
                    levelUnits.prefabNumber = i;
                    Debug.Log(i+ "Prefab");
                    Debug.Log(unit.unitName);
                    break;
                }
            }
            //AbilityCore[] abilities=unit.transform.GetComponentsInChildren<AbilityCore>(true);
            //Debug.Log(abilities.Length);
            //levelUnits.activeAbilities= new int[abilities.Length];
            //Abilities save function will have to be talked over with Huy
            //for(int i = 0; i < abilities.Length; i++)
            //{
            //    if (abilities[i].)
            //}
            //Debug.Log(levelUnits.prefabNumber);
            objects.levelUnits.Add(levelUnits);
            Debug.Log(objects.levelUnits.Count);
        }
        objects.generalData.sceneName = SceneManager.GetActiveScene().name;
        Debug.Log(objects.generalData.sceneName);
        Debug.Log(objects.levelUnits.Count);
        return objects;
    }

    public SurrogateSelector UpdateFormatter()
    {
        SurrogateSelector ss = new SurrogateSelector();

        Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), v3ss);

        QuaternionSerializationSurrogate q4ss = new QuaternionSerializationSurrogate();
        ss.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), q4ss);
        return ss;
    }
}
