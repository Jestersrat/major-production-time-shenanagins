﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainStitcher : MonoBehaviour {
    public Terrain terrainTop, terrainBottom, terrainLeft, terrainRight;
	// Use this for initialization
	void Start () {
        GetComponent<Terrain>().SetNeighbors(terrainLeft, terrainTop, terrainRight ,terrainBottom);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
