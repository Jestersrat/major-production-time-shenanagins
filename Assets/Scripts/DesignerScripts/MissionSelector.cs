﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MissionSelector : MonoBehaviour
{
    public string levelName;
    private bool highlighted=false;
    private bool mouseOver = false;

    private List<Color> startColors = new List<Color>();
    public Color highLightColor=Color.cyan;

    public int squadSize = 4;
    public Unit[] requiredUnits;
    public GameObject[] recolorTargets;
    [Header("Mission Info")]
    public string missionName;
    [TextArea(3, 10)]
    public string description;
    public int trauma;
    public int inStability;
    public int difficulty;
    public string rewards;
    public string requiredMission;
    public Unit[] gainUnits;
    public string playFlowChart = "";
    public bool playedText = false;

    private int pointerID = -1; // Used by event system to check if mouse is over UI element. NFI how it works but it seems to, sorry Huy 

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < recolorTargets.Length; i++){
            startColors.Add(recolorTargets[i].GetComponent<Renderer>().material.color);
        }
        if(requiredMission!="" && !GameManager.instance.completedMissions.Contains(requiredMission))
        {
            gameObject.SetActive(false);
        }

        for (int i = 0; i < requiredUnits.Length; i++)
        {
            foreach (var unit in GameManager.instance.units)
            {
                if (unit.name == requiredUnits[i].name)
                {
                    requiredUnits[i] = unit;
                    break;
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!highlighted)
        {
            if (mouseOver)
            {
                highlighted = true;
                foreach (GameObject obj in recolorTargets)
                {
                    //obj.GetComponent<Renderer>().material.shader = shader;
                    //obj.GetComponent<Renderer>().material.SetColor("_OutlineColor", highLightColor);
                    obj.GetComponent<Renderer>().material.color = highLightColor;
                }
            }
        }
        if (highlighted)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                MissionInfo.instance.FillInfo(this);
            }
        }
	}

    private void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject(pointerID))
        {
            mouseOver = true;
        }
    }

    void OnMouseOver()
    {
        if(mouseOver!=true && !EventSystem.current.IsPointerOverGameObject(pointerID))
        {
            mouseOver = true;
        }
    }

    private void OnMouseExit()
    {
        mouseOver = false;
        if (highlighted)
        {
            for (int i = 0; i < recolorTargets.Length; i++)
            {
                recolorTargets[i].GetComponent<Renderer>().material.color = startColors[i];
            }
            highlighted = false;
        }
    }

}
