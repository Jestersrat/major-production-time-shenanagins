﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIFiller : MonoBehaviour
{
    public static PlayerUIFiller instance;

    public GameObject unitUiPrefab;

    //private List<PlayerUnitUI> activeUnits;

    //Use this for initialization

    public List<PlayerUnitUI> unitUIs = new List<PlayerUnitUI>();

    void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        ReInitialize();
    }

    void OnEnable()
    {
        ReInitialize();
    }

    public void ReInitialize()
    {
        for (int i = unitUIs.Count - 1; i >= 0; i--)
        {
            if (!unitUIs[i])
                unitUIs.RemoveAt(i);
        }

        // creates more entries if required
        if(unitUIs.Count < TurnManager.instance.playerUnits.Count)
        {
            for (int i = unitUIs.Count; i < TurnManager.instance.playerUnits.Count; i++)
            {
                unitUIs.Add(Instantiate(unitUiPrefab, gameObject.transform).GetComponent<PlayerUnitUI>());
            }
        }

        for (int i = 0; i < unitUIs.Count; i++)
        {
            if (i < TurnManager.instance.playerUnits.Count)
            {
                // Setting units to unit ui 
                unitUIs[i].Initialize(TurnManager.instance.playerUnits[i]);
                unitUIs[i].gameObject.SetActive(true);
            }
            else
            {
                // Disable if no unit to set
                unitUIs[i].gameObject.SetActive(false);
            }
        }
    }
}
