﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingHealthBars : MonoBehaviour
{
    private Slider slider;
    public Unit unit = null;
    public bool hidden = false;
    public Image fill;
    public Vector3 barPos = Vector3.up * 3;

    private Camera mainCamera;
    private Color orange = new Color(1f, 0.5f, 0f);
    private FogOfWarRevealer fogOfWar;

    public float hitChance;
    public Text hitText;

    public GameObject APPips;
    public GameObject shields;

    private bool playerUnit;
    
    private float lastAP;
    private float lastDefence;

    void Start()
    {
        mainCamera = Camera.main;
        slider = GetComponent<Slider>();
        slider.maxValue = unit.stats.maxHP;
        slider.value = unit.stats.HP;
        transform.position = unit.transform.position + barPos;
        hitText.transform.parent.gameObject.SetActive(false);
        if (unit.CurrentTile.fog != null)
        {
            if (unit.CurrentTile.fog.revealed)
            {
                hidden = true;
            }
            else
            {
                hidden = false;
                foreach (Transform child in transform)
                {
                    child.gameObject.SetActive(true);
                }
            }
        }
        slider.onValueChanged.AddListener(delegate { UpdateBar(); });
        foreach (Transform child in shields.transform)
        {
            child.gameObject.SetActive(false);
        }
        if (unit.tag == "Player")
        {
            playerUnit = true;
        }
        else
        {
            foreach (Transform child in APPips.transform)
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    void UpdateBar()
    {
        if (playerUnit)
        {
            if (slider.value / slider.maxValue < 0.4f)
            {
                fill.color = orange;
            }
            else if (slider.value / slider.maxValue < 0.7f)
            {
                fill.color = Color.yellow;
            }
            else { fill.color = Color.green; }
        }
        else { fill.color = Color.red; }
    }

    public void UpdateIcons()
    {
        if (playerUnit)
        {
            for (int i = 0; i < 9; i++)
            {
                if (i <= unit.stats.actionPoints - 1)
                {
                    APPips.transform.GetChild(i).GetComponent<Image>().color = Color.white;
                }
                else
                {
                    APPips.transform.GetChild(i).GetComponent<Image>().color = Color.black;
                }
            }
        }
        float def = unit.stats.defence * 0.1f;
        foreach (Transform child in shields.transform)
        {
            child.gameObject.SetActive(false);
        }

        if (def > 0)
        {
            if (def > 90)
                def = 90;

            for (int i = 0; i < Mathf.Abs(def); i++)
            {
                shields.transform.GetChild(i).gameObject.SetActive(true);
                shields.transform.GetChild(i).GetComponent<Image>().color = Color.white;
            }
        }
        else
        {
            if (def < -90)
                def = -90;

            for (int i = 0; i < Mathf.Abs(def); i++)
            {
                shields.transform.GetChild(i).gameObject.SetActive(true);
                shields.transform.GetChild(i).GetComponent<Image>().color = Color.black;
            }
        }
    }

    void OnDisable()
    {
        Destroy(gameObject);
    }

    void Update()
    {
        fogOfWar = unit.CurrentTile.fog;

        if (fogOfWar != null && fogOfWar.revealed && hitChance > 0)
        {
            hitText.text = hitChance + "%";
            hitText.transform.parent.gameObject.SetActive(true);
        }
        else
        {
            hitText.text = "";
            hitText.transform.parent.gameObject.SetActive(false);
        }
        if (unit.stats.HP < unit.stats.maxHP && fogOfWar != null && fogOfWar.revealed)
        {            
            if (lastAP != unit.stats.actionPoints || lastDefence != unit.stats.defence)
            {
                lastAP = unit.stats.actionPoints;
                lastDefence = unit.stats.defence;
                UpdateIcons();
            }
            foreach (Transform child in transform)
            {
                if (child.name != "Panel" /*&& child!=shields.transform && child!=APPips.transform*/ )
                {
                    child.gameObject.SetActive(true);
                }
            }
            slider.maxValue = unit.stats.maxHP;
            slider.value = unit.stats.HP;
            transform.position = unit.transform.position + barPos;
            transform.forward = -(mainCamera.transform.forward);
        }
        else
        {
            foreach (Transform child in transform)
            {
                if (child.name != "Panel")
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
        if (hitText.transform.parent.gameObject.activeInHierarchy)
        {
            slider.value = unit.stats.HP;
            transform.position = unit.transform.position + barPos;
            transform.forward = -(mainCamera.transform.forward);
        }
    }

}
