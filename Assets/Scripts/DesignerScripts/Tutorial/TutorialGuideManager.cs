﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGuideManager : MonoBehaviour {
    public GameObject[] tutorials;
    private int currentTutorial=0;
    // Use this for initialization
    private void Start()
    {
        foreach (TutorialGuidePanel tutorial in transform.GetComponentsInChildren<TutorialGuidePanel>(true))
        {
            tutorial.tutorialGuideManager = this;
        }
        if (tutorials.Length >= 1)
        {
            tutorials[0].SetActive(true);
            UnitManager.instance.EnableInputLock();
        }
    }

    public void NextTutorial()
    {
        currentTutorial++;
        if (currentTutorial < tutorials.Length)
        {
            tutorials[currentTutorial - 1].SetActive(false);
            tutorials[currentTutorial].SetActive(true);
        }
        else
        {
            EndGuide();
        }
    }
    public void LastTutorial()
    {
        if (currentTutorial - 1 >= 0)
        {
            tutorials[currentTutorial].SetActive(false);
            currentTutorial--;
            tutorials[currentTutorial].SetActive(true);
        }
    }

    public void EndGuide()
    {
        UnitManager.instance.DisableInputLock();
        gameObject.SetActive(false);
    }

}
