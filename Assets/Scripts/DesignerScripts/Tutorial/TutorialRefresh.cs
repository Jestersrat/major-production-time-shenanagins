﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialRefresh : TutorialPopup {

    public override void PopUp() 
    {
        for (int i=0; i < TurnManager.instance.playerUnits.Count; i++)
        {
            
            TurnManager.instance.playerUnits[i].stats.HP = TurnManager.instance.playerUnits[i].stats.maxHP;
            TurnManager.instance.playerUnits[i].ApplyDamage(TurnManager.instance.playerUnits[i].stats.HP - TurnManager.instance.playerUnits[i].stats.maxHP, TurnManager.instance.playerUnits[i]);
            foreach (AbilityBase ability in TurnManager.instance.playerUnits[i].abilities)
            {
                if (ability.coolDownCounter > 0)
                {
                    ability.coolDownCounter = 0;
                }
                if (ability.hasCharges)
                {
                    ability.charges++;
                }
            }
        }
        popUp.SetActive(true);
        text.text = popUpText;
        Destroy(gameObject);
    }


}
