﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEnemyTurn : MonoBehaviour {
    public GameObject enemyTurn;
	// Use this for initialization
	void Start () {
        TurnManager.instance.OnStartTurn.AddListener(ShowHideDisplay);
        ShowHideDisplay(UnitType.Player);
	}
	

    void ShowHideDisplay(UnitType type)
    {
        if (type != UnitType.Player)
        {
            enemyTurn.SetActive(true);
        }

        else { enemyTurn.SetActive(false); }
    }
}
