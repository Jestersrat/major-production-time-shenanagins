﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSpiderKiller : MonoBehaviour {
    private Unit unit;
    public StatusEffects status;
	// Use this for initialization
	void Start () {
        unit = GetComponent<Unit>();
	}
	
	// Update is called once per frame
	void Update () {
        if (unit.statuses.Count>0)
        {
            for (int i = 0; i < unit.statuses.Count; i++)
            {
                if (unit.statuses[i].name == status.name)
                {
                    if (!unit.isDead)
                    {
                        Debug.Log("spiderkiller On");
                        unit.ApplyDamage(100, unit);
                    }
                    else { Destroy(this); }
                }
            }
            
        }
	}
}
