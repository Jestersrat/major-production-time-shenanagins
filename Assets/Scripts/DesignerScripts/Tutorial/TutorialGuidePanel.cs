﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StepRequirement
{
    NoRequirement,
    UnitSelected,
    UnitDead,
    PointReached
}

public class TutorialGuidePanel : MonoBehaviour {
    public StepRequirement requirement;
    public GameObject targetLocation;
    public Unit[] killTarget;
    public Unit selectTarget;
    public Button forwards;
    public TutorialGuideManager tutorialGuideManager;
    public bool autoNextTip;
    public bool disableLock;
    public bool enableLock;

    public void CompleteStep()
    {
        if (autoNextTip)
        {
            tutorialGuideManager.NextTutorial();
        }
        forwards.interactable = true;
        this.enabled = false;
    }
	void OnEnable()
    {
        if (disableLock)
        {
            UnitManager.instance.DisableInputLock();
        }
        else if (enableLock)
        {
            UnitManager.instance.EnableInputLock();
        }
    }
	// Update is called once per frame
	void Update () {
        switch (requirement)
        {
            case StepRequirement.PointReached:
                if (Vector3.Distance(selectTarget.transform.position, targetLocation.transform.position) < 2f)
                {
                    CompleteStep();
                }
                break;
            case StepRequirement.UnitSelected:
                if (UnitManager.instance.CurrentUnit!=null && UnitManager.instance.CurrentUnit.unitSaveID == selectTarget.unitSaveID)
                {
                    CompleteStep();
                }
                break;
            case StepRequirement.UnitDead:
                //foreach (Unit target in killTarget)
                //{
                //    if (target.isDead)
                //    {
                //        CompleteStep();
                //        break;
                //    }
                //}
                Debug.Log("Dont use the kill requirement, just make it auto because reasons for now");
                break;
            default:
                CompleteStep();
                break;
        }
	}
}
