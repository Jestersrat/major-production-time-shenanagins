﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialStartSpawns : MonoBehaviour
{
    public UnitSpawner[] spawners;
    private Agent unit;
	// Use this for initialization
	void Start () {
        unit = GetComponent<Agent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (unit.active)
        {
            Debug.Log("ActivatingSpawns");
            foreach (UnitSpawner spawn in spawners)
            {
                spawn.Activate();
            }
            Destroy(this);
        }
	}
}
