﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPopup : MonoBehaviour {
    public static GameObject popUp;
    [TextArea(3, 10)]
    public string popUpText;
    public static Text text;
    public bool enemyMayTrigger = false;
	// Use this for initialization
	void Start () {
        if (popUp == null)
        {
            popUp = GameObject.Find("TutorialPanelBase");            
        }
        if (text == null)
        {
            text = popUp.transform.Find("TutText").GetComponent<Text>();
        }
        popUp.SetActive(false);
	}
	
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PopUp();
        }
        else if(enemyMayTrigger && other.tag == "Enemy")
        {
            PopUp();
        }
    }

    public virtual void PopUp()
    {
        popUp.SetActive(true);
        text.text = popUpText;
        Destroy(gameObject);
    }
}
