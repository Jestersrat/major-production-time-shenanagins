﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SlotStatus
{
    Locked,
    Unlocked
}

[System.Serializable]
public class BuildingSlot
{
    [SerializeField]
    public SlotStatus status;
    public float cost;

    public Building building = null;
}
