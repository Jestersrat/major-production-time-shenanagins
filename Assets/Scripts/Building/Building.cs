﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Building", menuName = "Building/Building", order = 1)]
public class Building : ScriptableObject
{
    public int level = 0;

    public float levelUpCost = 0;

    public List<BuildingEffect> effects = new List<BuildingEffect>();

    public float cost;

    public int buildTime;

    [HideInInspector]
    public int buildProgress = 0;

    public Sprite icon;

    public void ApplyEffects()
    {
        foreach(var effect in effects)
        {
            effect.ApplyEffect(level);
        }
    }
}
