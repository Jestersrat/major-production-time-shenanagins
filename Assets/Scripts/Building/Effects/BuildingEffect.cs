﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum BonusType
{
    Passive,
    PerDay
}

[System.Serializable]
public enum GrowthType
{
    Multiplier,
    Static
}

[CreateAssetMenu(fileName = "BuildingEffect", menuName = "Building/Building Effect/Base Effect", order = 1)]
public class BuildingEffect : ScriptableObject
{
    [SerializeField]
    public BonusType bonusType;

    [SerializeField]
    public GrowthType growthType;

    public float growthValue;

    [Tooltip("Tick Rate in days")]
    public int tickRate = 1;

    protected int tickCounter = 0;

    public virtual void ApplyEffect(int buildingLevel)
    {

    } 
}
