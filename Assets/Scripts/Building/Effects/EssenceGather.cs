﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildingEffect", menuName = "Building/Building Effect/Essence Gather Effect", order = 1)]
public class EssenceGather : BuildingEffect
{
    public float essenceGain;

    public override void ApplyEffect(int buildingLevel)
    {
        if (bonusType == BonusType.PerDay)
        {
            if (++tickCounter >= tickRate)
            {
                var gain = essenceGain;
                if (growthType == GrowthType.Multiplier)
                {

                }
                else
                {
                    gain += growthValue * buildingLevel;
                }

                GameManager.instance.playerEssence += gain;
                tickCounter = 0;
            }
        }
    }
}
