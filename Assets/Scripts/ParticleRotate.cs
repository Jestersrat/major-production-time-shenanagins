﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRotate : MonoBehaviour {

    public Vector3 directon = Vector3.forward;
    // Use this for initialization

    public float speed = 1;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(directon * Time.deltaTime * speed);
		
	}
}
