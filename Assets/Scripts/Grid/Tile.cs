﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

[System.Serializable]
public class Tile : IHasNeighbours<Tile>
{
	public Tile(Vector2 point, CellInfo info)
	{
		location = point;
		Unobstructed = true;
		this.info = info;
		//cost = UnityEngine.Random.Range(1, 3);
		cost = 1;
        exitCost = 0;
	}
    
    public float cost;

    public float exitCost;

	Vector2 location;

	public CellInfo info;

	public bool Unobstructed
	{
		get;
		set;
	}

	public bool DestTile = false;

	public bool validStop = false;

	/// <summary>
    /// Returns empty true if there is no unit on tile or unit on tile is currently selected unit
    /// </summary>
	public bool Empty
	{
		get
		{
            return (info.unit == null || DestTile);// || info.unit == UnitManager.instance.CurrentUnit);// || (UAIManager.instance.currentAgent.currentTarget && UAIManager.instance.currentAgent.currentTarget == info.unit));
		}
	}
	public int X { get { return (int)location.x; } }

	public int Y { get { return (int)location.y; } }
	
	/// <summary>
    /// Neighbouring tiles within slope limit
    /// </summary>
	public IEnumerable<Tile> AllNeighbours { get; private set; }

	/// <summary>
    /// Neighbouring tiles ignoring slope limit
    /// </summary>
	public IEnumerable<Tile> ActualAllNeighbours { get; private set; }

	public IEnumerable<Tile> Neighbours
	{
		get
		{
            return AllNeighbours.Where(o => ((o.Unobstructed && o.info.Passable(UnitManager.instance.CurrentUnit)))); // || (o.info.unit == UnitManager.instance.CurrentUnit && o.Unobstructed))));
		}
	}

    public IEnumerable<Tile> UnobstructedNeighbours
    {
        get
        {
            return AllNeighbours.Where(o => o.Unobstructed);
        }
    }

	/// <summary>
	/// Returns reachable adjacent tiles given remaining Movement range
	/// </summary>
	public IEnumerable<Tile> ValidMove
	{
		get
		{
			return AllNeighbours.Where(o => o.Unobstructed && o.Empty && o.cost <= ((info.unit) ? info.unit.MoveRange : 2));
		}
	}

	/// <summary>
	/// Returns number of valid moves for given unit
	/// </summary>
	public int ValidMoveCount
	{
		get
		{
			int count = 0;

			foreach (var n in ValidMove)
				++count;

			return count;
		}
	}

	public IEnumerable<Tile> NeighboursForAI
	{
		get
		{
			// Additional checks to make sure AI don't stack up on tiles
			return AllNeighbours.Where(o => 
				((!o.DestTile && o.Unobstructed && o.info.Passable(UnitManager.instance.CurrentUnit))) || // && !validStop // stops ai from pathing into 
				(validStop && Empty && o.Unobstructed) || 
				(validStop && o.Empty && !o.DestTile && o.Unobstructed) || 
				(!validStop && o.DestTile) || 
                (o.info.unit == UnitManager.instance.CurrentUnit && Unobstructed));
		}
	}

	public List<Vector2> ValidNeighbours
	{
		get
		{
			if (GridManager.instance.gridType == GridType.Cube)
			{
				return new List<Vector2>
				{
					new Vector2(0, 1),
					new Vector2(0, -1),
					new Vector2(1, 0),
					new Vector2(-1, 0)
				};
			}
			else if (GridManager.instance.gridType == GridType.Hex)
			{				
				// neighbour coord offset due to grid shift upwards on odd x grids
				if(location.x % 2 == 0)
				return new List<Vector2>
				{
					new Vector2(0, 1),
					new Vector2(0, -1),
					new Vector2(1, 0),
					new Vector2(-1, 0),
					
					new Vector2(-1, -1),
					new Vector2(1, -1)
				};
				else
					return new List<Vector2>
				{
					new Vector2(0, 1),
					new Vector2(0, -1),
					new Vector2(1, 0),
					new Vector2(-1, 0),

					new Vector2(1, 1),
					new Vector2(-1, 1)
				};
			}
			return null;
		}
	}

	public void FindNeighbours(Dictionary<Vector2, CellInfo> map)
	{
		List<Tile> neighbours = new List<Tile>();
		List<Tile> actualNeighbours = new List<Tile>();
	
		foreach (var point in ValidNeighbours)
		{
			Vector2 neighbour = location + point;
			if(map.ContainsKey(neighbour))
			{
				// Checks height difference between neighbouring girds before adding
				if (Mathf.Abs(info.transform.position.y - map[neighbour].transform.position.y) < GridManager.instance.allowedSlope)
					neighbours.Add(map[neighbour].tile);

				// Connected regardless of slope
				actualNeighbours.Add(map[neighbour].tile);
			}
		}

		AllNeighbours = neighbours;
		ActualAllNeighbours = actualNeighbours;
    }

    /// <summary>
    /// Sets exit cost for adjacent tiles
    /// Use to increase cost of escaping from melee
    /// </summary>
    /// <param name="value"></param>
    public void AddNeighbourExitCost(float value)
    {
        foreach (var tile in AllNeighbours)
        {
            tile.exitCost += value;
            GridManager.instance.alteredTiles.Add(tile.info);
        }
    }

    /// <summary>
    /// Sets exit cost for this tile
    /// Use to increase cost of escaping from melee
    /// </summary>
    /// <param name="value"></param>
    public void AddExitCost(float value)
    {
        exitCost += value;
        GridManager.instance.alteredTiles.Add(info);
    }

    /// <summary>
    /// Sets exit cost to 0
    /// </summary>
    public void ResetExitCost()
    {
        foreach (var tile in AllNeighbours)
            exitCost = 0;
    }

	static public double distance(Tile tile1, Tile tile2)
	{
        // placeholder might need to change
        // posibily add cost to distance

        //Checks if leaving melee
        //And checks if acting unit has more than 1 action point
        //Increases cost if both are true
        if (tile1.exitCost > tile2.exitCost && (UnitManager.instance.CurrentUnit && UnitManager.instance.CurrentUnit.stats.actionPoints > 1))
            return tile2.cost + tile1.exitCost - tile2.exitCost;

        return tile2.cost;
	}

	static public double estimate(Tile tile, Tile destTile)
	{
		float distanceX = Mathf.Abs(destTile.X - tile.X);
		float distanceY = Mathf.Abs(destTile.Y - tile.Y);
		int z1 = -(tile.X + tile.Y);
		int z2 = -(destTile.X + destTile.Y);
		float distanceZ = Mathf.Abs(z2 - z1);

		return Mathf.Max(distanceX, distanceY, distanceZ);
	}
}
