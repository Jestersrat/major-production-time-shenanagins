﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleInfo : MonoBehaviour
{
	public bool canSeePast;

	// Use this for initialization
	void Start ()
	{
		transform.tag = "Obstacle";
	}
}
