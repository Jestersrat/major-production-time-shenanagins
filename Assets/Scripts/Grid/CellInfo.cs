﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellInfo : MonoBehaviour
{
	public Vector2 gridPos;

    [SerializeField]
	public Tile tile;

	public Unit unit;
	public ObstacleInfo obstacle;

    public FogOfWarRevealer fog;

    /// <summary>
    /// Returns True if there is no unit on tile
    /// </summary>
	public bool isEmpty
	{
		get
		{
			return ((unit == null));
		}
	}

	//public Color original;
	//public Color red;
	//public Color green;
	//public Color yellow;

	public Renderer rend;

	// Use this for initialization
	void Awake ()
	{
        Initialise();
    }

    void Initialise()
    {
        //Toby added this to allow for more unit flexibility
        if (GetComponent<Renderer>() != null)
        {
            rend = GetComponent<Renderer>();
        }
        else { rend = GetComponentInChildren<Renderer>(); }

        //original = rend.material.color;
        rend.enabled = false;

        fog = GetComponent<FogOfWarRevealer>();

        if (!fog)
            Debug.Log("Error with finding fog!~");
    }

    //void Update()
    //{
    //    if (tile.exitCost > 0)
    //    {
    //        var col = Color.magenta;
    //        col.a = tile.exitCost / 20f;
    //        rend.material.color = col;
    //        rend.material.SetColor("_EmissionColor", Color.magenta);
    //        rend.enabled = true;
    //    }
    //    else
    //        SetDefault();
    //}

    void OnEnable()
    {
        Initialise();
    }

    public void SetYellow()
    {
        if (!rend)
            return;

        var col = rend.material.color;

        if (rend.enabled)
            col = (col + GridManager.instance.yellow) * 0.5f;
        else
            col = GridManager.instance.yellow;

        rend.material.color = col;
        rend.material.SetColor("_EmissionColor", col);
        rend.enabled = true;
    }

    public void SetDefault()
	{
        if (!rend)
            return;

        rend.material.color = GridManager.instance.blue;
        rend.material.SetColor("_EmissionColor", Color.black);
        rend.enabled = false;        
	}

	public void SetGreen()
	{
        if (!rend)
            return;

        var col = rend.material.color;

        if (rend.enabled)
            col = (col + GridManager.instance.green) * 0.5f;
        else
            col = GridManager.instance.green;

        rend.material.color = col;
        rend.material.SetColor("_EmissionColor", col);
        rend.enabled = true;
    }

    public void SetRed()
	{
        var col = rend.material.color;

        if (rend.enabled)
            col = (col + GridManager.instance.red) * 0.5f;
        else
            col = GridManager.instance.red;

        rend.material.color = col;
        rend.material.SetColor("_EmissionColor", col);
        rend.enabled = true;
    }

    public void SetAOEColor()
    {
        if (!rend)
            return;

        var col = rend.material.color;

        var magenta = Color.magenta * Color.gray;
        magenta.a = GridManager.instance.red.a;

        if (rend.enabled)
        {
            col = (col + magenta) * 0.5f;
        }
        else
            col = magenta;

        rend.material.color = col;
        rend.material.SetColor("_EmissionColor", col);
        rend.enabled = true;
    }

	/// <summary>
	/// Sets color to w/e
	/// </summary>
	/// <param name="color"></param>
	public void SetColor(Color color)
	{
        if (!rend)
            return;

        color.a = GridManager.instance.red.a;

        var col = rend.material.color;

        if (rend.enabled)
            col = (col + color) * 0.5f;
        else
            col = color;

		rend.material.color = col;
		rend.material.SetColor("_EmissionColor", col);
		rend.enabled = true;
	}

    public bool Passable(Unit mover)
	{
		if (tile.Empty || mover == null)
			return true;

		if(mover.unitType == UnitType.Ally || mover.unitType == UnitType.Player)
		{
			if (unit.unitType == UnitType.Hostile)
				return false;

			return true;
		}

		if (mover.unitType == UnitType.Hostile)
		{
			if (unit.unitType == UnitType.Ally || unit.unitType == UnitType.Player)
				return false;			

			return true;
		}

		Debug.Log("something went wrong... cellinfo.Passable");
		return false;
	}
}
