﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GridType
{
	Cube,
	Hex
}

public class GridManager : MonoBehaviour
{
	public static GridManager instance = null;

	#region Grid Generation
	[Header("Grid Setup")]
	public GameObject gridPrefab;
	public float gridScale = 1f;
	[Tooltip("World xOffset for all tiles")]
	public float xOffset = 0;
	[Tooltip("World zOffset for all tiles")]
	public float zOffset = 0;
	[Tooltip("Maximum slope allowed between connecting grids")]
	public float allowedSlope = 1f;
	[Tooltip("Height to begin raycast down from")]
	public float rayHeight = 15;

	public Color blue;
	public Color red;
	public Color green;
	public Color yellow;

	public bool useTerrain;

	Vector3 scaleFactor;

	public GameObject groundBounds;

	public GameObject[] colliderObjects;

	public GridType gridType;

	public float gridWidth { get; private set; }
	float gridHeight;
	float gridHalfWidth;
	float gridHalfHeight;
	float groundWidth;
	float groundHeight;
	
	// Container for gridobjects objects
	GameObject gridContainer;
	#endregion

	// List grid points and related tile data
	public Dictionary<Vector2, CellInfo> map = new Dictionary<Vector2, CellInfo>();


	HashSet<CellInfo> traversable = new HashSet<CellInfo>();
	HashSet<CellInfo> obstacles = new HashSet<CellInfo>();

    public HashSet<CellInfo> alteredTiles = new HashSet<CellInfo>();

    //public HashSet<CellInfo> problemTiles = new HashSet<CellInfo>();

	void Awake()
	{
		instance = this;

        if(groundBounds == null)
        {
            groundBounds = GameObject.FindGameObjectWithTag("Terrain");
            if (groundBounds.GetComponent<Terrain>() != null)
            {
                useTerrain = true;
            }
        }
		scaleFactor = new Vector3(gridScale, gridPrefab.transform.localScale.y, gridScale);

		//groundBounds.layer = (int)Layer_Index.Terrain;
		SetSizes();
		CreateGrid();

		FindObstacles();
        
		foreach (var tile in map)
		{
			tile.Value.tile.FindNeighbours(map);
		}

        // Removes small patches if isolated tiles
        List<CellInfo> problemList = new List<CellInfo>();

        foreach (var tile in map)
        {
            var t = tile.Value;

            if (t.tile.Unobstructed && t.tile.ValidMoveCount < 3)
                problemList.Add(t);
        }

        while(problemList.Count > 0)
        {
            foreach (var cell in problemList)
            {
                var list = RingRangeUnobstructed(cell, 3, 0);
                list.Add(cell);

                if (list.Count < 3)
                {
                    foreach (var tile in list)
                    {
                        problemList.Remove(tile);
                        map.Remove(tile.gridPos);
                        Destroy(tile.gameObject);
                    }
                    list.Clear();
                }
                else
                {
                    foreach (var tile in list)
                    {
                        problemList.Remove(tile);
                    }
                }
                break;
            }

        }

        foreach (var tile in map)
        {
            tile.Value.tile.FindNeighbours(map);
        }
    }

	// Update is called once per frame
	void Update()
	{

	}

	// Calculates sizes of terrain and tiles
	void SetSizes()
	{
		//gridPrefab.transform.localScale.Scale(new Vector3(gridScale, 1, gridScale));
		var hexRend = (gridPrefab.GetComponent<Renderer>()) ? gridPrefab.GetComponent<Renderer>() : gridPrefab.GetComponentInChildren<Renderer>();
		// tries to get renderer from object
		// assume object is terrain if no renderer attached
		var groundRend = groundBounds.GetComponent<Renderer>();
		if(groundRend)
		{
			groundWidth = groundRend.bounds.size.x;
			groundHeight = groundRend.bounds.size.z;
		}
		else
		{
			var terrain = groundBounds.GetComponent<Terrain>();
			groundWidth = terrain.terrainData.size.x;
			groundHeight = terrain.terrainData.size.z;
		}

		gridWidth = hexRend.bounds.size.x * gridScale;
		gridHeight = hexRend.bounds.size.z * gridScale;

		gridHalfWidth = gridWidth * 0.5f;
		gridHalfHeight = gridHeight * 0.5f;
	}

	// Calculate number of rows and columns according to terrain size and grid size
	Vector2 CalcGridSize()
	{
		int col = 0;
		int row = 0;

		if (gridType == GridType.Cube)
		{
			col = (int)(groundWidth / gridWidth);
			row = (int)(groundHeight / gridHeight) + 1;
		}
		else if (gridType == GridType.Hex)
		{
			col = (int)(groundWidth / (gridWidth * 3 / 4)) + 1;
			row = (int)(groundHeight / gridHeight) + 1;
		}

		return new Vector2(col, row);
	}

	// Calculate starting position of first tile according to sizes
	Vector3 CalcInitPos()
	{
		Vector3 initPos;
		var groundRend = groundBounds.GetComponent<Renderer>();
		if (groundRend)
			initPos = new Vector3(-groundWidth / 2 + gridWidth / 2 + groundBounds.transform.position.x + xOffset, 0 + groundBounds.transform.position.y, groundHeight / 2 - gridWidth / 2 + groundBounds.transform.position.z + zOffset);
		else
		{
			initPos = new Vector3(gridWidth * 0.5f + groundBounds.transform.position.x + xOffset, 0 + groundBounds.transform.position.y, groundHeight - (gridHeight * 0.5f) + groundBounds.transform.position.z + zOffset); // half height offset seems to work better than half width for some reason
		}

		return initPos;
	}

	public Vector3 CalcWorldCoord(Vector2 gridPos)
	{
		Vector3 initPos = CalcInitPos();

		float x = 0;
		float z = 0;

		if (gridType == GridType.Cube)
		{
			x = initPos.x + gridPos.x * gridWidth;
			z = initPos.z - gridPos.y * gridHeight;
		}
		else if (gridType == GridType.Hex)
		{
			x = initPos.x + gridPos.x * gridWidth * 3 / 4;
			z = initPos.z - gridPos.y * gridHeight;
			if (gridPos.x % 2 == 0)
			{
				z += gridHeight * 0.5f;
			}
		}

		return new Vector3(x, 0, z);
	}

	public Vector2 GetGridPos(Vector3 coord)
	{
		Vector3 initPos = CalcInitPos();
		Vector2 gridPos = new Vector2();

		if (gridType == GridType.Cube)
		{
			gridPos.x = Mathf.RoundToInt((coord.x - initPos.x) / gridWidth);
			gridPos.y = Mathf.RoundToInt((initPos.z - coord.z) / (gridHeight));
		}
		else if (gridType == GridType.Hex)
		{
			gridPos.x = Mathf.RoundToInt((coord.x - initPos.x) / (gridWidth * 3 / 4));
			gridPos.y = Mathf.RoundToInt((initPos.z - coord.z) / (gridHeight));
			if(gridPos.x % 2 == 0)
			{
				gridPos.y = Mathf.RoundToInt((initPos.z - coord.z + (gridHeight * 0.5f)) / (gridHeight));
			}
		}

		return gridPos;
	}

	void CreateGrid()
	{
        // Calculating number of rows and columns
		Vector2 gridSize = CalcGridSize();

        // Creating empty GameObject to hold Grid gameobjects
		gridContainer = new GameObject("HexGrid");

		for (float y = 0; y < gridSize.y; ++y)
		{
			for (float x = 0; x < gridSize.x; ++x)
			{
				// Setting position according to grid pos
				Vector2 gridPos = new Vector2(x, y);

				var worldPos = CalcWorldCoord(gridPos);

				if (!useTerrain && IsTileHanging(worldPos))
					continue;

				//// Finding Corners
				//if (FindCornersV2(worldPos))
				//	continue;

				// Instantiating new grid object
				GameObject hex = Instantiate(gridPrefab);
				
				hex.transform.position = worldPos;
				
				hex.transform.localScale = scaleFactor;

				// Raising grid height to match terrain height
				var rayStartPos = hex.transform.position + Vector3.up * rayHeight;
				RaycastHit hit;
				int layer = LayerMask.GetMask("Terrain");

				if (Physics.Raycast(rayStartPos, Vector3.down, out hit, rayHeight, layer))
				{
					rayStartPos.y = hit.point.y;
					hex.transform.position = rayStartPos;
				}

				hex.transform.tag = "ExtraGrid";
                var cellInfo = hex.GetComponent<CellInfo>();
				
				cellInfo.gridPos = gridPos;
				//if (blue != c)
				//	cellInfo.original = blue;
				//if (red != c)
				//	cellInfo.red = red;
				//if (yellow != c)
				//	cellInfo.yellow = yellow;
				//if (green != c)
				//	cellInfo.green = green;
								
                // Setting layer to grid
                hex.gameObject.layer = (int)Layer_Index.Grid;
				
				hex.transform.parent = gridContainer.transform;

				Tile tile = new Tile(gridPos, cellInfo);

				cellInfo.tile = tile;

				map.Add(gridPos, cellInfo);

                SetCellNormal(cellInfo);
			}
		}
	}

	void DestroyExtraGrids()
	{
		colliderObjects = GameObject.FindGameObjectsWithTag("MapBounds");
		if (colliderObjects.Length > 0)
		{
			foreach (var o in colliderObjects)
			{
				var col = o.GetComponent<BoxCollider>();
				// getting all objects in colliders
				foreach (var grid in Physics.OverlapBox(col.bounds.center, Vector3.Scale(col.size, col.transform.localScale * 0.5f), col.transform.rotation))
				{
					//Remove DestroyMe tag
					if (grid.tag == "ExtraGrid")
						grid.tag = "Grid";
				}
				Destroy(o);
			}

			var grids = gridContainer.transform.GetComponentsInChildren<Transform>();

			//Find all grids with destroyme to destroy
			foreach (var grid in grids)
			{
				if (grid.tag == "ExtraGrid")
				{
					map.Remove(grid.GetComponent<CellInfo>().gridPos);
					Destroy(grid.gameObject);
				}
			}
		}
	}

	void FindObstacles()
	{
		colliderObjects = GameObject.FindGameObjectsWithTag("Obstacle");

		foreach (var o in colliderObjects)
		{
			var col = o.GetComponent<BoxCollider>();
			if (col)
			{
				foreach (var grid in Physics.OverlapBox(col.bounds.center, Vector3.Scale(col.size, col.transform.localScale * 0.5f), col.transform.rotation))
				{
					if (grid.tag == "Grid" || grid.tag == "ExtraGrid")
					{
						CellInfo tile = null;
						if (map.TryGetValue(grid.transform.GetComponent<CellInfo>().gridPos, out tile))
						{
							tile.tile.Unobstructed = false;
							obstacles.Add(tile);
							tile.obstacle = o.GetComponent<ObstacleInfo>();
						}
					}
				}
			}

			var capsuleCol = o.GetComponent<CapsuleCollider>();
            if (capsuleCol)
            {
                foreach (var grid in Physics.OverlapCapsule(capsuleCol.bounds.center + (Vector3.up * capsuleCol.height * 0.5f), capsuleCol.bounds.center - (Vector3.up * capsuleCol.height * 0.5f), capsuleCol.radius))
                {
                   if (grid.tag == "Grid" || grid.tag == "ExtraGrid")
                    {
                        CellInfo tile = null;
                        if (map.TryGetValue(grid.transform.GetComponent<CellInfo>().gridPos, out tile))
                        {
                            tile.tile.Unobstructed = false;
                            obstacles.Add(tile);
                            tile.obstacle = o.GetComponent<ObstacleInfo>();
                        }

                    }
                }
            }
		}
	}

	public CellInfo GetGridAtMouse()
	{
		CellInfo info = null;

		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit hit;

		var layer = LayerMask.GetMask("Grid");

		if (Physics.Raycast(ray, out hit, 10000, layer))
			info = hit.transform.GetComponent<CellInfo>();

		return info;
	}

	public Unit GetUnitAtMouse()
	{
		Unit unit = null;

		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit hit;

		var layer = LayerMask.GetMask("Friendly", "Hostile");

		if (Physics.Raycast(ray, out hit, 10000, layer))
			unit = hit.transform.GetComponent<Unit>();

		return unit;
	}

	public void Range(CellInfo gridInfo, int range)
	{
		if(this.traversable != null)
		{
			foreach(var cell in this.traversable)
			{
				cell.SetDefault();
			}
		}

		this.traversable = new HashSet<CellInfo>();

		if (gridInfo)
		{
			this.traversable.Add(gridInfo);

			var fringe = new HashSet<CellInfo>();

			foreach (var n in gridInfo.tile.Neighbours)
			{
				if (traversable.Add(n.info))
					fringe.Add(n.info);
			}

			while (range > 1)
			{
				var next = new HashSet<CellInfo>();

				foreach (var grid in fringe)
				{
					foreach (var n in grid.tile.Neighbours)
					{
						if (traversable.Add(n.info))
							next.Add(n.info);
					}
				}
				fringe = next;
				--range;
			}

			foreach (var tile in this.traversable)
			{
				if (tile.tile.Unobstructed)
					tile.SetGreen();
			}
		}
	}

	/// <summary>
	/// Outputs list of tiles within provided range of origin tile
	/// </summary>
	/// <param name="origin"></param>
	/// <param name="range"></param>
	/// <param name="list"></param>
	public static void Range(CellInfo origin, int range, out HashSet<CellInfo> list)
	{
		list = new HashSet<CellInfo>();

		if (origin)
		{
			list.Add(origin);

			var fringe = new HashSet<CellInfo>();

			foreach (var n in origin.tile.AllNeighbours)
			{
				if (list.Add(n.info))
					fringe.Add(n.info);
			}

			while (range > 1)
			{
				var next = new HashSet<CellInfo>();

				foreach (var grid in fringe)
				{
					foreach (var n in grid.tile.AllNeighbours)
					{
						if (list.Add(n.info))
							next.Add(n.info);
					}
				}
				fringe = next;
				--range;
			}

			foreach (var tile in list)
			{
				tile.SetGreen();
			}
		}
	}

	public void RangeWithCost(CellInfo gridInfo, int range)
	{
		if (traversable != null)
		{
			foreach (var cell in traversable)
			{
				cell.SetDefault();
			}
		}

		traversable = new HashSet<CellInfo>();
		Dictionary<CellInfo, float> costMap = new Dictionary<CellInfo, float>();
		

		if (gridInfo)
		{
			traversable.Add(gridInfo);
			costMap.Add(gridInfo, 0);

			var fringe = new HashSet<CellInfo>();

			foreach (var n in gridInfo.tile.Neighbours)
			{
                // cost of traveling to tile
                var cost = n.cost;

                // Checks if currentTile has a higher exit cost than target tile
                // Adds exit cost if higher
                if (gridInfo.tile.exitCost > n.exitCost)
                    cost += (gridInfo.tile.exitCost - n.exitCost);

                if (cost <= range && traversable.Add(n.info))
				{
					costMap.Add(n.info, cost);
					fringe.Add(n.info);
				}
			}

			int iter = range;

			while (iter > 0)
			{
				var next = new HashSet<CellInfo>();

				foreach (var grid in fringe)
				{
                    // current cost of tile
					var costFromOrigin = costMap[grid];
					foreach (var n in grid.tile.Neighbours)
					{
						var cost = costFromOrigin + n.cost;

                        if (grid.tile.exitCost > n.exitCost) 
                        {
                            cost += (grid.tile.exitCost - n.exitCost);
                        }

						if (cost <= range && traversable.Add(n.info))
						{
							costMap.Add(n.info, (float)cost);
							next.Add(n.info);
						}
						else if (costMap.ContainsKey(n.info) && cost < costMap[n.info])
							costMap[n.info] = cost;
					}
				}
				fringe = next;
				--iter;
			}

			foreach (var tile in traversable)
			{
                if (tile.tile.Unobstructed)
                    tile.SetGreen();

			}
		}
	}

	/// <summary>
	/// Outputs hashset of units within range of origin tile exclusive of starting tile
	/// mask used to filter and select specific UnitTypes
	/// </summary>
	/// <param name="origin"></param>
	/// <param name="range"></param>
	/// <param name="units"></param>
	/// <param name="mask"></param>
	public static void GetUnitsInRange(CellInfo origin, int range, out HashSet<Unit> units, UnitType mask = 0)
	{
		HashSet<CellInfo> list = new HashSet<CellInfo>();
		units = new HashSet<Unit>();

		if (origin)
		{
			list.Add(origin);

			var fringe = new HashSet<CellInfo>();

			foreach (var n in origin.tile.AllNeighbours)
			{
				if (list.Add(n.info))
				{
					fringe.Add(n.info);
					if(n.info.unit)
					{
						if (n.info.unit.isDead)
							continue;

						if (mask == 0)
							units.Add(n.info.unit);
						else if ((n.info.unit.unitType & mask) != 0)
							units.Add(n.info.unit);
					}
				}
			}

			while (range > 1)
			{
				var next = new HashSet<CellInfo>();

				foreach (var grid in fringe)
				{
					foreach (var n in grid.tile.AllNeighbours)
					{
						if (list.Add(n.info))
						{
							next.Add(n.info);
							if (n.info.unit)
							{
								if (n.info.unit.isDead)
									continue;

								if (mask == 0)
									units.Add(n.info.unit);
								else if ((n.info.unit.unitType & mask) != 0)
									units.Add(n.info.unit);
							}
						}
					}
				}
				fringe = next;
				--range;
			}
		}
	}

	public static void GetAttackRangeAndUnits(CellInfo origin, int range, out HashSet<Unit> units, out HashSet<CellInfo> cells, UnitType mask = 0)
	{
		cells = new HashSet<CellInfo>();
		units = new HashSet<Unit>();

		if (origin)
		{
			cells.Add(origin);

			if (origin.unit && (origin.unit.unitType & mask) != 0)
				units.Add(origin.unit);

			if (range == 0)
				return;

			var fringe = new HashSet<CellInfo>();

			foreach (var n in origin.tile.ActualAllNeighbours)
			{
				if (cells.Add(n.info))
				{
					fringe.Add(n.info);
					if (n.info.unit)
					{
						if (n.info.unit.isDead)
							continue;

						if (mask == 0)
							units.Add(n.info.unit);
						else if ((n.info.unit.unitType & mask) != 0)
							units.Add(n.info.unit);
					}
				}
			}

			while (range > 1)
			{
				var next = new HashSet<CellInfo>();

				foreach (var grid in fringe)
				{
					foreach (var n in grid.tile.ActualAllNeighbours)
					{
						if (cells.Add(n.info))
						{
							next.Add(n.info);
							if (n.info.unit)
							{
								if (n.info.unit.isDead)
									continue;

								if (mask == 0)
									units.Add(n.info.unit);
								else if ((n.info.unit.unitType & mask) != 0)
									units.Add(n.info.unit);
							}
						}
					}
				}
				fringe = next;
				--range;
			}
		}
	}

    /// <summary>
    /// Returns list of all cells at range units away from origin
    /// Returns cells between minRange and range if one is passed in
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public static HashSet<CellInfo> RingRangeALL(CellInfo origin, int range, int minRange = -1)
	{
		if (origin)
		{
			HashSet<CellInfo> list = new HashSet<CellInfo>();

			// Adding origin
			list.Add(origin);
                        
            // Return origin cell if range 0
            if (range == 0)
                return list;

            var fringe = new HashSet<CellInfo>();

            HashSet<CellInfo> ring = new HashSet<CellInfo>();

            if (minRange == 0)
                ring.Add(origin);

            foreach (var n in origin.tile.ActualAllNeighbours)
			{
                if (list.Add(n.info))
                {
                    fringe.Add(n.info);
                    if (minRange == 1 || minRange == 0) // minRange 0 so adding all cells to list
                        ring.Add(n.info);
                }

			}

            if(minRange == 0 && range == 1)
            {
                return ring;
            }

			if (range == 1)
				return fringe;

			int counter = 1;

			while (counter <= range)
			{
				var next = new HashSet<CellInfo>();

				foreach (var grid in fringe)
				{
					foreach (var n in grid.tile.ActualAllNeighbours)
					{
						if (list.Add(n.info))
						{
							next.Add(n.info);
							if (minRange != -1 && counter >= minRange)
								ring.Add(n.info);
							else if (counter + 1 == range)
								ring.Add(n.info);
						}
					}
				}
				fringe = next;

                if (++counter == range)
                    return ring;
            }
		}
		return null;
	}

    /// <summary>
	/// Returns list of unobsctruted cells at range units away from origin
    /// Returns cells between minRange and range if one is passed in
    /// Does not include origin
	/// </summary>
	/// <param name="origin"></param>
	/// <param name="range"></param>
	/// <returns></returns>
	public static HashSet<CellInfo> RingRangeUnobstructed(CellInfo origin, int range, int minRange = -1)
    {
        if (origin && instance)
        {
            HashSet<CellInfo> list = new HashSet<CellInfo>();

            // Adding origin
            list.Add(origin);

            // Return origin cell if range 0
            if (range == 0)
                return list;

            var fringe = new HashSet<CellInfo>();

            HashSet<CellInfo> ring = new HashSet<CellInfo>();

            foreach (var n in origin.tile.AllNeighbours)
            {
                if (n.Unobstructed)
                {
                    if (list.Add(n.info))
                    {
                        fringe.Add(n.info);
                        if (minRange == 0) // minRange 0 so adding all cells to list
                            ring.Add(n.info);
                    }
                }
            }

            if (range == 1)
                return fringe;

            int counter = 1;

            while (counter <= range)
            {
                var next = new HashSet<CellInfo>();

                foreach (var grid in fringe)
                {
                    foreach (var n in grid.tile.AllNeighbours)
                    {
                        if (n.Unobstructed)
                        {
                            if (list.Add(n.info))
                            {
                                next.Add(n.info);
                                if (minRange != -1 && counter >= minRange)
                                    ring.Add(n.info);
                                else if (counter + 1 == range)
                                    ring.Add(n.info);
                            }
                        }
                    }
                }
                fringe = next;

                if (++counter == range)
                    return ring;
            }
        }
        return null;
    }

    /// <summary>
    /// Generates a pathable region
    /// Units must be on one of the returned tiles to be able to path to anylocation in returned list
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="range"></param>
    /// <param name="minRange"></param>
    /// <returns></returns>
    public static HashSet<CellInfo> GetPathableRegion(CellInfo origin)
    {
        if (origin && instance)
        {
            HashSet<CellInfo> list = new HashSet<CellInfo>();

            // Adding origin
            list.Add(origin);

            var fringe = new HashSet<CellInfo>();

            foreach (var n in origin.tile.NeighboursForAI)
            {
                if (list.Add(n.info))
                {
                    fringe.Add(n.info);
                }    
            }

            while (fringe.Count > 0)
            {
                var next = new HashSet<CellInfo>();

                foreach (var grid in fringe)
                {
                    foreach (var n in grid.tile.NeighboursForAI)
                    {
                        if (list.Add(n.info))
                        {
                            next.Add(n.info);
                        }                        
                    }
                }
                fringe = next;
            }

            return list;
        }

        return null;
    }

    /// <summary>
    /// Returns closest empty tile to worldPos passed in
    /// </summary>
    /// <param name="worldPos"></param>
    /// <returns></returns>
    public static CellInfo GetClosestEmpty(Vector3 worldPos)
    {
        if (!instance)
            return null;

        var origin = instance.GetGridFromWorldPos(worldPos);

        if (origin)
        {
            if (origin.isEmpty && origin.tile.Unobstructed)
                return origin;

            HashSet<CellInfo> list = new HashSet<CellInfo>();

            // Adding origin
            list.Add(origin);


            var fringe = new HashSet<CellInfo>();

            //HashSet<CellInfo> ring = new HashSet<CellInfo>();

            foreach (var n in origin.tile.AllNeighbours)
            {
                if (list.Add(n.info))
                {
                    if (n.info.isEmpty && n.Unobstructed)
                        return n.info;

                    fringe.Add(n.info);
                }

            }

            int counter = 1;

            while (counter <= 20)
            {
                var next = new HashSet<CellInfo>();

                foreach (var grid in fringe)
                {
                    foreach (var n in grid.tile.AllNeighbours)
                    {
                        if (list.Add(n.info))
                        {
                            if (n.info.isEmpty && n.Unobstructed)
                                return n.info;

                            next.Add(n.info);
                        }
                    }
                }
                fringe = next;
            }
        }
        return null;
    }

    public static CellInfo GetClosestTile(Vector3 worldPos)
    {
        if (!instance)
            return null;

        var origin = instance.GetGridFromWorldPos(worldPos);

        if (origin)
        {
            if (origin.isEmpty && origin.tile.Unobstructed)
                return origin;

            HashSet<CellInfo> list = new HashSet<CellInfo>();

            // Adding origin
            list.Add(origin);


            var fringe = new HashSet<CellInfo>();

            //HashSet<CellInfo> ring = new HashSet<CellInfo>();

            foreach (var n in origin.tile.AllNeighbours)
            {
                if (list.Add(n.info))
                {
                    if (n.info.isEmpty && n.Unobstructed)
                        return n.info;

                    fringe.Add(n.info);
                }

            }

            int counter = 1;

            while (counter <= 20)
            {
                var next = new HashSet<CellInfo>();

                foreach (var grid in fringe)
                {
                    foreach (var n in grid.tile.AllNeighbours)
                    {
                        if (list.Add(n.info))
                        {
                            if (n.info.isEmpty && n.Unobstructed)
                                return n.info;

                            next.Add(n.info);
                        }
                    }
                }
                fringe = next;
            }
        }
        return null;
    }

    // May be obsolete
    public CellInfo GetGridFromWorldPos(Vector3 pos)
	{
		CellInfo grid;
		if (map.TryGetValue( GetGridPos(pos), out grid))
		{
			return grid;
		}

		return null;
	}

	public void ClearSelections()
	{
		if (traversable != null)
		{
			foreach (var cell in traversable)
			{
				cell.SetDefault();
			}
		}
		if(obstacles != null)
			foreach (var cell in obstacles)
			{
				cell.SetDefault();
			}
	}

	public void HighlightObstacles()
	{
		if (obstacles != null)
			foreach (var cell in obstacles)
			{
				cell.SetRed();
			}
	}

    /// <summary>
    /// Returns whether any part of the tile is hanging off the end of the map
    /// </summary>
    /// <param name="worldPos"></param>
    /// <returns></returns>
	bool IsTileHanging(Vector3 worldPos)
	{
		worldPos.y += 50;

		for (int i = 0; i < 2; ++i)
		{

			var z = worldPos + new Vector3(gridHalfHeight * -i, 0, 0);
			var x = worldPos + new Vector3(0, 0, gridHalfWidth * -i);

			RaycastHit hit;
			var layer = LayerMask.GetMask("Build");
			if (Physics.Raycast(z, Vector3.down, out hit, 100, layer))
			{
				return false;
			}

			if (Physics.Raycast(x, Vector3.down, out hit, 100, layer))
			{
				return false;
			}
		}
		return true;
	}

	public static float Distance(Vector2 a, Vector2 b)
	{
		return CubeDistance(CoordToCube(a), CoordToCube(b));
	}

	public static float CubeDistance(Vector3 a, Vector3 b)
	{
		return Mathf.Max(Mathf.Abs(a.x - b.x), Mathf.Abs(a.y - b.y), Mathf.Abs(a.z - b.z));
	}

	public static Vector2 CubeToCoord(Vector3 cube )
	{
		float col = cube.x;
		float row = cube.z + (cube.x - ((int)cube.x & 1)) / 2;
		return new Vector2(col, row);
	}

	public static Vector3 CoordToCube(Vector2 hex)
	{
		float x = hex.x;
		float z = hex.y - (hex.x - ((int)hex.x & 1)) / 2;

		float y = -x - z;

		return new Vector3(x, y, z);
	}

    /// <summary>
    /// Returns a hashset of overlaping cells from the two lists passed in
    /// </summary>
    /// <param name="list1"></param>
    /// <param name="list2"></param>
    /// <returns></returns>
	public static HashSet<CellInfo> Intersecting(HashSet<CellInfo> list1, HashSet<CellInfo> list2)
	{
		HashSet<CellInfo> intersects = new HashSet<CellInfo>();
		foreach(var tile in list2)
		{
			if(list1.Contains(tile))
			{
				intersects.Add(tile);
			}
		}

		return intersects;
	}

    /// <summary>
    /// Resets exit cost of all tiles in altertedTiles list and then clears it for reuse
    /// </summary>
    public void ResetAlteredTiles()
    {
        foreach (var tile in alteredTiles)
        {
            tile.tile.exitCost = 0;
        }

        alteredTiles.Clear();
    }

    /// <summary>
    /// Finds the average normal of four points below cell
    /// Sets cell's up vector to the average normal
    /// </summary>
    /// <param name="cell"></param>
    public void SetCellNormal(CellInfo cell)
    {
        LayerMask mask = LayerMask.GetMask("Terrain");

        var raystart = cell.transform.position + Vector3.up * 4;

        Vector3 normal = Vector3.up;

        RaycastHit hit;
        List<Vector3> corners = new List<Vector3>();

        for (int i = 0; i < 2; ++i)
        {
            var x = raystart + new Vector3(gridHalfWidth * -i, 0, 0);
            var z = raystart + new Vector3(0, 0, gridHalfHeight * -i);            

            if (Physics.Raycast(x, Vector3.down, out hit, 5, mask))
            {
                corners.Add(hit.point);
            }

            if (Physics.Raycast(z, Vector3.down, out hit, 5, mask))
            {
                corners.Add(hit.point);
            }
        }

        if (corners.Count == 4)// && connectingPoints.Count == 4)
        {
            normal = Vector3.zero;

            normal += Vector3.Cross(corners[1] - corners[3], corners[0] - corners[2]);

            normal.Normalize();
        }
        else if(corners.Count == 3)
        {
            normal = Vector3.zero;

            normal += Vector3.Cross(corners[1] - corners[0], corners[2] - corners[0]);

            normal.Normalize();
        }
        else if(corners.Count == 2)
        {
            normal = Vector3.zero;

            normal += Vector3.Cross(corners[0], corners[1]);

            normal.Normalize();
        }


        cell.transform.up = normal;
    }
}
