﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Fungus;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public List<Unit> units = new List<Unit>();

    public Dictionary<Unit, int> deadList = new Dictionary<Unit, int>();

    public int day = 0;

    public float resources;

    public float playerEssence = 0;

    private GameObject missionSelect;

    private GameObject combat;

    private GameObject mainMenu;

    public GameObject combatCamera;

    public Unit selectedUnit;
    
    public string gameVersion="1.00b";

    List<Unit> inGameUnits = new List<Unit>();

    public List<Unit> missionUnits = new List<Unit>();

    public List<BuildingSlot> buildings = new List<BuildingSlot>();

    public bool hasDoneMission = false;

    private bool playedOpeningScene = false;

    public List<string> completedMissions=new List<string>();
    private void Awake()
    {
        missionSelect = transform.Find("MissionSelect").gameObject;
        combat = transform.Find("Combat").gameObject;
        combatCamera = combat.transform.Find("RTSCamera").gameObject;
        mainMenu = transform.Find("MainMenu").gameObject;
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;

            if (units.Count > 0)
            {
                List<Unit> newUnits = new List<Unit>();

                foreach (var unit in units)
                {
                    var copy = Instantiate(unit);
                    copy.name = unit.name;
                    copy.transform.parent = gameObject.transform;
                    copy.gameObject.SetActive(false);

                    // creating copy of skill tree
                    copy.CopySkillTree();

                    newUnits.Add(copy);
                }

                units = newUnits;
            }
        }
        else if (instance != this)
        {
            instance.combatCamera.transform.position = combatCamera.transform.position;
            instance.combatCamera.transform.rotation = combatCamera.transform.rotation;
            Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }



    void OnEnable()
    {
        SceneManager.sceneLoaded += Initialize;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= Initialize;
    }
    //This will be called before the Start, but after Awake functions
    private void Initialize(Scene scene ,LoadSceneMode mode)
    {
        missionSelect.SetActive(true);
        combat.SetActive(true);
        BroadcastMessage("SetInstance", SendMessageOptions.DontRequireReceiver);
        missionSelect.SetActive(false);
        combat.SetActive(false);
        if (scene.name == "MissionSelect")
        {
            missionSelect.SetActive(true);
            combat.SetActive(false);
            mainMenu.SetActive(false);
            if (!playedOpeningScene)
            {
                FindObjectOfType<Flowchart>().ExecuteBlock("New Game Dialogue");
                playedOpeningScene = true;
            }
        }
        else if (scene.name == "MainMenu")
        {
            combat.SetActive(false);
            missionSelect.SetActive(false);
            mainMenu.SetActive(true);
        }
        else
        {
            hasDoneMission = true;
            combat.SetActive(true);
            missionSelect.SetActive(false);
            mainMenu.SetActive(false);
            SpawnMissionUnits();
            if(UnitManager.instance)
            {
                UnitManager.instance.DisableInputLock();
            }

        }
        if (units.Count == 0)
        {
            for (int i = 0; i < 5; i++)
            {
                units.Add(StaticValues.instance.UnitPrefabs[i].GetComponent<Unit>());
            }
        }
    }

    /// <summary>
    /// Used to initialise new units for the first time
    /// </summary>
    /// <param name="unit"></param>
    public void InitialiseUnit(Unit unit)
    {
        if(unit.skillTree)
        {
            // Making copy of skill tree scriptable object and assigning to unit
            var tree = Instantiate(unit.skillTree);
            tree.name = unit.skillTree.name;
            unit.skillTree = tree;
        }

        // Adds unit to list if not in list already
        if(!units.Contains(unit))
        {
            units.Add(unit);
        }
    }

    /// <summary>
    /// Loops through list of player unit and levels unit up if possible
    /// Units leveled up return true in loop
    /// </summary>
    public void LevelUnits()
    {
        if (selectedUnit.level.currentExp>StaticValues.instance.expTable[selectedUnit.level.unitLevel])
        {
            selectedUnit.LevelUp();
            MissionSelectUnitDetails.instance.FillUI();
        }
    }

    public void AddEssence(float value, Vector3 pos)
    {
        playerEssence += value;
        PoolMananger.instance.GetPooledText("+" + Mathf.Floor(value), Color.blue, pos,null, true);
    }

    /// <summary>
    /// Spawns units selected in mission select
    /// Removes pre existing units in scene
    /// Requires at least one spawn location in scene
    /// </summary>
    public void SpawnMissionUnits()
    {
        GameObject[] spawnLoc = GameObject.FindGameObjectsWithTag("Spawn Location");
        if(spawnLoc[0])
        {
            // Check if loading in from missionSelect
            if(missionUnits.Count > 0)
            {
                // remove in future
                List<Unit> unitList = new List<Unit>();

                // Find pre-existing units
                foreach (var unit in GameObject.FindObjectsOfType<Unit>())
                {
                    if (unit.unitType == UnitType.Player)
                        unitList.Add(unit);
                }

                // Destroy exiting units
                for (int i = unitList.Count - 1; i >= 0; i--)
                {
                    Destroy(unitList[i].gameObject);
                    Destroy(unitList[i]);
                }
            }

            int x = 0;

            inGameUnits.Clear();

            // Instantiate in selected units from mission select
            foreach (var unit in missionUnits)
            {
                var newUnit = Instantiate(unit.gameObject);

                newUnit.transform.position = spawnLoc[x].transform.position;

                newUnit.name = unit.name;

                newUnit.SetActive(true);

                inGameUnits.Add(newUnit.GetComponent<Unit>());

                // Rotates through all spawnlocations in level
                if (++x >= spawnLoc.Length)
                {
                    x = 0;
                }
            }

            // Reinitialize all lists of units
            if (TurnManager.instance)
                TurnManager.instance.ReInitializeLists();
        }
        else
        {
            Debug.LogError(SceneManager.GetActiveScene().name + " is missing spawn location object");
        }
    }

    public void NextDay()
    {
        day++;
        hasDoneMission = false;
    }

    public void TimeTravel()
    {
        var amount = Mathf.FloorToInt(playerEssence / 1000f);
        day -= amount;
    }

    public void DeadUnit(int saveID)
    {
        Unit unit = null;
        for (int i = units.Count - 1; i >= 0; i--)
        {
            if (units[i].unitSaveID == saveID)
            {
                unit = units[i];
                units.RemoveAt(i);
                break;
            }
        }

        if (unit)
        {
            deadList.Add(unit, day);
        }
        else
            Debug.LogError(name + " can't kill target: no matching id found");
    }
}
