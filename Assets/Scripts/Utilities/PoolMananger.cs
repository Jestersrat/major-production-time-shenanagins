﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoolMananger : MonoBehaviour
{
	public static PoolMananger instance;

    public int startingPool = 20;

    [Header("Particle System Pool")]

	public Dictionary<string, List<ParticleSystem>> PSLookUp = new Dictionary<string, List<ParticleSystem>>();

	Transform particleHolder;

    [Header("Scrolling Text Pool")]
    public GameObject scrollingTextPrefab;

    public Dictionary<GameObject, Text> pooledTexts;

    public GameObject worldCanvas;

    void Awake()
    {
        if (instance)
        {
            Debug.Log("Why are there multiple PoolManagers in this scene?");
        }

        instance = this;
    }


    // Use this for initialization
    void Start ()
	{        
		particleHolder = new GameObject("Particle Container").transform;

        foreach (var canvas in GameManager.instance.GetComponentsInChildren<Canvas>(true))
        {
            if (canvas.name == "WorldCanvas")
                worldCanvas = canvas.gameObject;
        }

        if (!scrollingTextPrefab)
            scrollingTextPrefab = Resources.Load("Prefabs/ScrollingText") as GameObject;

        //var ability = Resources.Load("Prefabs/test") as GameObject;

        //var ability2 = Resources.Load("AbilityButtonPrefab");

        pooledTexts = new Dictionary<GameObject, Text>();

        for (int i = 0; i < startingPool; i++)
        {
            var obj = Instantiate(scrollingTextPrefab);
            pooledTexts.Add(obj, obj.GetComponent<Text>());
            obj.transform.SetParent(worldCanvas.transform);
            obj.SetActive(false);
        }		
	}
    //void SetInstance()
    //{
    //    if (instance == null)
    //        instance = this;
    //}
    // Update is called once per frame
    void Update ()
	{
		
	}

	public void AddPSList(ParticleSystem p)
	{		
		if(!PSLookUp.ContainsKey(p.name))
		{
			var psList = new List<ParticleSystem>();
			PSLookUp.Add(p.name, psList);

			for (int i = 0; i < startingPool; ++i)
			{
				var newInstance = Instantiate(p);

				psList.Add(newInstance);

				newInstance.transform.parent = particleHolder;

				newInstance.gameObject.SetActive(false);
			}
		}
	}

	/// <summary>
	/// Adds extra ps to particlepool
	/// </summary>
	/// <param name="list"></param>
	public ParticleSystem NeedExtra(List<ParticleSystem> list)
	{
		var newPS = Instantiate(list[0]);
		newPS.gameObject.SetActive(false);
		list.Add(newPS);

		if (list.Count > 20)
			Debug.LogError(list[0].name + " particle list is greater than 20");

		newPS.transform.parent = particleHolder;

		return newPS;
	}

	IEnumerator WaitForParticle(ParticleSystem ps)
	{
		if (!ps.isPlaying)
			ps.Play();

        float playTime = 0;
        while (ps.IsAlive())
        {
#if UNITY_EDITOR
            Debug.Log(ps.name + " " + playTime);
#endif
            playTime += Time.unscaledDeltaTime;
            if (playTime > 5)
            {
                ps.gameObject.SetActive(false);
                break;
            }
            yield return null;
        }



        ps.gameObject.SetActive(false);
	}



	/// <summary>
	/// Returns matching inactive particle system from pool for use
    /// Creates a particle pool for particle if one doesn't exist
	/// </summary>
	/// <param name="source"></param>
	/// <returns></returns>
	public ParticleSystem PSGet(ParticleSystem source)
	{
		List<ParticleSystem> list;
        if (PSLookUp.TryGetValue(source.name, out list))
        {
            foreach (var p in list)
            {
                // returns particlesystem if not active
                if (p && !p.gameObject.activeSelf)
                {
                    p.transform.parent = particleHolder;
                    return p;
                }
            }

            // Generates new ps and adds to list
            return NeedExtra(list);
        }
        else
        {
            //throw new System.Exception("Missing " + source.name + " from ParticleManager");
            AddPSList(source);
            if (PSLookUp.TryGetValue(source.name, out list))
            {
                foreach (var p in list)
                {
                    // returns particlesystem if not active
                    if (!p.gameObject.activeSelf)
                        return p;
                }

                // Generates new ps and adds to list
                return NeedExtra(list);
            }
        }

        return null;
	}

    /// <summary>
    /// Activates particle and removes on death or when lifetime hits 10
    /// </summary>
    /// <param name="p"></param>
	public void ActivatePS(ParticleSystem p)
	{
		p.gameObject.SetActive(true);
		StartCoroutine(WaitForParticle(p));
	}

    /// <summary>
    /// This function moves a scrolling Text effect of target Color and message to a position, and sets it as active.
    /// It returns the text for future modifications if needed.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="col"></param>
    /// <param name="pos"></param>
    /// <returns></returns>
    public GameObject GetPooledText(string message, Color col, Vector3 pos,Unit unit=null,  bool sway=false)
    {
        float x = 3;
        if (unit)
        {
            x = unit.headPos + 2;
        }
        else
        {
            if (sway)
            {
                x = 2;
            }
        }

        foreach(var pooled in pooledTexts)
        {
            if (pooled.Key.gameObject)
            {
                if (!pooled.Key.activeInHierarchy)
                {
                    pooled.Value.color = col;
                    pooled.Value.text = message;
                    pooled.Key.transform.position = new Vector3(pos.x, pos.y, pos.z) + Vector3.up * x;
                    pooled.Key.GetComponent<ScrollingText>().sway = sway;
                    pooled.Key.SetActive(true);
                    return pooled.Key;
                }
            }
        }

        if (scrollingTextPrefab && worldCanvas)
        {
            //Creates scrolling text if no idle ones avaliable.
            GameObject obj = Instantiate(scrollingTextPrefab, worldCanvas.transform);
            var text = obj.GetComponent<Text>();
            text.color = col;
            text.text = message;
            obj.transform.position = new Vector3(pos.x, pos.y, pos.z) + Vector3.up * x;
            obj.transform.SetParent(worldCanvas.transform);
            obj.GetComponent<ScrollingText>().sway = sway;
            obj.SetActive(true);

            pooledTexts.Add(obj, text);

            return obj;
        }

        return null;
    }
}
