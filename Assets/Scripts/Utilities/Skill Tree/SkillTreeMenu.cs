﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTreeMenu : MonoBehaviour
{
    public static SkillTreeMenu instance;

    public Unit unit;

    public List<List<Button>> buttonsList = new List<List<Button>>();

    public GameObject[] groups;

    public Text skillPoints;
    public Text unitClass;
	// Use this for initialization
	void Start ()
    {
        instance = this;

        if (groups.Length <= 0)
            throw new System.Exception(this.name + " group is missing horiztonal layout groups. Create and drag groups into group section");

        foreach(var g in groups)
        {
            List<Button> buttons = new List<Button>();
            foreach(var button in g.gameObject.GetComponentsInChildren<Button>())
            {
                buttons.Add(button);
                button.gameObject.SetActive(false);
            }

            buttonsList.Add(buttons);
            g.SetActive(true);
        }

        //gameObject.SetActive(false);
	}
	
    void Update()
    {
        if (unit != null)
        {
            skillPoints.text = unit.level.skillPoints.ToString();
        }
        if (Input.GetButton("Fire2"))
        {
            gameObject.SetActive(false);
        }
    }

    public void DisplayTree()//Unit unit = null)
    {
        Reset();

		//if (unit)
		//    this.unit = unit;

		//if(!this.unit)
		//{
		//    this.unit = GameManager.instance.selectedUnit;
		//}

		unit = GameManager.instance.selectedUnit;

        if (!unit.skillTree)
        {
            Debug.LogError(unit.name + " missing skill tree");
            return;
        }

        var tree = unit.skillTree;

        skillPoints.text = unit.level.skillPoints.ToString();
        unitClass.text = unit.unitClass;
        for (int i = 0; i < tree.sets.Count; i++)
        {
            var level = tree.sets[i].requiredLevel;
            while (level > buttonsList.Count - 1)
                AddExtraGroup();

            bool assigned = false;

            foreach (var button in buttonsList[level])
            {
                if (!button.gameObject.activeInHierarchy)
                {
                    var script = button.GetComponent<SkillTreeButton>();
                    var set = tree.sets[i];
                    script.SetUp(unit, set, button);

                    assigned = true;

                    if (level > unit.level.unitLevel || tree.setsLearnt.Contains(tree.sets[i]) || tree.setsBlocked.Contains(tree.sets[i]))
                        button.interactable = false;

                    break;
                }
            }

            if (assigned)
                continue;

            var b = AddExtraButton(buttonsList[level]);
            b.GetComponent<SkillTreeButton>().SetUp(unit, tree.sets[i], b);

            if (level > unit.level.unitLevel || tree.setsLearnt.Contains(tree.sets[i]) || tree.setsBlocked.Contains(tree.sets[i]))
                b.interactable = false;
        }

    }

    public void Reset()
    {
        foreach(var list in buttonsList)
        {
            foreach(var button in list)
            {
                button.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Adds extra button to provided list
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    Button AddExtraButton(List<Button> list)
    {
        var button = Instantiate(list[0]);

        list.Add(button);

        button.transform.SetParent(list[0].transform.parent);

        button.transform.localScale = list[0].transform.localScale;

        button.gameObject.SetActive(false);

        return button;
    }

    /// <summary>
    /// Creates an extra skill level group thingy
    /// </summary>
    void AddExtraGroup()
    {
        List<Button> buttons = new List<Button>();
        var group = Instantiate(groups[0]);

        foreach (var button in group.GetComponentsInChildren<Button>(true))
        {
            buttons.Add(button);
            button.gameObject.SetActive(false);
        }

        group.transform.SetParent(groups[0].transform.parent);

        group.transform.localScale = groups[0].transform.localScale;

        group.SetActive(true);

        buttonsList.Add(buttons);
    }
}
