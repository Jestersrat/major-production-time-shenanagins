﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "S Skill Set", menuName = "Skill Tree/Skill Set", order = 1)]
public class SkillSet : ScriptableObject
{
    public string setName;
    public List<AbilityBase> abilities = new List<AbilityBase>();
    public int requiredLevel;
    public int cost = 1;
    public bool hasStatsMod = false;
    public Stats statsMod = new Stats(0);
    public List<SkillSet> blocks = new List<SkillSet>();
    public string classChange = "";
    public Sprite setImage;
    [TextArea(3,10)]
    public string setDescription;
    public int extraExitCost = 0;
    public List<AbilityBase> replacedAbilities = new List<AbilityBase>();
}
