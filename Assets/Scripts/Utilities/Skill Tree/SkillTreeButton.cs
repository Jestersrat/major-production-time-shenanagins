﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTreeButton : MonoBehaviour
{
    Unit unit = null;

    public SkillSet skillSet = null;

    Button button = null;

    /// <summary>
    /// Sets up skillTreebutton and activates game object
    /// Sets up listeners to button
    /// Need to check for whether skill has been added before and is obtainable yourself
    /// 
    /// </summary>
    /// <param name="unit"></param>
    /// <param name="skillSet"></param>
    /// <param name="button"></param>
    /// <returns></returns>
    public Button SetUp(Unit unit, SkillSet skillSet, Button button)
    {
        this.unit = unit;

        this.skillSet = skillSet;

        this.button = button;

        if (skillSet.setImage != null)
        {
            button.image.sprite = skillSet.setImage;
        }
        else if(skillSet.abilities.Count > 0)
        {
            button.image.sprite = skillSet.abilities[0].icon;
        }

        GetComponent<TooltipBasic>().tooltipInfo ="<b>"+skillSet.setName+"</b>\n" + skillSet.setDescription;

        button.onClick.RemoveAllListeners();

        button.interactable = true;

        button.onClick.AddListener(AddAbility);

        gameObject.SetActive(true);

        return button;
    }

    void AddAbility()
    {
        if(unit && skillSet)
        {
            if(unit.level.skillPoints >= skillSet.cost)
            {
                button.interactable = false;

                unit.skillTree.ApplySet(unit, skillSet);
            }
        }
    }
}
