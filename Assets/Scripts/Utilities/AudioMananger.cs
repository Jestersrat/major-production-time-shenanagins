﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMananger : MonoBehaviour
{
    public static AudioMananger instance;

    public List<AudioSource> audioPool = new List<AudioSource>();

    GameObject audioSource;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

	// Use this for initialization
	void Start ()
    {
        var mixer = Resources.Load("MasterAudio") as AudioMixer;

        audioSource = new GameObject();

        var s = audioSource.AddComponent<AudioSource>();

        s.outputAudioMixerGroup = mixer.FindMatchingGroups("Effects")[0];

        audioPool.Add(s);

        audioSource.name = "Audio Source";

        audioSource.transform.SetParent(transform);

        while (audioPool.Count < 10)
        {
            AddExtraSource();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    AudioSource AddExtraSource()
    {
        var a = Instantiate(audioSource);
        //a.enabled = false;
        var source = a.GetComponent<AudioSource>();
        audioPool.Add(source);
        a.transform.SetParent(transform);
        if (audioPool.Count > 20)
            Debug.Log("Audio Manager's Pool is at " + audioPool.Count);

        return source;
    }

    /// <summary>
    /// Returns an inactive AudioSource
    /// Creates a new one and returns if none available
    /// </summary>
    /// <returns></returns>
    AudioSource GetFreeSource()
    {
        foreach(var s in audioPool)
        {
            if(!s.isPlaying)
            {
                return s;
            }
        }

        return AddExtraSource();
    }

    /// <summary>
    /// Plays clip at location with a free audio clip
    /// Pass in true to have audio source be parented to passed in transform
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="location"></param>
    /// <param name="parent"></param>
    public void PlayClip(AudioClip clip, Transform location, bool parent = false)
    {
        var s = GetFreeSource();

        s.transform.position = location.position;

        if (parent)
            s.transform.SetParent(location);
        else
            s.transform.SetParent(transform);

        s.clip = clip;
        s.pitch = Random.Range(0.75f, 1.25f);
        s.Play();
    }
}
