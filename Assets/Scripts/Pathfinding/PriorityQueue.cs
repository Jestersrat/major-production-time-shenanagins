﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class PriorityQueue<PRIORITY, Node>
{
	private SortedDictionary<PRIORITY, Queue<Node>> list = new SortedDictionary<PRIORITY, Queue<Node>>();

	public void Enqueue(PRIORITY priority, Node value)
	{
		Queue<Node> queue;

		if(!list.TryGetValue(priority, out queue))
		{
			queue = new Queue<Node>();
			list.Add(priority, queue);
		}

		queue.Enqueue(value);
	}

	public Node Dequeue()
	{
		var pair = list.First();
		var value = pair.Value.Dequeue();
		if (pair.Value.Count == 0)
			list.Remove(pair.Key);

		return value;
	}

	public bool IsEmpty
	{
		get { return !list.Any(); }
	}
}
