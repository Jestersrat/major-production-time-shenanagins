﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface IHasNeighbours<N>
{
    IEnumerable<N> UnobstructedNeighbours { get; }

    IEnumerable<N> Neighbours { get; }
	IEnumerable<N> NeighboursForAI { get; }
}

public class Path<Node> : IEnumerable<Node>
{
	public Node LastStep { get; private set; }

	public Path<Node> PreviousSteps { get; private set; }

	public double TotalCost { get; private set; }

	private Path(Node lastStep, Path<Node> previousSteps, double totalCost)
	{
		LastStep = lastStep;
		PreviousSteps = previousSteps;
		TotalCost = totalCost;
	}

	public Path(Node start): this(start, null, 0) { }

	public Path<Node> AddStep(Node step, double stepCost)
	{
		return new Path<Node>(step, this, TotalCost + stepCost);
	}
	
	public IEnumerator<Node> GetEnumerator()
	{
		for (Path<Node> p = this; p != null; p = p.PreviousSteps)
			yield return p.LastStep;
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	public static Path<TNode> FindPath<TNode>(
		TNode start,
		TNode destination,
		Func<TNode, TNode, double> distance,
		Func<TNode, TNode, double> estimate)
		where TNode : IHasNeighbours<TNode>
	{
		var closed = new HashSet<TNode>();
		var queue = new PriorityQueue<double, Path<TNode>>();
		queue.Enqueue(0, new Path<TNode>(start));

		while(!queue.IsEmpty)
		{
			var path = queue.Dequeue();

			if (closed.Contains(path.LastStep))
				continue;

			if (path.LastStep.Equals(destination))
				return path;

			closed.Add(path.LastStep);

			foreach(TNode n in path.LastStep.Neighbours)
			{
				double d = distance(path.LastStep, n);
				var newPath = path.AddStep(n, d);
				queue.Enqueue(newPath.TotalCost + estimate(n, destination), newPath);
			}
		}
		return null;
	}

	//public static Path<TNode> FindPathForAI<TNode>
	//	(
	//	TNode start,
	//	TNode destination,
	//	Func<TNode, TNode, double> distance,
	//	Func<TNode, TNode, double> estimate
	//	)
	//	where TNode : IHasNeighbours<TNode>
	//{
	//	var closed = new HashSet<TNode>();
	//	var queue = new PriorityQueue<double, Path<TNode>>();
	//	queue.Enqueue(0, new Path<TNode>(start));

	//	while (!queue.IsEmpty)
	//	{
	//		var path = queue.Dequeue();

	//		if (closed.Contains(path.LastStep))
	//			continue;

	//		if (path.LastStep.Equals(destination))
	//			return path.PreviousSteps;

	//		closed.Add(path.LastStep);

	//		foreach (TNode n in path.LastStep.NeighboursForAI)
	//		{				
	//			double d = distance(path.LastStep, n);
	//			var newPath = path.AddStep(n, d);
	//			queue.Enqueue(newPath.TotalCost + estimate(n, destination), newPath);
	//		}
	//	}
	//	return null;
	//}



	/// <summary>
	/// Uses Tile.NeighboursForAI interface which requires dest boolean in Tile to be set true
	/// Setting of boolean handled in method
	/// Returns a path with a suitable end position for an AI unit
	/// </summary>
	/// <param name="start"></param>
	/// <param name="destination"></param>
	/// <param name="distance"></param>
	/// <param name="estimate"></param>
	/// <param name="unit"></param>
	/// <returns></returns>
	public static Path<Tile> FindPathForAI
		(
		Tile start,
		Tile destination,
		Func<Tile, Tile, double> distance,
		Func<Tile, Tile, double> estimate,
		Unit unit
		)
	{
		var closed = new HashSet<Tile>();
		var queue = new PriorityQueue<double, Path<Tile>>();
		queue.Enqueue(0, new Path<Tile>(start));

		destination.DestTile = true;

		while (!queue.IsEmpty)
		{
			var path = queue.Dequeue();

			if (closed.Contains(path.LastStep))
				continue;

			// Prevents units from stacking up when running out of movement range before reaching target
			if (path.TotalCost == unit.MoveRange && path.TotalCost > 0 && !path.LastStep.Empty)
			{
				path = queue.Dequeue();
				if (!path.LastStep.Empty)
					path = queue.Dequeue();
				continue;
			}

			if (path.LastStep.Equals(destination))
			{
				// Rejects paths where tile before target is not empty
				// This is because ai units will always stop tile before last tile of path
				if(!path.LastStep.Empty)
				{
					path = queue.Dequeue();
					continue;
				}

				destination.DestTile = false;
				return path.PreviousSteps;
			}

			closed.Add(path.LastStep);
			
			foreach (Tile n in path.LastStep.NeighboursForAI)
			{
				double d = distance(path.LastStep, n);
				var newPath = path.AddStep(n, d);
				queue.Enqueue(newPath.TotalCost + estimate(n, destination), newPath);
				
			}
		}

		destination.DestTile = false;
		return null;
	}

	/// <summary>
	/// Ignores any units in the way and paths to destination
	/// </summary>
	/// <typeparam name="TNode"></typeparam>
	/// <param name="start"></param>
	/// <param name="destination"></param>
	/// <param name="distance"></param>
	/// <param name="estimate"></param>
	/// <returns></returns>
    public static Path<TNode> FindBrutePath<TNode>(
    TNode start,
    TNode destination,
    Func<TNode, TNode, double> distance,
    Func<TNode, TNode, double> estimate)
    where TNode : IHasNeighbours<TNode>
    {
        var closed = new HashSet<TNode>();
        var queue = new PriorityQueue<double, Path<TNode>>();
        queue.Enqueue(0, new Path<TNode>(start));

        while (!queue.IsEmpty)
        {
            var path = queue.Dequeue();

            if (closed.Contains(path.LastStep))
                continue;

            if (path.LastStep.Equals(destination))
                return path;

            closed.Add(path.LastStep);

            foreach (TNode n in path.LastStep.UnobstructedNeighbours)
            {
                double d = distance(path.LastStep, n);
                var newPath = path.AddStep(n, d);
                queue.Enqueue(newPath.TotalCost + estimate(n, destination), newPath);
            }
        }
        return null;
    }
}
