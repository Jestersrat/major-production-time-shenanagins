﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitLevel 
{
    [Header("Level Up")]
    public float currentExp = 0;

    public float essenceValue = 0;

    public float expValue = 0;

    public int unitLevel = 0;

    public int skillPoints = 0;
}
