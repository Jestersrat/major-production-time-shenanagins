﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnSet
{
    public Unit unit;
    public int level;
    public int spawnCost = 1;
}

[System.Serializable]
public enum SpawnOrder
{
    Random,
    Sequence
}

public class UnitSpawner : MonoBehaviour
{     
    [Header("Spawn Rate settings")]
    [Tooltip("Number of sets of units that come out of spawner before being disabled. 0 To disable")]
    public int spawnLimit = 0;

    public int spawnCounter = 0;

    [Tooltip("Units spawn until budget is spent each time spawner is triggered")]
    public int spawnBudget = 0;

    [Tooltip("Number of turns before more units spawn. 0 to Disable")]
    public int repeat = 0;

    int repeatCounter = 0;

    [Tooltip("Number of units to spawn every repeat interval")]
    public int repeatBudget = 0;

    [Header("Activation Requirements")]
    [Tooltip("Will only being spawning once triggered")]
    public bool triggerOnly = false;

    [Tooltip("Number of turns before spawner becomes active")]
    public int spawnDelay = 0;
    public int delayCounter = 0;

    [Header("Unit Details")]
    [SerializeField]
    [Tooltip("Spawns units randomly or in order they appear in list")]
    public SpawnOrder spawnOrder;

    int sequenceIndex;
    
    public UnitType unitTeam = UnitType.Hostile;

    public List<int> objectiveID = new List<int>();

    public List<SpawnSet> SpawnList = new List<SpawnSet>();

    SortedDictionary<float, CellInfo> validSpawnLocations = new SortedDictionary<float, CellInfo>();

    bool activated;    
        
	// Use this for initialization
	void Start ()
    {
        var cell = GridManager.instance.GetGridFromWorldPos(transform.position);
        transform.position = cell.transform.position;

        var spawnLocations = GridManager.RingRangeUnobstructed(cell, 3, 0);

        sequenceIndex = 0;

        activated = false;

        float dif = 0.0001f;

        foreach (var loc in spawnLocations)
        {
            validSpawnLocations.Add(Vector3.Distance(loc.transform.position, transform.position) + dif, loc);
            dif += dif;             
        }

        // Add listener to turns to increment counters
        if (!triggerOnly || repeat > 0)
            TurnManager.instance.OnStartTurn.AddListener(OnTurnChange);
    }
	

	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTurnChange(UnitType e)
    {
        if (e == UnitType.Player)
        {
            if (!activated && !triggerOnly)
            {
                if (++delayCounter >= spawnDelay)
                {
                    Activate();
                }
            }
            else if (activated && repeat > 0 && ++repeatCounter >= repeat)
            {
                SpawnSet(repeatBudget);
                repeatCounter = 0;
            }
        }
    }    

    public bool Activate()
    {
        if(!activated)
        {
            SpawnSet(spawnBudget);
            activated = true;
            return activated;
        }

        return false;
    }

    /// <summary>
    /// Spawns units until budget is spent
    /// </summary>
    /// <param name="budget"></param>
    void SpawnSet(int budget)
    {
        var tempBudget = budget;

        int loopBreaker = 0;

        while (tempBudget > 0)
        {
            if (loopBreaker++ > spawnBudget)
                return;

            if (spawnOrder == SpawnOrder.Random)
            {
                int selection = Random.Range(0, SpawnList.Count);

                if (SpawnList[selection].unit)
                {
                    if (SpawnList[selection].spawnCost <= tempBudget)
                    {
                        tempBudget -= SpawnList[selection].spawnCost;
                        // spawns unit and levels them up to target level
                        var unitSpawned = SpawnUnit(SpawnList[selection].unit); //.ForceLevelUp(SpawnList[sequenceIndex].level);
                        unitSpawned.level.unitLevel = SpawnList[selection].level;
                    }
                }
                else
                {
                    // Removes invalid / empty spawnset
                    SpawnList.RemoveAt(selection);
                }
            }
            else // Spawn in sequence
            {
                if (SpawnList[sequenceIndex].unit)
                {
                    tempBudget -= SpawnList[sequenceIndex].spawnCost;
                    // spawns unit and levels them up to target level
                    var unitSpawned = SpawnUnit(SpawnList[sequenceIndex].unit); //.ForceLevelUp(SpawnList[sequenceIndex].level);
                    unitSpawned.level.unitLevel = SpawnList[sequenceIndex].level;
                }
                else
                {
                    // Removes invalid / empty spawnset
                    SpawnList.RemoveAt(sequenceIndex);
                    // Continue to skip increase of sequence
                    continue;
                }

                sequenceIndex++;
                if (sequenceIndex >= SpawnList.Count)
                    sequenceIndex = 0;
            }


        }

        if (spawnLimit > 0)
        {
            if (++spawnCounter >= spawnLimit)
                gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Instantiates copy of unit passed into function and returns reference
    /// Team of unit is set to that of the one stored in spawner
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public Unit SpawnUnit(Unit unit)
    {
        var u = Instantiate(unit);
        u.name = unit.name;
        u.unitType = unitTeam;

        TurnManager.instance.RegisterUnit(u);

        foreach (var tile in validSpawnLocations)
        {
            if(tile.Value.isEmpty)
            {
                u.gameObject.transform.position = tile.Value.transform.position;
                u.SetCurrentTile(tile.Value);
                break;
            }
        }

        u.gameObject.SetActive(true);

        //if (u.unitType != UnitType.Player)
        //{
        //    var agent = u as Agent;
        //    agent.InitialiseAgent();
        //}

        // assigning IDs tied to spawner
        foreach(var ID in objectiveID)
        {
            u.objectiveID.Add(ID);
        }

        u.ResetStatMods();
        u.ResetActionPoints();
        u.ResetUnit();


        return u;
    }
}
