﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats
{
	[System.Serializable]
	public struct MinMax
	{
		public float min;
		public float max;

		public MinMax(float min, float max)
		{
			this.min = min;
			this.max = max;
		}
	}
	public Stats()
	{
        HP = 100f;
        maxHP = 100f;
        attackRange = 1f;
        damageRange = new MinMax(10f, 20f);
        accuracy = 60f;
        dodge = 0;
        critChance = 10;
        critMod = 2;
        defence = 0;
        actionPoints = 9;
        moveSpeed = 5;
        visionRange = 10;
        //currentExp = 0;
        //expValue = 10;
        //essenceValue = 10;
        //unitLevel = 0;
        //skillPoints = 0;
    }

    public Stats(bool lvlUp)
    {
        HP = 100f;
        maxHP = 100f;
        attackRange = 1f;
        damageRange = new MinMax(10f, 20f);
        accuracy = 60f;
        dodge = 0;
        critChance = 10;
        critMod = 2;
        defence = 0;
        actionPoints = 9;
        moveSpeed = 5;
        visionRange = 10;
        //currentExp = 0;
        //expValue = 10;
        //essenceValue = 10;
        //unitLevel = 0;
        //skillPoints = 0;
    }

    public Stats(float value)
    {
        HP = value;
        maxHP = value;
        attackRange = value;
        damageRange = new MinMax(value, value);
        accuracy = value;
        dodge = value;
        critChance = value;
        critMod = value;
        defence = value;
        actionPoints = (int)value;
        moveSpeed = (int)value;
        visionRange = (int)value;
        //currentExp = value;
        //expValue = value;
        //essenceValue = value;
        //unitLevel = (int)value;
        //skillPoints = (int)value;
    }

    public void Zero()
    {
        HP = 0;
        maxHP = 0;
        attackRange = 0;
        damageRange = new MinMax(0, 0);
        accuracy = 0;
        dodge = 0;
        critChance = 0;
        critMod = 0;
        defence = 0;
        actionPoints = 0;
        moveSpeed = 0;
        visionRange = 0;
        //currentExp = 0;
        //expValue = 0;
        //essenceValue = 0;
        //unitLevel = 0;
        //skillPoints = 0;
    }

    public void SetAll(float value)
    {
        HP = value;
        maxHP = value;
        attackRange = value;
        damageRange = new MinMax(value, value);
        accuracy = value;
        dodge = value;
        critChance = value;
        critMod = value;
        defence = value;
        actionPoints = (int)value;
        moveSpeed = (int)value;
        visionRange = (int)value;
        //currentExp = value;
        //expValue = value;
        //essenceValue = value;
        //unitLevel = (int)value;
        //skillPoints = (int)value;
    }

    public static Stats operator + (Stats left, Stats right)
    {
        Stats newStats = new Stats();
        newStats.Zero();

        newStats.HP = left.HP + right.HP;
        newStats.maxHP = left.maxHP + right.maxHP;
        newStats.attackRange = left.attackRange + right.attackRange;

        newStats.damageRange.min = left.damageRange.min + right.damageRange.min;
        newStats.damageRange.max = left.damageRange.max + right.damageRange.max;

        newStats.accuracy = left.accuracy + right.accuracy;
        newStats.dodge = left.dodge + right.dodge;
        newStats.critChance = left.critChance + right.critChance;
        newStats.critMod = left.critMod + right.critMod;
        newStats.defence = left.defence + right.defence;
        newStats.actionPoints = left.actionPoints + right.actionPoints;
        newStats.moveSpeed = left.moveSpeed + right.moveSpeed;
        newStats.visionRange = left.visionRange + right.visionRange;
        //newStats.currentExp = left.currentExp + right.currentExp;
        //newStats.expValue = left.expValue + right.expValue;
        //newStats.essenceValue = left.essenceValue + right.essenceValue;
        //newStats.unitLevel = left.unitLevel + right.unitLevel;
        //newStats.skillPoints = left.skillPoints + right.skillPoints;

        return newStats;
    }

    public static Stats operator -(Stats left, Stats right)
    {
        Stats newStats = new Stats();
        newStats.Zero();

        newStats.HP = left.HP - right.HP;
        newStats.maxHP = left.maxHP - right.maxHP;
        newStats.attackRange = left.attackRange - right.attackRange;

        newStats.damageRange.min = left.damageRange.min - right.damageRange.min;
        newStats.damageRange.max = left.damageRange.max - right.damageRange.max;

        newStats.accuracy = left.accuracy - right.accuracy;
        newStats.dodge = left.dodge - right.dodge;
        newStats.critChance = left.critChance - right.critChance;
        newStats.critMod = left.critMod - right.critMod;
        newStats.defence = left.defence - right.defence;
        newStats.actionPoints = left.actionPoints - right.actionPoints;
        newStats.moveSpeed = left.moveSpeed - right.moveSpeed;
        newStats.visionRange = left.visionRange - right.visionRange;
        //newStats.currentExp = left.currentExp - right.currentExp;
        //newStats.expValue = left.expValue - right.expValue;
        //newStats.essenceValue = left.essenceValue - right.essenceValue;
        //newStats.unitLevel = left.unitLevel - right.unitLevel;
        //newStats.skillPoints = left.skillPoints - right.skillPoints;

        return newStats;
    }

    public static Stats operator *(Stats left, Stats right)
    {
        Stats newStats = new Stats();
        newStats.Zero();

        newStats.HP = left.HP * right.HP;
        newStats.maxHP = left.maxHP * right.maxHP;
        newStats.attackRange = left.attackRange * right.attackRange;

        newStats.damageRange.min = left.damageRange.min * right.damageRange.min;
        newStats.damageRange.max = left.damageRange.max * right.damageRange.max;

        newStats.accuracy = left.accuracy * right.accuracy;
        newStats.dodge = left.dodge * right.dodge;
        newStats.critChance = left.critChance * right.critChance;
        newStats.critMod = left.critMod * right.critMod;
        newStats.defence = left.defence * right.defence;
        newStats.actionPoints = left.actionPoints * right.actionPoints;
        newStats.moveSpeed = left.moveSpeed * right.moveSpeed;
        newStats.visionRange = left.visionRange * right.visionRange;
        //newStats.currentExp = left.currentExp * right.currentExp;
        //newStats.expValue = left.expValue * right.expValue;
        //newStats.essenceValue = left.essenceValue * right.essenceValue;
        //newStats.unitLevel = left.unitLevel * right.unitLevel;
        //newStats.skillPoints = left.skillPoints * right.skillPoints;

        return newStats;
    }

    [Header("Combat Stats")]
	public float HP = 100f;
	public float maxHP = 100f;

	public float attackRange = 1f;
	[SerializeField]
	public MinMax damageRange;

    /// <summary>
    /// Return a value between min and max damageRange
    /// </summary>
    public float Damage { get { return Random.Range(damageRange.min, damageRange.max); } }

    /// <summary>
    /// Returns Hitchance based on accuracy and other unit's dodge chance
    /// Returns value between 10 and 100
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public float HitChance(Unit other, float accuracyMod = 0)
	{
		return Mathf.Clamp(accuracy + accuracyMod - other.stats.dodge, 10f, 100f);
	}

    /// <summary>
    /// Hitchance based on accuracy and dodge chance
    /// Returns value between 10 and 100
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public float HitChance(float dodge, float accuracyMod = 0)
	{
		return Mathf.Clamp(accuracy + accuracyMod - dodge, 10f, 100f);
	}
    
	public float accuracy = 50f;
	public float dodge = 0;

    public float critChance = 10;
    public float critMod = 2;

	public float defence = 0;

    public float Defence(bool unclamped = false)
    {
        return Mathf.Clamp(defence / 100f, 0, .9f);
    }
	
	[Header("Utility Stats")]
	public int actionPoints = 9;

	public int moveSpeed = 5;
	
	public int visionRange = 10;

 //   [Header("Level Up")]
 //   public float currentExp = 0;

 //   public float essenceValue = 0;

 //   public float expValue = 0;

 //   public int unitLevel = 0;

 //   public int skillPoints = 0;
}
