﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
/// <summary>
/// Takes sends 1 arge UnitType
/// </summary>
public class TurnEvent : UnityEvent<UnitType> { }

public class TurnManager : MonoBehaviour
{
    public static TurnManager instance;

    public List<Unit> playerUnits = new List<Unit>();

    public List<Agent> enemyUnits = new List<Agent>();

    public List<Unit> allUnits = new List<Unit>();

    public List<Unit> allyUnits = new List<Unit>();

    public UnitType[] teams;

    public int currentIndex = 0;

    public int turnNumber = 1;

    bool startInvoked = false;

    public UnitType currentTeam
    {
        get
        {
            return teams[currentIndex];
        }
    }

    [SerializeField]
    public TurnEvent OnStartTurn;

    [SerializeField]
    public TurnEvent OnEndTurn;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void OnEnable()
    {
        if (allUnits.Count == 0)
        {
            foreach (var unit in FindObjectsOfType<Unit>())
            {
                allUnits.Add(unit);

                if (unit.unitType == UnitType.Player)
                    playerUnits.Add(unit);

                if (unit.unitType == UnitType.Hostile)
                    enemyUnits.Add((Agent)unit);

                if (unit.unitType == UnitType.Ally)
                    allyUnits.Add(unit);
            }
        }

        currentIndex = 0;

        StartCoroutine(StartTurnInvoker());
    }

    void OnDisable()
    {
        if (allUnits.Count > 0)
        {
            allUnits.Clear();
            enemyUnits.Clear();
            allyUnits.Clear();
            playerUnits.Clear();
        }

        startInvoked = false;
    }

    void Start()
    {
        if (allUnits.Count == 0)
        {
            foreach (var unit in FindObjectsOfType<Unit>())
            {
                allUnits.Add(unit);

                if (unit.unitType == UnitType.Player)
                    playerUnits.Add(unit);

                if (unit.unitType == UnitType.Hostile)
                    enemyUnits.Add((Agent)unit);

                if (unit.unitType == UnitType.Ally)
                    allyUnits.Add(unit);
            }
        }
        foreach (Unit playerunit in playerUnits)
        {
            Unit target = null;
            for (int i = 0; i < GameManager.instance.units.Count; i++)
            {
                if (GameManager.instance.units[i].unitSaveID == playerunit.unitSaveID)
                {
                    target = GameManager.instance.units[i];
                    break;
                }
            }
            if (target != null && !GameManager.instance.missionUnits.Contains(target))
            {
                GameManager.instance.missionUnits.Add(playerunit);
            }
        }
        StartCoroutine(StartTurnInvoker());
    }

    IEnumerator StartTurnInvoker()
    {
        // waits until all enemy units have set their tiles
        yield return new WaitWhile(() => enemyUnits.Count <= 0);
        yield return new WaitUntil(() => enemyUnits[enemyUnits.Count - 1].CurrentTile);
        if (!startInvoked)
        {
            startInvoked = true;
            OnStartTurn.Invoke(teams[currentIndex]);
        }
    }

    void SetInstance()
    {
        if (instance == null)
            instance = this;
    }
    /// <summary>
    /// Broadcasts endturn and start turn 
    /// Ends turn for current team and broadcasts endturn event
    /// Also clears all grids
    /// </summary>
	public void EndTurn()
    {
        OnEndTurn.Invoke(teams[currentIndex]);

        startInvoked = false;

        currentIndex++;
        if (currentIndex >= teams.Length)
            currentIndex = 0;

        if (currentTeam == UnitType.Player)
        {
            turnNumber++;
            MessageLog.instance.AddEvent("<b>Turn" + turnNumber + "</b>");
        }

        GridManager.instance.ResetAlteredTiles();

        OnStartTurn.Invoke(teams[currentIndex]);

        GridManager.instance.ClearSelections();
    }

    public void ReInitializeLists()
    {
        allUnits.Clear();
        enemyUnits.Clear();
        playerUnits.Clear();

        foreach (var unit in FindObjectsOfType<Unit>())
        {
            allUnits.Add(unit);

            if (unit.unitType == UnitType.Player)
                playerUnits.Add(unit);

            if (unit.unitType == UnitType.Hostile)
                enemyUnits.Add((Agent)unit);
        }

        PlayerUIFiller.instance.ReInitialize();
    }

    public void RegisterUnit(Unit unit)
    {
        allUnits.Add(unit);

        if (unit.unitType == UnitType.Hostile)
        {
            enemyUnits.Add(unit as Agent);
            UAIManager.instance.evaluationOrder.AddLast(unit as Agent);
        }
        else if (unit.unitType == UnitType.Player)
        {
            playerUnits.Add(unit);

            bool skip = false;

            foreach (var u in GameManager.instance.units)
            {
                if (u.unitSaveID == unit.unitSaveID)
                    skip = true;
            }

            if (!skip)
            {
                GameManager.instance.units.Add(unit);
                GameManager.instance.missionUnits.Add(unit);
            }
        }

        //foreach (Unit playerunit in playerUnits)
        //{
        //    Unit target = null;
        //    for (int i = 0; i < GameManager.instance.units.Count; i++)
        //    {
        //        if (GameManager.instance.units[i].unitSaveID == playerunit.unitSaveID)
        //        {
        //            target = GameManager.instance.units[i];
        //            break;
        //        }
        //    }
        //    if (target != null && !GameManager.instance.missionUnits.Contains(target))
        //    {
        //        GameManager.instance.units.Add(target);
        //        GameManager.instance.missionUnits.Add(target);
        //    }
        //}
    }

}
