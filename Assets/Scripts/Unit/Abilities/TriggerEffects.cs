﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum TriggerTarget
{
    User,
    Target
}

[CreateAssetMenu(fileName = "ExtraTrigger", menuName = "Status Effects/Status Triggers/BaseTrigger", order = 1)]
public class TriggerEffects : ScriptableObject
{
    [SerializeField]
    [Tooltip("Does trigger effect user or target")]
    public TriggerTarget triggerTarget;

    public Stats stats = new Stats(0);

    //public float damage = 0;

    public virtual void Trigger(Unit user, Unit target)
    {
        if(triggerTarget == TriggerTarget.User)
        {
            if (user)
                user.stats += stats;
        }
        else
        {
            target.stats += stats;
        }
    }
}
