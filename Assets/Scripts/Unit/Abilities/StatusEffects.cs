﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StatusEffect", menuName = "StatusEffect", order = 1)]
public class StatusEffects : ScriptableObject
{
    [SerializeField]
    [Tooltip("Determines whether status effect's stat changes are additive or multiplicative")]
    public AbilityBase.DamageMod effectMod;

    [SerializeField]
    [Tooltip("stat changes to apply. Positive values to increase stats and negative values to decrease")]
    public Stats stats = new Stats(0);

    public float duration = 1;

    [SerializeField]
    public AbilityBase.DamageMod damageMod;

    public float damage = 0;

    public TriggerEffects[] onApplyEffects;
    public TriggerEffects[] onDeathEffects;
    public TriggerEffects[] onRemoveEffects;

    public Unit user = null;

    public Unit target = null;

    [SerializeField]
    public AbilityBase ability;

    [Header("Visual Effects")]
    public Sprite icon;
    public ParticleSystem particle;
    public Vector3 particlePosition = Vector3.up;
    public string scrollingText;

    [TextArea]
    public string description;

    ParticleSystem ps;

    public void ApplyStatsEffects()
    {
        if(effectMod == AbilityBase.DamageMod.Modifier)
        {
            target.multStatsMod += stats;
        }
        else
        {
            target.staticStatsMod += stats;
        }

        foreach(var effect in onApplyEffects)
        {
            effect.Trigger(user, target);
        }

        target.UpdateStats();

        if (particle)
            PlayParticle();
    }

    /// <summary>
    /// Removes stat changes of this status effect from target
    /// Gets called before status effect is removed
    /// </summary>
    /// <param name="target"></param>
    public void RemoveStatsEffect()
    {
        if (effectMod == AbilityBase.DamageMod.Modifier)
        {
            target.multStatsMod -= stats;
        }
        else
        {
            target.staticStatsMod -= stats;
        }

        foreach (var effect in onRemoveEffects)
        {
            effect.Trigger(user, target);
        }

        target.UpdateStats();

        if (ps)
        {
            ps.Stop();
            ps = null;
        }
    }

    /// <summary>
    /// Counts down duration and applies any damaging effects
    /// </summary>
    /// <param name="target"></param>
    public void Tick()
    {
        duration--;

        if(damage != 0)
        {
            if (damageMod == AbilityBase.DamageMod.Modifier)
            {
                var dam = damage * user.stats.Damage;
                if (dam > 0)
                {
                    user.damageDealt += dam;
                    target.damageTaken += dam;
                }
                target.ApplyDamage(dam, user);

                if (MessageLog.instance && target.beenRevealed)
                    MessageLog.instance.AddEvent(user.name + "' s "+ name +  " ticks on " + target.name + " for<color=red>"+Mathf.RoundToInt(dam) +" </color>");

                if (PoolMananger.instance && target.CurrentTile.fog.revealed)
                {
                    if (dam > 0)
                    {
                        PoolMananger.instance.GetPooledText(scrollingText +  Mathf.RoundToInt(dam).ToString(), Color.red, target.transform.position,target);

                    }
                    else if (dam < 0)
                    {
                        PoolMananger.instance.GetPooledText(scrollingText +  "+"+ Mathf.RoundToInt(Mathf.Abs(damage)), Color.green, target.transform.position, target);
                    }
                }
            }
            else
            {
                target.ApplyDamage(damage, user);

                if (MessageLog.instance && target.beenRevealed)
                    MessageLog.instance.AddEvent(user.name + "' s " + name + " ticks on " + target.name + " for " + damage);

                if (damage > 0 && target.CurrentTile.fog.revealed)
                {
                    PoolMananger.instance.GetPooledText(scrollingText +  damage.ToString(), Color.red, target.transform.position , target);

                }
                else if (damage < 0 && target.CurrentTile.fog.revealed)
                {
                    PoolMananger.instance.GetPooledText(scrollingText + "+" + Mathf.Abs(damage), Color.green, target.transform.position, target);
                }
            }
        }
        else if (scrollingText != "")
        {
            PoolMananger.instance.GetPooledText(scrollingText , Color.yellow, target.transform.position , target);
        }
        if (duration <= 0)
        {
            RemoveStatsEffect();
        }
    }

    public void Initialise(Unit user, Unit target, AbilityBase ability, bool addToList = true)
    {
        this.user = user;
        this.ability = ability;
        this.target = target;

        ApplyStatsEffects();

        if (addToList)
            target.statuses.Add(this);
    }

    void PlayParticle()
    {
        ps = PoolMananger.instance.PSGet(particle);
        ps.transform.position = target.transform.position+particlePosition;

        particle = ps;
        ps.gameObject.transform.SetParent(target.transform);
        ps.gameObject.SetActive(true);
        ps.Play();
    }
}
