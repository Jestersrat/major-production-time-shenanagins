﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	// May implement a projectile pool in future if neccessary
	//public static List<Projectile> pool = null;

	public AbilityBase ability;

	public float speed;

	public Vector3 dir;

	public Unit user;

	public CellInfo targetTile;

	public bool hit = false;

	// Used to stop extra chain lightnings to spawn
	public bool trigger = false;

    float fallBackTimer;

	public static Transform projectileContainer;

	// Use this for initialization
	void Start ()
	{
		if(projectileContainer == null)
		{
			projectileContainer = new GameObject("Projectile Container").transform;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
        var moveDelta = Mathf.Clamp((speed + StaticValues.instance.unitMoveSpeed * 0.01f) * Time.deltaTime, 0, Vector3.Distance(targetTile.transform.position, transform.position));
        
        transform.position += dir * moveDelta;

        transform.forward = dir;



        fallBackTimer += Time.deltaTime;

        if ((transform.position - targetTile.transform.position).magnitude < 1f || fallBackTimer > 5)
        {
            if (!trigger)
            {
                ability.ProjectileHit(user, targetTile);
            }

            hit = true;

            // Play any animation if needed
            Destroy(gameObject);
        }
    }
	
	//void OnTriggerEnter(Collider other)
	//{
 //       var unit = other.GetComponent<Unit>();
 //       if (unit != user)
 //       {
 //           ability.ProjectileHit(user, other.gameObject);

 //           // Play any animation if needed

 //           Destroy(gameObject);
 //       }
	//}

	public void Shoot(Unit user, AbilityBase ability, CellInfo targetLocation, Unit startingLocation = null)
	{
		this.ability = ability;

        Vector3 offSet = new Vector3(0, 0.5f);

		if(startingLocation != null)
		{
			dir = (targetLocation.transform.position + offSet - startingLocation.transform.position).normalized;
			transform.position = startingLocation.transform.position;
		}
		else
		{
			dir = (targetLocation.transform.position + offSet - user.transform.position).normalized;
			transform.position = user.transform.position;
		}
		
		speed = ability.projectileSpeed;
		
		this.user = user;

		hit = false;

		targetTile = targetLocation;

        fallBackTimer = 0;

		trigger = false;
	}
}
