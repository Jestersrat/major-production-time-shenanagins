﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AbilityIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public static List<AbilityIcon> iconPool = null;

	public Button button;

	public AbilityBase ability;

    public Text coolDownText;

    public Text chargesText;

    public Text hotkey;

    public GameObject cdBackground;
    private Image cdBackgroundFill;

	TooltipBasic toolTip;

    public Color selectedColor;

    KeyCode key = KeyCode.None;

    RectTransform rectTrans;


    Vector2 baseSize;

    List<RectTransform> childRects = new List<RectTransform>();

	// Use this for initialization
	void Start ()
	{
		if(iconPool == null)
		{
			iconPool = new List<AbilityIcon>();

			int count = 0;
			foreach (var icon in transform.parent.GetComponentsInChildren<AbilityIcon>())
			{
				iconPool.Add(icon);
				++count;
			}


		}
        cdBackgroundFill = cdBackground.transform.Find("CFill").GetComponent<Image>();
		toolTip = GetComponent<TooltipBasic>();
        
		button = GetComponent<Button>();

		button.onClick.AddListener(OnClick);

        rectTrans = GetComponent<RectTransform>();
        baseSize = rectTrans.sizeDelta;

        foreach(var rect in  GetComponentsInChildren<RectTransform>())
        {
            if(rect != rectTrans)
            {
                childRects.Add(rect);
            }
        }

        gameObject.SetActive(false);
	}
	
    void Update()
    {
        if(Input.GetKeyDown(key))
        {
            OnClick();
        }

        //if(selected)
        //{
        //    //transform.localScale = selectedScale;
        //    var rectTrans = GetComponent<RectTransform>();
  
        //    rectTrans.sizeDelta = new Vector2(75, 75);

        //    //layoutGroup.SetLayoutHorizontal();
            
        //}

        // Maybe move this to only check after each move
        if(UnitManager.instance.CurrentUnit.remainingActionPoints < ability.apCost)
        {
            var col = button.image.color;
            col.a = .5f;
            button.image.color = col;
        }
        else
        {
            var col = button.image.color;
            col.a = 1;
            button.image.color = col;
        }

        if(!ability.CanUse(UnitManager.instance.CurrentUnit))
        {
            button.interactable = false;
            if (ability.coolDownCounter > 0)
            {
                cdBackground.SetActive(true);
                coolDownText.text = ability.coolDownCounter.ToString();
                cdBackgroundFill.fillAmount = (float)ability.coolDownCounter / (float)ability.coolDown;
            }
        }
        else
        {
            button.interactable = true;
            cdBackground.SetActive(false);
        }
        if (ability.hasCharges)
        {
            chargesText.text= ability.charges.ToString();
        }
    }

	void OnClick()
	{
		if(ability.CanUse(UnitManager.instance.CurrentUnit))
		{
			UnitManager.instance.CurrentAbility = ability;
            UnitManager.instance.button = this;
            button.image.color = selectedColor;

            rectTrans.sizeDelta = baseSize * 1.3f;
            foreach(var rect in childRects)
            {
                rect.transform.localScale = rect.transform.localScale * 1.3f;
            }

            UnitManager.instance.CurrentUnit.potentialPath = null;
        }
	}

    public void Reset()
    {
        button.image.color = Color.white;
        rectTrans.sizeDelta = baseSize;
        foreach (var rect in childRects)
        {
            rect.transform.localScale = Vector3.one; 
        }
    }

	public static void ShowAbilities(Unit unit)
	{
		if (unit)
		{
			if (unit.abilities.Count > iconPool.Count)
				AddExtra();

			for (int i = 0; i < unit.abilities.Count; ++i)
			{
                if (iconPool[i].key == KeyCode.None)
                {
                    iconPool[i].key = KeyCode.Alpha1 + i;
                    iconPool[i].hotkey.text = (1 + i).ToString();
                }

                iconPool[i].ability = unit.abilities[i];
				iconPool[i].button.image.sprite = unit.abilities[i].icon;
                iconPool[i].toolTip.tooltipInfo = "<b>" + iconPool[i].ability.name + "</b> \n" + "<b>Cost</b> " + iconPool[i].ability.apCost +
                    "AP";
                if (iconPool[i].ability.coolDown > 0)
                {
                    iconPool[i].toolTip.tooltipInfo += " <b>Cooldown </b>:" + iconPool[i].ability.coolDown;

                }
                if (iconPool[i].ability.damage != 0)
                {
                    if (iconPool[i].ability.damageMod == AbilityBase.DamageMod.Modifier)
                    {
                        iconPool[i].toolTip.tooltipInfo += "\n<b>Damage</b>:" + Mathf.Abs(unit.stats.damageRange.min * iconPool[i].ability.damage) + "-" + Mathf.Abs(unit.stats.damageRange.max * iconPool[i].ability.damage);
                    }
                    else
                    {
                        iconPool[i].toolTip.tooltipInfo += "\n<b>Damage</b>:" + Mathf.Abs(iconPool[i].ability.damage);
                    }
                }
                if (iconPool[i].ability.range.max == 0)
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Range</b>: Self";
                }
                else if (iconPool[i].ability.range.min > 0)
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Range</b>:" + iconPool[i].ability.range.min + " - " + iconPool[i].ability.range.max;
                }
                else
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Range</b>:" + iconPool[i].ability.range.max;
                }
                switch (iconPool[i].ability.areaOfEffect)
                {
                    case AbilityBase.AreaOfEffect.AreaOfEffect:
                        iconPool[i].toolTip.tooltipInfo += "\n<b>AoE</b>:" + iconPool[i].ability.aoeSize;
                        break;
                    case AbilityBase.AreaOfEffect.Chaining:
                        iconPool[i].toolTip.tooltipInfo += "\n<b>Chaining</b>:" + iconPool[i].ability.bounces + " targets \n <b>Chain Range</b>:" + iconPool[i].ability.bounceRange;
                        break;
                    default:
                        break;
                }
                if (!iconPool[i].ability.hasCharges)
                {
                    iconPool[i].chargesText.text = "";
                }
                else
                {
                    iconPool[i].toolTip.tooltipInfo += "\n<b>Charges Left</b>:" + iconPool[i].ability.charges;

                }
                iconPool[i].toolTip.tooltipInfo += "\n" + iconPool[i].ability.description;
                iconPool[i].gameObject.SetActive(true);
                iconPool[i].transform.parent.GetComponent<HorizontalLayoutGroup>().CalculateLayoutInputHorizontal();
			}
		}
	}

	public static void HideAbilities()
	{
		for (int i = 0; i < iconPool.Count; ++i)
		{
			if (iconPool[i].isActiveAndEnabled)
				iconPool[i].gameObject.SetActive(false);
			else
				break;
		}
	}

	public static void AddExtra()
	{
		var icon = Instantiate(iconPool[0].gameObject);

		icon.transform.SetParent(iconPool[0].transform.parent);

		iconPool.Add(icon.GetComponent<AbilityIcon>());

		icon.SetActive(false);		
	}

    void OnDestroy()
    {
        iconPool = null;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UnitManager.instance.CurrentUnit.actionCost = ability.apCost;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UnitManager.instance.CurrentUnit.actionCost = 0;
    }
}
