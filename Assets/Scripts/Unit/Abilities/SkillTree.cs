﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "T Skill Tree", menuName = "Skill Tree/Tree", order = 1)]
public class SkillTree : ScriptableObject
{
    public List<SkillSet> sets = new List<SkillSet>();

    public List<SkillSet> setsLearnt = new List<SkillSet>();

    public List<SkillSet> setsBlocked = new List<SkillSet>();

    public void ApplySet(Unit unit, SkillSet skillSet)
    {
        foreach (var ability in skillSet.replacedAbilities)
        {
            if (unit.abilityList.Contains(ability))
            {
                unit.abilityList.Remove(ability);
            }
        }
        foreach (var ability in skillSet.abilities)
        {
            if (!unit.abilityList.Contains(ability))
                unit.abilityList.Add(ability);
        }

        unit.skillTree.setsLearnt.Add(skillSet);

        foreach (var blocked in skillSet.blocks)
        {
            unit.skillTree.setsBlocked.Add(blocked);
        }

        unit.level.skillPoints -= skillSet.cost;

        if(skillSet.hasStatsMod)
        {
            unit.baseStats += skillSet.statsMod;
            unit.escapeCost += skillSet.extraExitCost;
        }

        if (skillSet.classChange != "")
        {
            unit.unitClass = skillSet.classChange;
        }
    }

    

    //public List<AbilityBase> abilities = new List<AbilityBase>();

    //public List<int> unlockLevel = new List<int>();

    //public List<AbilityBase> abilitiesLearnt = new List<AbilityBase>();
}
