﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityObstacleObject : MonoBehaviour
{
    public float duration;

    [HideInInspector]
    public List<CellInfo> obstructedTiles = new List<CellInfo>();

    public List<SphereCollider> sphereColliders = new List<SphereCollider>();

    public List<BoxCollider> boxColliders = new List<BoxCollider>();

    Unit caster;

    AbilityObstacle ability;

    // Use this for initialization
    void Start()
    {
        if (sphereColliders.Count == 0 && boxColliders.Count == 0)
        {
            foreach (var col in GetComponentsInChildren<SphereCollider>())
            {
                sphereColliders.Add(col);
            }

            foreach (var col in GetComponentsInChildren<BoxCollider>())
            {
                boxColliders.Add(col);
            }
        }

        obstructedTiles.Clear();

        duration = 0;

        TurnManager.instance.OnStartTurn.AddListener(Tick);
    }

    // Update is called once per frame
    void Update()
    {
        foreach(var tile in obstructedTiles)
        {
            if (!tile.tile.Unobstructed)
                tile.SetRed();
        }
    }

    void Tick(UnitType e)
    {
        if (e == UnitType.Player)
        {
            duration--;

            if (ability)
                Damage();

            if (duration <= 0)
                Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        foreach(var tile in obstructedTiles)
        {
            if (tile)
            {
                tile.tile.Unobstructed = true;
                tile.SetDefault();
            }
        }

        TurnManager.instance.OnStartTurn.RemoveListener(Tick);
    }

    void ObstructTiles()
    {
        foreach(var tile in obstructedTiles)
        {
            tile.tile.Unobstructed = false;
            tile.SetRed();
        }
    }

    /// <summary>
    /// Deals damage/heal if set to do so
    /// otherwise does nothing
    /// </summary>
    void Damage()
    {
        if (ability.damage != 0)
        {
            // Gets target mask for ability
            var mask = ability.GetTargetMask(caster);
            foreach (var tile in obstructedTiles)
            {
                if (tile && tile.unit)
                {
                    // Checks if unit is a valid target
                    if ((tile.unit.unitType & mask) != 0)
                    {
                        ability.DealDamage(caster, tile.unit, ability.Damage(caster, tile.unit));
                    }
                }
            }
        }
    }

    public void Set(float duration, AbilityObstacle ability, Unit caster)
    {
        this.duration = duration;
        this.ability = ability;
        this.caster = caster;

        Damage();

        if (!ability.noPushBack)
        {
            ObstructTiles();
            PushBackUnits();
        }

        if (duration <= 0)
        {
            Destroy(gameObject);
        }
        gameObject.layer = 0;
    }

    public void PushBackUnits()
    {
        foreach(var tile in obstructedTiles)
        {
            if(tile && tile.unit)
            {
                var dir = tile.unit.transform.position - ability.attackOrigin;

                if(Vector3.Magnitude(dir) < 1)
                {
                    dir = tile.unit.transform.position - caster.CurrentTile.transform.position;
                }

                dir = dir.normalized * 2f;

                tile.unit.transform.position += dir;

                tile.unit.SnapToClosestTile();
            }
        }
    }

    public void Preview(Transform target, Vector3 dir)
    {
        // set to ignore raycast
        gameObject.layer = 2;
        if (obstructedTiles.Count > 0)
        {
            foreach (var tile in obstructedTiles)
            {
                if (tile)
                {
                    if (tile.tile.Unobstructed)
                        tile.SetDefault();
                }
            }

            obstructedTiles.Clear();
        }

        transform.position = target.position;
        transform.forward = dir;
        gameObject.SetActive(true);

        foreach (var sphere in sphereColliders)
        {
            foreach (var obj in Physics.OverlapSphere(sphere.bounds.center, sphere.radius))
            {
                CellInfo tile = obj.GetComponent<CellInfo>();

                if (tile && !obstructedTiles.Contains(tile))
                {
                    obstructedTiles.Add(tile);
                    tile.SetRed();
                }
            }
        }

        foreach (var box in boxColliders)
        {
            foreach (var obj in Physics.OverlapBox(box.bounds.center, Vector3.Scale(box.size, box.transform.lossyScale * 0.5f), box.transform.rotation))
            {
                CellInfo tile = obj.GetComponent<CellInfo>();

                if (tile && !obstructedTiles.Contains(tile))
                {
                    obstructedTiles.Add(tile);
                    tile.SetRed();
                }
            }
        }
    }
}

