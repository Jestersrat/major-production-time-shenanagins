﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class MinMax
{
	[SerializeField]
	public float min =0;
	[SerializeField]
	public float max =1;
}

[CreateAssetMenu(fileName = "Ability", menuName = "Abilities/Ability", order = 1)]
public class AbilityBase : ScriptableObject
{
	#region enums
	public enum AbilityType
	{
		Instant,
		Projectile,
	}
	public enum Targets
	{
		All,
		Hostile,
		Ally
	}
	public enum AreaOfEffect
	{
		SingleTarget,
		AreaOfEffect,
		Chaining
	}
	public enum DamageMod
	{
		Modifier,
		Static
	}
	#endregion

	#region General
	[Header("General")]
	[SerializeField]
    [Tooltip("Determines whether ability deals a set amount of damage or is based on unit's damage stat")]
	public DamageMod damageMod;
	public float damage = 1;
	[SerializeField]
	public AbilityType abilityType;
    [Tooltip("Accuracy of ability. Value is additive")]
    [Range(-100, 100)]
    public float accuracy = 0;
    [Tooltip("Extra crit chance on ability. Value is added to base crit of unit")]
    public float critChance = 0;
    [Tooltip("Applied multiplicatively. 1 for base crit")]
    public float critMod = 1;

    [Tooltip("Hit Effects trigger when an ability hits the target")]
    public AbilityBase[] hitEffects;

    [Tooltip("Makes hit target the caster of hiteffects if true")]
    public bool makeTargetCaster;

	[Tooltip("Whether this spell can be used a limited amount")]
	public bool hasCharges = false;
	public int charges;
    public int maxCharges;
	[Tooltip("Any status effects the ability causes")]
	public StatusEffects[] statusEffects;
	[Header("Cost")]
	[Tooltip("Cost in Action Points")]
	public int apCost;
	[Tooltip("How many turns the unit must wait to use again, set to 1 to allow only 1 use a turn")]
	public int coolDown;
    public int coolDownCounter = 0; // Used to track cooldown
	#endregion

	#region targets
	[Header("Targeting")]
	public Targets targets = Targets.Hostile;
    [Tooltip("What layers this can hit, leave it alone unless you have a good reason to do otherwise")]
    [SerializeField]
    public MinMax range;
	public bool requireLOS=true;
	#endregion

	#region AOE & Chainning
	[Header("AoE")]
	public AreaOfEffect areaOfEffect;
	public LayerMask clickLayer = 2048;
	[Tooltip("How many tiles this aoe can affect")]
	public int aoeSize;
	[Header("Chaining")]
	[Tooltip("How many times this ability can bounce")]
	public int bounces;
	[Tooltip("Time delay between bounces")]
	public float bounceDelay = .3f;
	public bool bouncing = false;
	[Tooltip("Whether this ability can hit the same target more than once")]
	public bool canReHit;
	[Tooltip("How far this ability can bounce between target")]
	public int bounceRange;
    #endregion

    #region Projectile
    [Header("Projectile")]
    [Tooltip("Projectile prefab, with a projectile script. Only use if the ability is set to projectile")]
    public GameObject projectilePrefab;
    [Tooltip("How fast the projectile moves")]
    public float projectileSpeed = 10;

    public int projectileCount = 0;

    public bool Executing
    {
        get
        {
            return !(projectileCount == 0 && !bouncing);
           // return projectileCount > 0;
        }
    }
    #endregion

    #region Visuals
    [Header("Visuals")]
    public bool shakeOnHit = false;
    public float shakeAmount = 0.25f;
    public float shakeDuration = 0.2f;

	public StaticValues.ColorCode abilityColor;
	public ParticleSystem particles;
    public Vector3 particlePosition = Vector3.up;
	[Tooltip("The Icon for the skill")]
	public Sprite icon;
    #endregion

    #region Audio
    [Header("Audio")]
    public AudioClip castSound;
    public AudioClip hitSound;
    #endregion

    [TextArea]
	public string description;

    public Vector3 attackOrigin;

	/// <summary>
	/// Initialises particle pool
	/// </summary>
	public virtual void Initialise()
	{
		if (particles)
		{
			PoolMananger.instance.AddPSList(particles);
		}
        if (hasCharges)
        {
            maxCharges = charges;
        }

        airHorn = Resources.Load("mlg-airhorn") as AudioClip;
	}

    /// <summary>
    /// Returns attack damage of ability if used by provided "user" unit
    /// Applies damage reduce based on target's defence if one is passed in
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public float Damage(Unit user, Unit target = null)
	{
        float defenceMod = 1;

        if (target)
            defenceMod -= (target.stats.Defence());

        if (damageMod == DamageMod.Static)
            return damage * defenceMod;


		return damage * user.stats.Damage * defenceMod;
	}

    private AudioClip airHorn;

	/// <summary>
	/// Returns a mask with valid targets for attack from user
	/// </summary>
	/// <param name="user"></param>
	/// <returns></returns>
	public UnitType GetTargetMask(Unit user)
	{
		UnitType mask = 0;
		
		if (targets == Targets.Hostile)
		{
			if (user.unitType == UnitType.Player || user.unitType == UnitType.Ally)
			{
				mask = UnitType.Player | UnitType.Ally;
				mask = ~mask;
			}
			else
				mask = ~UnitType.Hostile;
		}
		else if (targets == Targets.Ally)
		{
			if (user.unitType == UnitType.Player || user.unitType == UnitType.Ally)
				mask = UnitType.Player | UnitType.Ally;
			else
				mask = UnitType.Hostile;
		}
		else
		{
			// return mask for all
			return ~mask;
		}

		return mask;
	}

    /// <summary>
    /// Returns a list of targets that can be hit by skill
    /// Will return all units in AOE if skill is aoe
    /// </summary>
    /// <param name="user"></param>
    /// <param name="targetTile"></param>
    /// <returns></returns>
    public HashSet<Unit> PossibleTargets(Unit user, CellInfo targetTile)
	{
		HashSet<Unit> targets = new HashSet<Unit>();

		var mask = GetTargetMask(user);

		switch (areaOfEffect)
		{
			case AreaOfEffect.SingleTarget:
				targets.Add(targetTile.unit);
				break;
			case AreaOfEffect.AreaOfEffect:
				GridManager.GetUnitsInRange(targetTile, aoeSize, out targets, mask);
				if (targetTile.unit && (targetTile.unit.unitType & mask) != 0)
					targets.Add(targetTile.unit);
				break;
			case AreaOfEffect.Chaining:
				break;
			default:
#if UNITY_EDITOR
                Debug.Log("Error evaluating possible targets for " + name);
#endif
                break;
		}

		return targets;
	}

    /// <summary>
    /// Returns a list of potential chain bounce targets
    /// </summary>
    /// <param name="caster"></param>
    /// <param name="target"></param>
    /// <param name="HitAlready"></param>
    /// <returns></returns>
    protected List<Unit> ChainTargets(Unit caster, Unit target, HashSet<Unit> HitAlready = null)
	{		
		List<Unit> targets = new List<Unit>();

		var mask = GetTargetMask(caster);

        if(canReHit && bounceRange == 0)
        {
            if (!target.isDead)
                targets.Add(target);
            return targets;
        }

		HashSet<Unit> targetList;

		GridManager.GetUnitsInRange(target.CurrentTile, bounceRange, out targetList, mask);

		foreach (var unit in targetList)
		{
			if (unit && !unit.isDead)
			{
				if (canReHit)
					targets.Add(unit);
				else if (!HitAlready.Contains(unit))
				{
                    if (!requireLOS || (requireLOS && CheckLOS(target, unit.CurrentTile, true)))
                        targets.Add(unit);
				}
			}
		}
		
		return targets;
	}

    /// <summary>
    /// Attempts to deal damage to target
    /// Miss and crit calculations are done in here
    /// </summary>
    /// <param name="user"></param>
    /// <param name="target"></param>
    /// <param name="damageValue"></param>
    public void DealDamage(Unit user, Unit target, float damageValue)
	{
		if (target && user)
		{
            // Reduces damage by defence
            if (damageValue > 0)
            {
                damageValue *= (1f - target.stats.Defence());
                user.damageDealt += damageValue;
                target.damageTaken += damageValue;
            }

            foreach (var a in hitEffects)
            {
                if (makeTargetCaster)
                    a.Execute(target, target.CurrentTile);
                else
                    a.Execute(user, target.CurrentTile);
            }

            if (areaOfEffect != AreaOfEffect.AreaOfEffect)
            {
                // Play Particles
                if (particles)
                    PlayParticle(target.transform, user);
                
                // Play Hit sound at target
                if (hitSound)
                    PlayHitAudio(target.transform);
            }

            // Activates ai units 
            if (target.unitType == UnitType.Hostile)
            {
                var agent = target as Agent;
                agent.Activated = true;
            }

            damageValue = Mathf.RoundToInt(damageValue);

            // Checks hit
            if (RNGSucess(CalculateHitChance(user, target)))
            {
                // Applying status effects on hit
                ApplyStatusEffects(user, target);

                // Checks crit
                var critSuccess = RNGSucess(CalculateCritChance(user));

                if (critSuccess)
                {
                    damageValue = ApplyCritBonus(user, damageValue);
                    damageValue = Mathf.RoundToInt(damageValue);
                    // Shakes camera
                    RTSCamera.instance.CameraShake(.2f, .25f);
                    RTSCamera.instance.SlowTime(.5f, .25f);
                    if (StaticValues.instance.airHorn)
                    {
                        AudioMananger.instance.PlayClip(airHorn, user.transform);
                    }
                }

                // Overrides crit's shake if has shake on hit
                if (shakeOnHit)
                    RTSCamera.instance.CameraShake(shakeDuration, shakeAmount);


                // 
                // Damage Dealt here
                //
                target.ApplyDamage(damageValue, user, this);

                if (damageValue > 0)
                {
                    if (MessageLog.instance)
                        MessageLog.instance.AddEvent(user.name + ((critSuccess) ? " crits " : " hits ") + target.name + " for<color=red>"+ damageValue+" </color>");

                    if (PoolMananger.instance)
                    {
                        var text = PoolMananger.instance.GetPooledText(damageValue.ToString(), Color.red, target.transform.position,target);

                        if (critSuccess)
                        {
                            var scale = text.transform.localScale;
                            scale *= 2;
                            text.transform.localScale = scale;
                        }
                    }
                }
                else if (damageValue == 0)
                {
                    if (MessageLog.instance)
                        MessageLog.instance.AddEvent(user.name + ((critSuccess) ? " crits " : " hits ") + target.name + " with " + name);
                }
                else if(damageValue < 0)
                {
                    if (MessageLog.instance)
                        MessageLog.instance.AddEvent(user.name + ((critSuccess) ? " crits heals" : " heals ") + target.name + " for<color=green>"+ Mathf.Abs(damageValue)+"</color>");

                    if (PoolMananger.instance)
                    {
                        PoolMananger.instance.GetPooledText("+"+Mathf.Abs(damageValue), Color.green, target.transform.position, target);
                    }
                }

            }
            else // Attack missed
            {
                foreach (Animator anim in target.animators)
                {
                    anim.SetFloat("Blend", target.blends[Random.Range(0, target.blends.Length)]);
                    anim.SetTrigger("BlockDamage");
                }
                // AOE can never miss only deal half damage
                if (areaOfEffect == AreaOfEffect.AreaOfEffect)
                {
                    // Checks crit
                    var critSuccess = RNGSucess(CalculateCritChance(user));

                    if (critSuccess)
                        damageValue = ApplyCritBonus(user, damageValue);

                    // halve damage on aoe miss
                    damageValue *= 0.5f;

                    damageValue = Mathf.RoundToInt(damageValue);

                    // 
                    // Damage Dealt here
                    //
                    target.ApplyDamage(damageValue, user, this);

                    if (critSuccess)
                    {
                        damageValue = ApplyCritBonus(user, damageValue);
                        // Shakes camera
                        RTSCamera.instance.CameraShake(.2f, .25f);
                    }

                    // Overrides crit's shake if has shake on hit
                    if (shakeOnHit)
                        RTSCamera.instance.CameraShake(shakeDuration, shakeAmount);


                    //if (particles && )
                    //{
                    //    PlayParticle(target.transform);
                    //}

                    if (MessageLog.instance)
                        MessageLog.instance.AddEvent(user.name + " glances " + target.name + " for " + damageValue);

                    if (PoolMananger.instance)
                    {
                        PoolMananger.instance.GetPooledText(damageValue.ToString(), Color.red, target.transform.position);
                    }
                }
                else
                {
                    if (MessageLog.instance)
                        MessageLog.instance.AddEvent(user.name + " missed " + target.name + " with " + name);

                    if (PoolMananger.instance)
                    {
                        PoolMananger.instance.GetPooledText("Miss", Color.red, target.transform.position);
                    }
                }
            }
        }
		else
		{
#if UNITY_EDITOR
            if (!target)
				Debug.Log("Target is null");

			if(!user)
				Debug.Log("User is null");
#endif
        }
	}

    /// <summary>
    /// Needs to be started a called from a monobehaviour
    /// </summary>
    /// <param name="user"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    protected IEnumerator ExecuteChain(Unit user, Unit target)
	{
		bouncing = true;
		int bounceCount = 0;
		HashSet<Unit> hits = new HashSet<Unit>();

        // Adds target to hit list if units can only be hit once
		if (!canReHit)
			hits.Add(target);

        // ????
		if (!target)
			canReHit = false;
		
		DealDamage(user, target, Damage(user));

        attackOrigin = target.transform.position;

		while (bounceCount < bounces)
		{
			yield return new WaitForSecondsRealtime(bounceDelay);

			var chainTargets = ChainTargets(user, target, hits);

			if (chainTargets.Count == 0)
				break;

			int count = chainTargets.Count;

			int rand = Random.Range(0, count);

			// Sets target to random target in list
			target = chainTargets[rand];

			if (!target)
				canReHit = false;

            // Add target to hits before adding to list
            if (!canReHit)
                hits.Add(target);

			DealDamage(user, target, Damage(user));

            attackOrigin = target.transform.position;

			++bounceCount;
		}

        bouncing = false;
	}

    protected IEnumerator ExecuteProjectileChain(Unit user, Unit target)
	{
		bouncing = true;

		int bounceCount = 0;

		HashSet<Unit> hits = new HashSet<Unit>();

		if (!canReHit)
			hits.Add(target);

		if (!target)
			canReHit = false;

		DealDamage(user, target, Damage(user));

        attackOrigin = target.transform.position;

        // Used to determine where to start chaining from
        var currentUnitHit = target;

		while (bounceCount < bounces)
		{
			var chainTargets = ChainTargets(user, target, hits);

			if (chainTargets.Count == 0)
				break;

			int count = chainTargets.Count;

			int rand = Random.Range(0, count);

			// Sets target to random target in list
			target = chainTargets[rand];

			if (!target)
				canReHit = false;

			var projectile = Instantiate(projectilePrefab).GetComponent<Projectile>();

			// fires projectile at target
			projectile.Shoot(user, this, target.CurrentTile, currentUnitHit);

			projectile.trigger = true;

			// Waits until projectile hits before dealing damage
			yield return new WaitUntil(() => projectile.hit);

            // Add target to hits before adding to list
            if (!canReHit)
                hits.Add(target);

            DealDamage(user, target, Damage(user));

            attackOrigin = target.transform.position;

            // Set new chain position to current hit target
            currentUnitHit = target;

			++bounceCount;
		}
        
        bouncing = false;
	}

	/// <summary>
	/// Applies AP cost to user
	/// Sets spell on cooldown if it has one
	/// Reduces charges if ability has charges
	/// </summary>
	/// <param name="user"></param>
	protected void ApplyCost(Unit user)
	{
		if (hasCharges)
			charges--;

		coolDownCounter = coolDown;
        
        user.stats.actionPoints -= apCost;
	}

	/// <summary>
	/// Trys to execute ability given user and targeted tile
	/// returns boolean on whether ability was successfuly used
	/// </summary>
	/// <param name="user"></param>
	/// <param name="targetTile"></param>
	/// <returns></returns>
	public virtual bool Execute(Unit user, CellInfo targetTile)
	{
		// Exits early if no charges
		if(hasCharges && charges == 0)		
			return false;

		// Exits early if not enough ap
		if (user.stats.actionPoints < apCost)
			return false;

		// Exits if targetTile does not have a unit and ability is not an area of effect
		if (!targetTile.unit && areaOfEffect != AreaOfEffect.AreaOfEffect)
			return false;

        // Exits if targeting unit which shouldn't be targeted, except for when ability is aoe
        if (targetTile.unit)
        {
            var mask = ~GetTargetMask(user);
            if ((targetTile.unit.unitType & mask) != 0 && areaOfEffect != AreaOfEffect.AreaOfEffect)
                return false;
        }

		bool performed = false;

		if (coolDownCounter <= 0)
		{
            // Sets origin for knockback visuals
            if (areaOfEffect == AreaOfEffect.AreaOfEffect)
            {
                attackOrigin = targetTile.transform.position;
            }
            else
            {
                attackOrigin = user.transform.position;
            }

			var distance = GridManager.Distance(user.CurrentTile.gridPos, targetTile.gridPos);
            if (distance <= range.max && (distance >= range.min))// || range.min == 0))
			{
                // Exits if skill requires los and unit is not in los
                if (requireLOS && !CheckLOS(user, targetTile))
                    return false;

				if (abilityType == AbilityType.Projectile)
				{
					var p = Instantiate(projectilePrefab).GetComponent<Projectile>();
                    
                    p.Shoot(user, this, targetTile);

                    projectileCount++;

                    if (areaOfEffect == AreaOfEffect.Chaining)
                        bouncing = true;
                    
                    performed = true;
				}
				else
				{
					// List of targets
					// AOE considerations done in here as well
					var targets = PossibleTargets(user, targetTile);
					
					if (areaOfEffect != AreaOfEffect.Chaining)
					{
                        if (areaOfEffect == AreaOfEffect.AreaOfEffect)
                        {
                            if (particles)
                                PlayParticle(targetTile.transform, user);

                            if (hitSound)
                                PlayHitAudio(targetTile.transform);
                        }

                        foreach (var target in targets)
						{
							if (!target)
								canReHit = false;

							DealDamage(user, target, Damage(user));
						}

						performed = true;
					}
					else
					{
						bouncing = true;
						UnitManager.instance.StartCoroutine(ExecuteChain(user, targetTile.unit));


                        performed = true;
					}
				}
			}
		}

        if (performed)
        {
            ApplyCost(user);
            foreach(Animator anim in user.animators)
            {
                anim.SetFloat("Blend", user.blends[Random.Range(0, user.blends.Length)]);
                anim.SetTrigger("Attack");
            }
            if (castSound)
                PlayCasterAudio(user);

            FaceTargetDirection(user, targetTile);
        }

		return performed;
	}

    /// <summary>
    /// Makes user face adjacent tile closest to tile passed in
    /// </summary>
    /// <param name="user"></param>
    /// <param name="tile"></param>
    public void FaceTargetDirection(Unit user, CellInfo tile)
    {
        // Returns early and doesn't change facing if target tile is current tile
        if (user.CurrentTile == tile)
            return;

        var score = Mathf.Infinity;               

        // tile to face to
        CellInfo fTile = null;
            
        foreach(var cell in user.CurrentTile.tile.ActualAllNeighbours)
        {
            var dist = Vector3.Distance(cell.info.transform.position, tile.transform.position);
            if(dist < score)
            {
                score = dist;
                fTile = cell.info;
            }
        }

        var dir = fTile.transform.position - user.transform.position;

        dir.y = 0;

        dir.Normalize();

        user.transform.forward = dir;
    }

	/// <summary>
	/// Reduces remaining cooldown by passed in value
	/// </summary>
	/// <param name="value"></param>
	public void CoolDownTick(int value)
	{
		if (coolDownCounter > 0)
			coolDownCounter -= value;
	}

	/// <summary>
	/// Checks if user can use ability
	/// Checks cooldoown
	/// Checks ap
	/// Checks charges if any
	/// </summary>
	/// <param name="user"></param>
	/// <returns></returns>
	public bool CanUse(Unit user, Unit target = null)
	{
        if(target)
        {
            var distance = GridManager.Distance(user.CurrentTile.gridPos, target.CurrentTile.gridPos);
			
            if (!(distance <= range.max && distance >= range.min))
            {
                return false;
            }

			if (requireLOS && !CheckLOS(user, target.CurrentTile))
				return false;
		}

		if(coolDownCounter <= 0 && user.stats.actionPoints >= apCost)
		{
			if ((hasCharges && charges > 0) || !hasCharges)
				return true;
		}

		return false;		
	}

	public void ProjectileHit(Unit user, CellInfo hit)
	{
		//var grid = GridManager.instance.GetGridFromWorldPos(hit.transform.position);

        projectileCount--;

		if (hit)
		{
			var targets = PossibleTargets(user, hit);

			if (areaOfEffect != AreaOfEffect.Chaining)
			{
                if (areaOfEffect == AreaOfEffect.AreaOfEffect)
                {
                    if (particles)
                        PlayParticle(hit.transform, user);

                    if (hitSound)
                        PlayHitAudio(hit.transform);
                }

				foreach (var target in targets)
				{
					if (!target)
						canReHit = false;
                    
					DealDamage(user, target, Damage(user));
				}
			}
			else
			{
				if (abilityType == AbilityType.Projectile)
					UnitManager.instance.StartCoroutine(ExecuteProjectileChain(user, hit.unit));
				else
					UnitManager.instance.StartCoroutine(ExecuteChain(user, hit.unit));
			}
		}
		else
		{
#if UNITY_EDITOR
            Debug.Log("projectile flopped");
#endif
        }
	}

    /// <summary>
    /// Returns hit chance to hit target as a percentage 0-100
    /// </summary>
    /// <param name="user"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public float CalculateHitChance(Unit user, Unit target)
    {
        if(areaOfEffect == AreaOfEffect.AreaOfEffect)
        {
            return user.stats.HitChance(0, accuracy);
        }

        if (targets == Targets.Ally)
        {
            if (CheckLOS(user, target.CurrentTile))
                return 100;
            else
                return 0;
        }

        float losMod = 1;

        if (requireLOS)
        {
            var head = user.GetHeadPos();

            var targetHead = target.GetHeadPos();

            float height = Vector3.Distance(targetHead, target.transform.position);

            float increments = height / 5;

            float rayDist = Vector3.Distance(user.CurrentTile.transform.position, target.CurrentTile.transform.position) - 0.1f;

            LayerMask mask = LayerMask.GetMask("Default", "Terrain");

            for (int i = 0; i < 5; i++)
            {
                var rayTarget = targetHead;

                rayTarget.y -= increments * i;

                var rayDir = rayTarget - head;

                rayDir.Normalize();

                RaycastHit hit;

                if (Physics.Raycast(head, rayDir, out hit, rayDist, mask))
                {
#if UNITY_EDITOR
                    Debug.DrawLine(head, hit.point, Color.red, 10);
                    Debug.Log(hit.transform.name + " from accuracy");
#endif
                    // reduce by flat % for each missed ray
                    losMod -= 0.2f;
                }
            }
        }
        
        return user.stats.HitChance(target, accuracy) * losMod;
    }

    public float CalculateCritChance(Unit user)
    {
        return user.stats.critChance + critChance;
    }

    /// <summary>
    /// Returns critical value
    /// </summary>
    /// <param name="damageValue"></param>
    /// <returns></returns>
    public float ApplyCritBonus(Unit user, float damageValue)
    {
        return damageValue * user.stats.critMod * critMod;
    }

    protected bool RNGSucess(float hit)
    {
        var rand = Random.Range(1, 100);

        return hit > rand;
    }

    protected void ApplyStatusEffects(Unit user, Unit target)
    {
        foreach(var status in statusEffects)
        {
            var s = Instantiate(status);

            s.name = status.name;

            s.Initialise(user, target, this);
        }
    }

    protected void PlayParticle(Transform target, Unit user = null)
    {
        var p = PoolMananger.instance.PSGet(particles);

        p.transform.position = target.position + particlePosition;

        var dir = target.transform.position - attackOrigin;

        dir.y = 0;

        dir.Normalize();

        if (dir != Vector3.zero)
            p.transform.forward = dir;

        PoolMananger.instance.ActivatePS(p);
    }

    /// <summary>
    /// Plays cast sound at caster if there is one
    /// </summary>
    /// <param name="caster"></param>
    protected void PlayCasterAudio(Unit caster)
    {
        caster.audioSource.clip = castSound;
        caster.audioSource.pitch = Random.Range(0.75f, 1.25f);
        caster.audioSource.Play();
        //caster.audioSource.PlayOneShot(castSound);
    }

    /// <summary>
    /// Plays hit sound at target transform
    /// </summary>
    /// <param name="target"></param>
    protected void PlayHitAudio(Transform target)
    {
        if (AudioMananger.instance)
            AudioMananger.instance.PlayClip(hitSound, target);
        else
        {
#if UNITY_EDITOR
            Debug.LogError("Missing Audio Manager");
#endif
        }
    }

    public bool CheckLOS(Unit user, CellInfo targetTile, bool showObstruction = false)
    {
        if(areaOfEffect == AreaOfEffect.AreaOfEffect || !targetTile.unit)
        {
            var head = user.GetHeadPos();

            var dir = (targetTile.fog.losTarget - head).normalized;

            var dist = Vector3.Distance(head, targetTile.fog.losTarget);

            RaycastHit hit;

            LayerMask mask = LayerMask.GetMask("Default", "Terrain");
            //LayerMask mask = ~LayerMask.GetMask("Friendly", "Hostile", "Ignore Raycast", "Build", "UI");

            if (Physics.Raycast(head, dir, out hit, dist, mask))
            {
                if(showObstruction)
                {
                    PoolMananger.instance.GetPooledText("Obstructed", Color.red, hit.point);
                }

#if UNITY_EDITOR
                Debug.DrawLine(head, hit.point, Color.red, 10);

                Debug.Log(hit.transform.name + " from los check");
#endif
            }
            else
                return true;
        }
        else if(targetTile.unit)
        {
            var head = user.GetHeadPos();

            var targetHead = targetTile.unit.GetHeadPos();

            float height = Vector3.Distance(targetHead, targetTile.unit.transform.position);

            float increments = height / 5;

            float rayDist = Vector3.Distance(user.CurrentTile.transform.position, targetTile.transform.position);

            LayerMask mask = LayerMask.GetMask("Default", "Terrain");
            //LayerMask mask = ~LayerMask.GetMask("Friendly", "Hostile", "Ignore Raycast", "Build", "UI");

            for (int i = 0; i < 5; i++)
            {
                var rayTarget = targetHead;

                rayTarget.y -= increments * i;

                var rayDir = rayTarget - head;

                rayDir.Normalize();

                RaycastHit hit;

                if (Physics.Raycast(head, rayDir, out hit, rayDist, mask))
                {
#if UNITY_EDITOR
                    Debug.DrawLine(head, hit.point, Color.red, 10);
#endif
                }
                else
                {
                    return true;
                }
            }
        }

        return false;
    }
}
