﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObstacleAbility", menuName = "Abilities/Obstacle Ability", order = 1)]
public class AbilityObstacle : AbilityBase
{
    [Header("Obstacle Properties")]
    public GameObject obstacleObject;
    public float duration;
    public bool canTargetTileWithUnit = false;
    public bool noPushBack = false;

    public override void Initialise()
    {
        base.Initialise();
    }

    public override bool Execute(Unit user, CellInfo targetTile)
    {
        bool executed = false;

        // Exits early if no charges
        if (hasCharges && charges == 0)
            return false;

        // Exits early if not enough ap
        if (user.stats.actionPoints < apCost)
            return false;

        if (targetTile.unit && !canTargetTileWithUnit)
            return false;


        if (coolDownCounter <= 0)
        {
            var distance = GridManager.Distance(user.CurrentTile.gridPos, targetTile.gridPos);
            if (distance <= range.max && (distance >= range.min))
            {
                // Exits if skill requires los and unit is not in los
                if (requireLOS && !CheckLOS(user, targetTile))
                    return false;

                attackOrigin = targetTile.transform.position;

                UnitManager.instance.CurrentObstacle.Set(duration, this, user);                

                executed = true;
            }
        }

        if (executed)
        {
            ApplyCost(user);

            if (castSound)
                PlayCasterAudio(user);

            FaceTargetDirection(user, targetTile);
        }

        return executed;
    }
}
