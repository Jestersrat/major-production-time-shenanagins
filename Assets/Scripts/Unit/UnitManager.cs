﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Fungus;

[System.Serializable]
/// <summary>
/// Sends unity that died
/// </summary>
public class UnitEvents : UnityEvent<Unit> { }

public class UnitManager : MonoBehaviour
{
    public static UnitManager instance { get; private set; }

    public static UnitEvents OnUnitDeath = new UnitEvents();

    public static UnitEvents OnUnitSpawned = new UnitEvents();

    public static UnitEvents OnUnitSelected = new UnitEvents();


    /// <summary>
    /// Current selected unit or current acting agent
    /// Hides and shows player unit abilities if Current unit is set on player's turn
    /// </summary>
    public Unit CurrentUnit
    {
        get { return cUnit; }
        set
        {
            if (TurnManager.instance.currentTeam == UnitType.Player)
            {
                if (cUnit)
                {
                    AbilityIcon.HideAbilities();
                    CurrentAbility = null;
                    if (cUnit.unitType == UnitType.Player)
                    {
                        lastPlayerUnit = cUnit;

                        cUnit.ResetBaseColor();

                        if (!cUnit.action && !cUnit.hasUsableSkill)
                        {
                            cUnit.SetBaseColor(Color.grey);
                        }
                    }
                }

                cUnit = value;

                OnUnitSelected.Invoke(cUnit);

                if (cUnit && cUnit.action)
                    forceStay = false;
                else
                    forceStay = true;

                if (value == null)
                    CurrentAbility = null;

                else if (cUnit.unitType == UnitType.Player)
                {
                    AbilityIcon.ShowAbilities(cUnit);
                    CurrentUnit.ShowAvailableMoves();
                    cUnit.SetBaseColor(Color.yellow);
                    cUnit.SetBaseEmmisiveValue(3);
                }
            }
            else
                cUnit = value;
        }
    }

    Unit lastPlayerUnit;

    Unit cUnit;

    public AbilityIcon button;

    // Used to forceSelect a 0 AP unit
    bool forceStay = false;

    /// <summary>
    /// Sets current selected ability
    /// Clears old data if previous ability wasn't null
    /// Displays suitable attack targets
    /// </summary>
    public AbilityBase CurrentAbility
    {
        get { return cAbility; }
        set
        {
            // Can only select ability while unit is not moving
            // Can change if needed
            if (cUnit && !cUnit.followingPath)
            {
                // Clears lists if cAbility wasn't null
                if (cAbility)
                {
                    if (cAbility.areaOfEffect == AbilityBase.AreaOfEffect.AreaOfEffect)
                        ResetAOEIndicator();

                    if (hoverTile == cUnit.CurrentTile)
                        cUnit.SetBasePrevColor();

                    ClearAllTiles();
                    minRange.Clear();
                    abilityTargets.Clear();
                    button.Reset();

                    if (CurrentObstacle)
                        CurrentObstacle = null;
                }

                cAbility = value;

                if (cAbility)
                {
                    GetAbilityTargets();
                    CurrentUnit.actionCost = cAbility.apCost;
                }
                else
                {
                    CurrentUnit.actionCost = 0;
                }
            }
        }
    }

    public AbilityObstacleObject CurrentObstacle
    {
        get
        {
            return cObstacle;
        }

        set
        {
            if (cObstacle && cObstacle.duration == 0)
            {
                Destroy(cObstacle.gameObject);
            }

            cObstacle = value;
        }
    }

    AbilityObstacleObject cObstacle;

    AbilityBase cAbility;

    HashSet<CellInfo> minRange = new HashSet<CellInfo>();

    HashSet<CellInfo> abilityRange = new HashSet<CellInfo>();

    HashSet<CellInfo> aoeIndicator = new HashSet<CellInfo>();

    HashSet<Unit> abilityTargets = new HashSet<Unit>();

    public CellInfo currentTile { get; private set; }

    public CellInfo hoverTile;

    public LayerMask targetLayers = 2048;//2048 because bit shenanagins - Layermask for Grid Layer

    private int pointerID = -1; // Used by event system to check if mouse is over UI element. NFI how it works but it seems to, sorry Huy 

    int playerUnitIndex = 0;

    bool pressed = false;

    public bool waitingForDialog = false;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;

        //apparently this is imporant to the UI check, as in the editor it needs to be -1, outside of editor it needs to be 0 because magic?
        //#if !UNITY_EDITOR
        //        pointerID = 0;
        //#endif
    }

    void Start()
    {
        CurrentUnit = null;

        CurrentAbility = null;

        lastPlayerUnit = null;

        TurnManager.instance.OnStartTurn.AddListener(TurnStart);
    }

    void TurnStart(UnitType e)
    {
        if (e == UnitType.Player)
        {
            if (lastPlayerUnit != null)
            {
                CurrentUnit = lastPlayerUnit;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // locks update if fungus is running
        if (waitingForDialog)
        {
            return;
        }

        bool inputDetected = false;

        if (TurnManager.instance.currentTeam == UnitType.Player && !(CurrentAbility && CurrentAbility.bouncing))
        {
            if ((Input.GetAxis("End Turn") != 0) && (!CurrentUnit || !CurrentUnit.followingPath))
            {
                inputDetected = true;

                if (!pressed)
                {
                    pressed = true;

                    ClearAllTiles();

                    if (CurrentUnit)
                        CurrentUnit.ResetUnit();
                    CurrentUnit = null;
                    TurnManager.instance.EndTurn();
                }
            }

            if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Tab)) && (!CurrentUnit || !CurrentUnit.followingPath))
            {
                if (CurrentUnit)
                {
                    CurrentUnit.ResetUnit();
                }
                CycleUnits();
            }

            // Checks for selected Unit
            if (CurrentUnit)
            {
#if UNITY_EDITOR
                // for testing things
                if(Input.GetKeyDown(KeyCode.P))
                {
                    CurrentUnit.stats.actionPoints = 90;
                }
#endif

                if ((Input.GetAxis("Deselect") != 0) && !CurrentUnit.followingPath)
                {
                    inputDetected = true;

                    if (!pressed)
                    {
                        pressed = true;
                        ClearAllTiles();
                        CurrentUnit.ResetUnit();
                        CurrentUnit = null;
                    }

                    return;
                }

                // TODO only highlight once and on change
                GridManager.instance.HighlightObstacles();

                // Deselects unit if no actions available
                if ((!CurrentUnit.action && CurrentUnit.path.Count <= 0 && !CurrentUnit.hasUsableSkill && !forceStay))// || (currentUnit.attackTargets.Count == 0 && !currentUnit.HasValidMoves && !currentUnit.followPath)))
                {
                    ClearAllTiles();
                    CurrentUnit.ResetUnit();
                    var unit = CurrentUnit;
                    CurrentUnit = null;

                    return;
                }

                // If no ability currently selected
                if (!CurrentAbility && hoverTile)
                {	
                    CurrentUnit.UpdatePath(hoverTile); // Updates path and draws
                }

                // Clears hover tile if mouse is over an ability icon
                if (EventSystem.current.IsPointerOverGameObject(pointerID))
                {
                    if (hoverTile)
                        hoverTile.SetDefault();

                    hoverTile = null;
                    CurrentUnit.potentialPath = null;
                }

                if (CurrentAbility && !CurrentUnit.followingPath)
                {
                    GridManager.instance.ClearSelections();

                    foreach (var tile in minRange)
                    {
                        if (tile != hoverTile)
                            tile.SetRed();
                    }

                    var mask = CurrentAbility.GetTargetMask(CurrentUnit);

                    // valid target tiles for ability
                    foreach (var p in abilityRange)
                    {
                        if (p.tile.Unobstructed)
                            p.SetYellow();

                        // Checks of all valid targets
                        if (p.unit && (p.unit.unitType & mask) != 0)
                        {
                            var unit = p.unit;

                            //// Reset to prevent getting multiple instances of colors in color stack
                            //ResetUnitAttackable(unit);

                            if (unit.healthBar)
                                unit.healthBar.bar.hitChance = CurrentAbility.CalculateHitChance(CurrentUnit, unit);

                            abilityTargets.Add(unit);

                            //unit.ShowOutline();
                            //unit.SetBaseColor(Color.magenta);
                            //unit.SetBaseEmmisiveValue(1.5f);
                        }
                    }

                    if (CurrentAbility.areaOfEffect == AbilityBase.AreaOfEffect.AreaOfEffect)
                    {
                        // resets previous aoeIndicator if one exists
                        ResetAOEIndicator();

                        // generate new aoeIndicator if there is a hover tile
                        if (hoverTile)
                        {
                            aoeIndicator = GridManager.RingRangeALL(hoverTile, CurrentAbility.aoeSize, 0);
                            foreach (var tile in aoeIndicator)
                            {
                                if (abilityRange.Contains(hoverTile))
                                    tile.SetAOEColor();
                                else
                                    tile.SetRed();

                                if (tile.unit && (tile.unit.unitType & mask) != 0)
                                {
                                    tile.unit.healthBar.bar.hitChance = CurrentAbility.CalculateHitChance(CurrentUnit, tile.unit);
                                    abilityTargets.Add(tile.unit);

                                    tile.unit.SetBaseColor(Color.magenta);
                                    tile.unit.SetBaseEmmisiveValue(1.5f);
                                }
                            }
                        }
                    }

                    // Allows player to click anywhere to selfcast 
                    if (CurrentAbility.range.max == 0)
                    {
                        HighlightHover(CurrentUnit.CurrentTile);
                    }

                    if (hoverTile)
                    {
                        hoverTile.SetAOEColor();

                        if (hoverTile.unit && abilityTargets.Contains(hoverTile.unit))
                        {
                            hoverTile.unit.SetBaseColor(Color.magenta);
                            hoverTile.unit.SetBaseEmmisiveValue(1.5f);
                        }
                    }

                    if (CurrentObstacle && hoverTile)
                    {
                        var dir = (hoverTile.transform.position - CurrentUnit.CurrentTile.transform.position).normalized;
                        CurrentObstacle.Preview(hoverTile.transform, dir);
                    }

                    if (Input.GetAxis("Fire1") != 0 && hoverTile)
                    {
                        CellInfo targetTile = hoverTile;

                        if ((CurrentAbility.requireLOS && CurrentAbility.CheckLOS(CurrentUnit, targetTile, true) || !CurrentAbility.requireLOS) && CurrentAbility.Execute(CurrentUnit, targetTile))
                        {
                            ClearAllTiles();
                            CurrentAbility = null;
                            if(hoverTile.unit)
                            {
                                hoverTile.unit.ResetBaseColor();
                            }
                        }
                    }
                }
                //Checks to make sure mouse over target is the tile they will move to, to prevent accidental moves.
                else if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject(pointerID))// && cell == hoverTile)
                {
                    if (hoverTile == currentTile)
                    {
                        // why is this empty again?
                    }
                    else if (CurrentUnit.potentialPath != null && hoverTile.tile == CurrentUnit.potentialPath.LastStep && CurrentUnit.potentialPath.TotalCost <= CurrentUnit.MoveRange)
                    {
                        CurrentUnit.followingPath = true;
                        ClearAllTiles(); // Clears available moves
                    }
                }

            }
        }

        HighlightHover();

        if (CurrentUnit == null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SelectObjectAtMouse();
            }
        }

        if (pressed && !inputDetected)
            pressed = false;
    }

    void CycleUnits()
    {
        int cycleBreak = 0;
        if (!CurrentUnit)
        {
            while (!CurrentUnit)
            {
                if (playerUnitIndex >= TurnManager.instance.playerUnits.Count)
                {
                    playerUnitIndex = 0;
                }

                if (TurnManager.instance.playerUnits[playerUnitIndex].action)
                {
                    CurrentUnit = TurnManager.instance.playerUnits[playerUnitIndex];
                }
                else
                {
                    playerUnitIndex++;
                    if (playerUnitIndex >= TurnManager.instance.playerUnits.Count)
                    {
                        playerUnitIndex = 0;
                    }
                }

                cycleBreak++;
                if (cycleBreak > 1)
                    break;
            }
        }
        else
        {
            var current = CurrentUnit;
            while (CurrentUnit == current)
            {
                if (playerUnitIndex >= TurnManager.instance.playerUnits.Count)
                {
                    playerUnitIndex = 0;
                }

                if (TurnManager.instance.playerUnits[playerUnitIndex].action && TurnManager.instance.playerUnits[playerUnitIndex] != current)
                {
                    CurrentUnit = TurnManager.instance.playerUnits[playerUnitIndex];
                }
                else
                {
                    playerUnitIndex++;
                    if (playerUnitIndex >= TurnManager.instance.playerUnits.Count)
                    {
                        playerUnitIndex = 0;
                        cycleBreak++;
                        if (cycleBreak > 1)
                            break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Highlights the current tile beneath cursor
    /// Pass in a tile to set hovertile
    /// </summary>
    /// <param name="setTile"></param>
    void HighlightHover(CellInfo setTile = null)
    {
        // Locks update of hoverTile if selected ability is selfcast
        if (!(CurrentAbility && CurrentAbility.range.max == 0))
        {
            var tile = GridManager.instance.GetGridAtMouse();

            if ((tile && tile.tile.Unobstructed) || (tile && CurrentAbility))// && tile != hoverTile)
            {
                if (hoverTile && (!CurrentAbility || CurrentAbility && CurrentAbility.areaOfEffect != AbilityBase.AreaOfEffect.AreaOfEffect))
                    hoverTile.SetDefault();

                if(hoverTile && CurrentAbility && hoverTile.unit)
                {
                    if(hoverTile != tile && abilityTargets.Contains(hoverTile.unit))
                    {
                        ResetUnitAttackable(hoverTile.unit);
                    }
                }
                
                hoverTile = tile;

                if (!CurrentAbility || CurrentAbility && CurrentAbility.areaOfEffect != AbilityBase.AreaOfEffect.AreaOfEffect)
                    hoverTile.SetColor(Color.blue);
            }
        }

        if(setTile)
        {
            hoverTile = setTile;
        }
    }

    Unit GetUnitAtMouse()
    {
        Unit unit = null;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        var layer = LayerMask.GetMask("Friendly", "Hostile");

        if (Physics.Raycast(ray, out hit, 10000, layer))
            unit = hit.transform.GetComponent<Unit>();

        return unit;
    }

    void SelectTile()
    {
        if (currentTile)
            currentTile.SetDefault();

        currentTile = GridManager.instance.GetGridAtMouse();

        if (currentTile)
        {
            currentTile.SetGreen();
            if (currentTile.unit)
            {
                CurrentUnit = currentTile.unit;
            }
        }
    }

    void SelectObjectAtMouse()
    {
        if (currentTile)
            currentTile.SetDefault();

        var unit = GridManager.instance.GetUnitAtMouse();

        if (unit)
        {
            currentTile = unit.CurrentTile;
            currentTile.SetGreen();
            CurrentUnit = unit;
        }
        else
        {
            var tile = GridManager.instance.GetGridAtMouse();
            if (tile)
            {
                currentTile = tile;
                currentTile.SetGreen();
                if (currentTile.unit)
                {
                    CurrentUnit = currentTile.unit;
                }
            }
        }
    }

    void ClearHitChance()
    {
        if (abilityTargets.Count > 0)
        {
            foreach (var unit in abilityTargets)
            {
                unit.healthBar.bar.hitChance = 0;
            }
        }
    }

    /// <summary>
    /// Sets up tiles to display
    /// </summary>
	void GetAbilityTargets()
    {
        ClearHitChance();

        //GridManager.GetAttackRangeAndUnits(CurrentUnit.CurrentTile, (int)CurrentAbility.range.max, out abilityTargets, out maxRange , CurrentAbility.GetTargetMask(CurrentUnit));
        abilityRange = GridManager.RingRangeALL(CurrentUnit.CurrentTile, (int)CurrentAbility.range.max, (int)CurrentAbility.range.min);
        if (CurrentAbility.range.min > 0)
            minRange = GridManager.RingRangeALL(CurrentUnit.CurrentTile, (int)CurrentAbility.range.min, 0);

        // trys to cast current able as obstacle
        // generates game object for preview if so
        var obstacle = (CurrentAbility as AbilityObstacle);

        if (obstacle)
        {
            var obj = obstacle.obstacleObject;

            var newObs = Instantiate(obj);

            newObs.SetActive(true);

            CurrentObstacle = newObs.GetComponent<AbilityObstacleObject>();
        }

        // Removes tiles out of line of sight from valid target tiles
        if (CurrentAbility.requireLOS)
            abilityRange.RemoveWhere(o => (!CurrentAbility.CheckLOS(CurrentUnit, o)));



        //foreach (var unit in abilityTargets)
        //{
        //    if (unit.healthBar)
        //        unit.healthBar.bar.hitChance = CurrentAbility.CalculateHitChance(CurrentUnit, unit);

        //    unit.ShowOutline();
        //    unit.SetBaseColor(Color.magenta);
        //    unit.SetBaseEmmisiveValue(1.5f);
        //}

        GridManager.instance.ClearSelections();
    }

    /// <summary>
    /// Resets colour of all tiles
    /// Also clears hit chance text above units
    /// </summary>
    void ClearAllTiles()
    {
        foreach (var tile in minRange)
        {
            tile.SetDefault();
        }

        foreach (var tile in abilityRange)
        {
            tile.SetDefault();
        }

        foreach (var tile in aoeIndicator)
        {
            tile.SetDefault();
        }

        ClearHitChance();

        GridManager.instance.ClearSelections();
    }

    /// <summary>
    /// Blocks player input from doing anything
    /// Used by fungus
    /// </summary>
    public void EnableInputLock()
    {
        waitingForDialog = true;
    }

    /// <summary>
    /// Blocks player input from doing anything
    /// Used by fungus
    /// </summary>
    public void DisableInputLock()
    {
        waitingForDialog = false;
    }

    public void EnableDisableInputLock()
    {
        if (waitingForDialog)
        {
            waitingForDialog = false;
        }
        else
        {
            waitingForDialog = true;
        }
    }

    void ResetAOEIndicator()
    {
        if (aoeIndicator.Count > 0)
        {
            var mask = CurrentAbility.GetTargetMask(CurrentUnit);

            foreach (var tile in aoeIndicator)
            {
                // Set color to default if not being set by ability range
                if (!abilityRange.Contains(tile))
                    tile.SetDefault();

                if (tile.unit && (tile.unit.unitType & mask) != 0)
                {
                    ResetUnitAttackable(tile.unit);
                }
            }
        }
    }

    void ResetUnitAttackable(Unit unit)
    {
        if (unit.healthBar)
            unit.healthBar.bar.hitChance = 0;

        unit.HideOutline();
        unit.SetBasePrevColor();
        unit.SetBasePrevEmValue();
    }
}
