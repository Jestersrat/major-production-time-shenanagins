﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyAbility : AgentAction
{
    public AllyAbility(Agent agent, AbilityBase ability)
    {
        this.agent = agent;
        this.ability = ability;
    }

    public override void Enter(Unit unit)
    {
        if (!unit)
            return;

        agent.executingAction = true;

        var distance = GridManager.Distance(agent.CurrentTile.gridPos, agent.currentTarget.CurrentTile.gridPos);

        // Generates path to a location where ability can be used if target is inside min range
        // TODO: update evaluate to avoid these situations
        if (distance < ability.range.min && ability.range.min != 0)
        {
            var validAttackFromGrids = GridManager.RingRangeALL(agent.currentTarget.CurrentTile, (int)ability.range.max + 2, (int)ability.range.min);

            var escapeSearch = GridManager.RingRangeALL(agent.CurrentTile, agent.MoveRange, 0);

            var validLocations = GridManager.Intersecting(validAttackFromGrids, escapeSearch);

            CellInfo moveTarget = null;

            float bestScore = 0;

            var currentLocation = agent.CurrentTile.transform.position;

            currentLocation.y = 0;

            foreach (var location in validLocations)
            {
                if (location.isEmpty && location.tile.Unobstructed)
                {
                    var loc = location.transform.position;
                    loc.y = 0;
                    float score = GridManager.Distance(currentLocation, loc);
                    if (!moveTarget || score < bestScore)
                    {
                        bestScore = score;
                        moveTarget = location;
                    }
                }
            }

            if (moveTarget == null)
            {
                escapeSearch = GridManager.RingRangeALL(agent.CurrentTile, agent.stats.moveSpeed, 0);

                validLocations = GridManager.Intersecting(validAttackFromGrids, escapeSearch);

                foreach (var location in validLocations)
                {
                    if (location.isEmpty && location.tile.Unobstructed)
                    {
                        var loc = location.transform.position;
                        loc.y = 0;
                        float score = GridManager.Distance(currentLocation, loc);
                        if (!moveTarget || score < bestScore)
                        {
                            bestScore = score;
                            moveTarget = location;
                        }
                    }
                }
            }

            if (moveTarget == null)
            {
                Debug.Log(agent.name + " " + ability.name + " still has no movetarget");
                GeneratePathToTarget();
                return;
            }

			GeneratePathToDestination(moveTarget, unit);

			//if (agent.path.Count <= 0)
			//	unit.GeneratePathableRegion();
		}
		else if (distance <= ability.range.max && ability.CanUse(agent, unit))
		{
			executed = ability.Execute(agent, unit.CurrentTile);
			Exit(unit);
		}
		else
        {
            GeneratePathToTarget();
        }
    }

    public override float Evaluate(Unit unit)
    {
        if (!unit || !agent || !GridManager.instance)
        {
            return Mathf.NegativeInfinity;
        }

        // Makes sure agent won't try to cast ability on itself if it can't
        if (unit == agent && ability.range.min > 0)
            return -Mathf.Infinity;

        float score = 0;

        // Checks to see if target currently has a limited pathable region
        if (unit.pathableRegion.Count > 0)
		{
			// otherwise checks if agent is within currently pathable region
			if (!unit.pathableRegion.Contains(agent.CurrentTile))
			{
				// reduces score accordingly if not
				score -= 5;
			}
		}

		// Return zero if can't use ability
		if (TurnManager.instance.currentTeam == agent.unitType && !ability.CanUse(agent))
            return score - GridManager.Distance(agent.CurrentTile.gridPos, unit.CurrentTile.gridPos);

        //var heal = Mathf.Abs(ability.Damage(agent));

        score += unit.HealthPercent * 2f;

        costAsDistance = Mathf.Ceil(agent.MPA * ability.apCost);

        var distance = (GridManager.Distance(agent.CurrentTile.gridPos, unit.CurrentTile.gridPos));

        // Guess if target is within range
        // Checks if agent can reach target and have enough ap to use skill
        if (distance + costAsDistance <= agent.MoveRange + ability.range.max)
        {
            // Check if target is outside unit's attack range
            if (distance > ability.range.max)
            {
                Path<Tile> path = null;

                // Checks to see if target currently has a limited pathable region
                if (unit.pathableRegion.Count <= 0)
                {
                    // performs normal pathing check if none	
                    if (!agent.isHuge)
                        path = Path<Tile>.FindPathForAI(unit.CurrentTile.tile, agent.CurrentTile.tile, Tile.distance, Tile.estimate, agent);
                    else
                        path = Path<Tile>.FindBrutePath(unit.CurrentTile.tile, agent.CurrentTile.tile, Tile.distance, Tile.estimate);

                    if (path == null)
                    {
                        unit.GeneratePathableRegion();

                        score -= 10;
                    }
                }

                // Checks real distance from target
                if (path != null && path.TotalCost - ability.range.max > agent.MoveRange + costAsDistance)
                {
                    float penalty = ((float)path.TotalCost - ability.range.max + costAsDistance) / (agent.MoveRange);
                    score -= penalty;
                }
                else
                {
                    float penalty = (distance - ability.range.max + costAsDistance) / (agent.MoveRange);
                    score -= penalty;
                }
            }
            else if (ability.requireLOS && !ability.CheckLOS(agent, unit.CurrentTile)) // skill is in range so check los if required
            {
                return score - 1.5f;
            }
        }
        else
        {
            // reduces score based on how far away target is
            float penalty = (distance - ability.range.max + costAsDistance) / agent.MoveRange;
            score -= penalty;
        }

        // TODO reduce score further if low health?
        // Only need to include if ai will have healing abilities

        // Add slight random variance
        var randomness = UnityEngine.Random.Range(90f, 110f);

        randomness /= 100;

        return score;
    }

    public override void Exit(Unit unit)
    {
        agent.executingAction = false;
        agent.currentAction = null;
    }

    public override void UpdateAction(Unit unit)
    {
        if (unit)
        {
            var distance = GridManager.Distance(agent.CurrentTile.gridPos, agent.currentTarget.CurrentTile.gridPos);

            if (!agent.FollowPath())
            {
                distance = GridManager.Distance(agent.CurrentTile.gridPos, agent.currentTarget.CurrentTile.gridPos);
                if (distance <= ability.range.max && distance > ability.range.min)
                {
                    ability.Execute(agent, unit.CurrentTile);
                    Exit(unit);
                }
                else
                    Exit(unit);
            }
            // stops early to attack if tile is empty and within attack range
            else if (distance <= ability.range.max && distance > ability.range.min)
            {
                ability.Execute(agent, unit.CurrentTile);
                Exit(unit);
            }
        }
        else
            Exit(unit);
    }
}
