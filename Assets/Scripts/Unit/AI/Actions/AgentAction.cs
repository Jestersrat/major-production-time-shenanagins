﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AgentAction
{
	public Agent agent;

    protected bool executed = false;

	public AbilityBase ability;

    protected float costAsDistance;

    public static HashSet<CellInfo> moveRange = new HashSet<CellInfo>();
		
	public abstract float Evaluate(Unit unit);
	public abstract void UpdateAction(Unit unit);
	public abstract void Enter(Unit unit);
	public abstract void Exit(Unit unit);

	protected void GeneratePathToTarget()
	{
		agent.currentTarget.CurrentTile.tile.DestTile = true;

		Path<Tile> path = null;

		int it = 0;        

		while (path == null)
		{

			var validStops = GridManager.RingRangeALL(agent.currentTarget.CurrentTile, ++it);

			// might rework this in future
			// Sets all adjacent tiles to target unit's tile as a valid stopping point
			foreach (var stop in validStops)
			{
				if (stop.tile.Unobstructed)
					stop.tile.validStop = true;
			}

			path = Path<Tile>.FindPathForAI(agent.CurrentTile.tile, agent.currentTarget.CurrentTile.tile, Tile.distance, Tile.estimate, agent);

			// discard final node occupied by target unit
			if (it > 1 && path != null)
			{
				path = path.PreviousSteps;
				--it;
			}

			// toggling bool in tile to help with ai pathing
			foreach (var stop in validStops)
				stop.tile.validStop = false;

			// may need to increase value in future
			if (it > 2)
				break;
		}
		
		if (path == null)
		{
            float dist = Mathf.Infinity;

            CellInfo moveTarget = null;

			// set target position
            var targetPos = agent.currentTarget.CurrentTile.transform.position;

            moveRange.Clear();

			// generate list of agent's reachable tiles
            moveRange = GridManager.RingRangeUnobstructed(agent.currentTarget.CurrentTile, agent.MoveRange, 0);

			// loops through each and checks distance from target tile
            foreach (var tile in moveRange)
            {
                float distToTile = Vector3.Distance(tile.transform.position, targetPos);

                if (tile.isEmpty && tile.tile.Unobstructed || tile.unit == agent)
                {
                    if (distToTile < dist)
                    {
                        dist = distToTile;
                        moveTarget = tile;
                    }
                }
            }

			// generate path tile with shortest distance found
            GeneratePathToDestination(moveTarget, agent.currentTarget);
			
   //         if (agent.path.Count <= 0)
			//{
			//	// generates pathable region if no path
			//	agent.currentTarget.GeneratePathableRegion();
			//}                

            agent.currentTarget.CurrentTile.tile.DestTile = false;

            return;
		}

        // remove nodes from end of path if too far away
		while (path.TotalCost > agent.MoveRange)
		{
			path = path.PreviousSteps;
		}

        // Apply ap cost of path
        agent.stats.actionPoints -= agent.GetMoveCost((int)path.TotalCost);

        // Put list in correct order
        foreach (var node in path)
		{
			agent.path.AddFirst(node.info);
		}

		while (!agent.path.Last.Value.tile.Empty && path.PreviousSteps != null)
		{
            if (agent.path.Last.Value.tile.info.unit == agent)
                break;

            if (path.PreviousSteps != null)
                agent.path.RemoveLast();
            else
                break;
		}

		agent.currentTarget.CurrentTile.tile.DestTile = false;
	}

    protected void GeneratePathToDestination(CellInfo dest, Unit target = null)
    {
        Path<Tile> path = null;

        // Exits early if destination tile is null
        if (!dest)
            return;

        path = Path<Tile>.FindPath(agent.CurrentTile.tile, dest.tile, Tile.distance, Tile.estimate);

        // skip current unit's turn if no path is found
        // only cause of no path is due to an error
        if (path == null)
        {
			if (target)
				target.GeneratePathableRegion();

			Debug.LogError(agent.name + " couldn't generate path to updating? " + agent.currentTarget.name);

			return;
        }

        while (path.TotalCost > agent.MoveRange)
        {
            path = path.PreviousSteps;
        }

        agent.stats.actionPoints -= agent.GetMoveCost((int)path.TotalCost);

        // Puting path into correct order
        foreach (var node in path)
        {
            agent.path.AddFirst(node.info);
        }

        // making sure last tile in path is empty
        while (!agent.path.Last.Value.tile.Empty && path.PreviousSteps != null)
        {
            if (agent.path.Last.Value.tile.info.unit == agent)
                break;

            if (path.PreviousSteps != null)
                agent.path.RemoveLast();
            else
                break;
        }
    }

    protected void GenerateOversizedPath()
    {
        Path<Tile> path = Path<Tile>.FindBrutePath(agent.CurrentTile.tile, agent.currentTarget.CurrentTile.tile, Tile.distance, Tile.estimate);

        if (path == null)
        {
            agent.stats.actionPoints = 0;

            return;
        }

        // remove nodes from end of path if too far away
        while (path.TotalCost > agent.MoveRange)
        {
            path = path.PreviousSteps;
        }

        // Apply ap cost of path
        agent.stats.actionPoints -= agent.GetMoveCost((int)path.TotalCost);

        // Put list in correct order
        foreach (var node in path)
        {
            agent.path.AddFirst(node.info);
        }

        if(agent.path.Last.Value.tile.info.unit == agent.currentTarget)
        {
            agent.path.RemoveLast();
        }
    }
}
