﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UAIManager : MonoBehaviour
{
	public static UAIManager instance { get; private set; }

	public Agent bestAgent;

	public Agent currentAgent;

    float delay = 0;

    public LinkedList<Agent> evaluationOrder = new LinkedList<Agent>();

    int evaluationIndex = 0;
    
    public bool WaitingOnCurrentAgent
    {
        get
        {
            if(currentAgent && currentAgent.currentAction != null)
            {
                return true;
            }

            return false;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
	
	// Use this for initialization
	void Start ()
	{
        currentAgent = null;
		TurnManager.instance.OnEndTurn.AddListener(OnTurnChange);
	}
	
	// Update is called once per frame
	void Update ()
	{
        EvaluateNextInLine();
                
        if (!(delay > 0) || (currentAgent && currentAgent.executingAction))
        {
            if (TurnManager.instance.currentTeam == UnitType.Hostile)
            {
                foreach(var agent in TurnManager.instance.enemyUnits)
                {
                    if(!agent.active)
                    {
                        if (agent.patrol.LeadPatrol())
                        {
                            return;
                        }
                    }
                }

                foreach (var agent in TurnManager.instance.enemyUnits)
                {
                    if (!agent.active)
                    {
                        if (agent.patrol.FollowPatrol())
                        {
                            return;
                        }
                    }
                }


                if (currentAgent == null)
                {
                    GetBestAgent();
                    if (bestAgent)
                        currentAgent = bestAgent;
                }

                // Prevents anything from happening when there are projectiles flying
                if (currentAgent && currentAgent.currentAction != null && currentAgent.currentAction.ability.Executing)
                    return;

                if (currentAgent && currentAgent.currentAction != null)
                {
                    if (!currentAgent.ExecuteAction())
                    {
                        // place holder
                        if (!currentAgent.hasUsableSkill)
                        {
                            currentAgent.stats.actionPoints = 0;
                        }

                        delay = .3f;

                        UnitManager.instance.CurrentUnit = null;
                        currentAgent = null;
                        GetBestAgent();

                    }
                    else if (!currentAgent.HasValidMoves && currentAgent.path.Count <= 0)
                    {
                        UnitManager.instance.CurrentUnit = null;
                        currentAgent = null;
                        GetBestAgent();
                    }
                }

                if (!StillMovesAvailable() && (currentAgent == null))// || actionsPerformed == TurnManager.instance.enemyUnits.Count)
                {
                    EndAITurn();
                }
            }
        }
        else
            delay -= Time.deltaTime;

        
    }

    void EvaluateNextInLine()
    {
        if (TurnManager.instance.enemyUnits.Count > 0)
        {
            if (evaluationIndex >= TurnManager.instance.enemyUnits.Count)
                evaluationIndex = 0;

            while (TurnManager.instance.enemyUnits[evaluationIndex].isDead)
            {
                TurnManager.instance.enemyUnits.RemoveAt(evaluationIndex);

                if (evaluationIndex >= TurnManager.instance.enemyUnits.Count)
                    evaluationIndex = 0;
            }

            if (evaluationIndex < TurnManager.instance.enemyUnits.Count)
            {
                if (!TurnManager.instance.enemyUnits[evaluationIndex].executingAction)
                    TurnManager.instance.enemyUnits[evaluationIndex].SetBestAction();

                evaluationIndex++;
            }
        }
    }

    void OnTurnChange(UnitType turn)
    {
        if (turn == UnitType.Hostile)
        {
            ResetUAI();
        }
    }

    bool StillMovesAvailable()
	{
		foreach(var unit in TurnManager.instance.enemyUnits)
		{
			Agent agent = unit as Agent;
			if (agent.action && agent.active)
				return true;
		}

		return false;
	}

	void ResetUAI()
	{
		foreach(var unit in TurnManager.instance.enemyUnits)
		{
			unit.ResetUnit();
			unit.executingAction = false;
		}

		bestAgent = null;
		currentAgent = null;
	}

	void EndAITurn()
	{
		TurnManager.instance.EndTurn();
	}

	bool GetBestAgent()
	{
		Agent agent = null;
		float score = 0;

		foreach(var baseUnit in TurnManager.instance.enemyUnits)
		{
			var unit = baseUnit as Agent;
            if ((unit.active && unit.action && unit.currentAction != null) && (unit.actionValue > score || agent == null))
			{
				agent = unit;
				score = unit.actionValue;
			}
		}

		bestAgent = agent;

		if (agent)
			UnitManager.instance.CurrentUnit = agent;

		return agent;
	}

    public void AddDelay(float amount)
    {
        delay += amount;
    }

}
