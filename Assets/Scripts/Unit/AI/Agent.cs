﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : Unit
{
    public AgentAction currentAction;

    [Header("Agent Options")]
    public Unit currentTarget;

	public float actionValue;

	public bool executingAction;

	public int alertRadius = 10;

	public List<AgentAction> actions = new List<AgentAction>();

    public List<AgentAction> allyActions = new List<AgentAction>();

    public bool startActivated = false;

    public bool eventActivateOnly = false;

    public Patrol patrol = new Patrol();

    // Determines whether AI can perform actions
    /// <summary>
    /// Units will not alert allies if activated using setter
    /// </summary>
    public bool Activated
	{
		get
		{
			if (!active && !eventActivateOnly)
			{
				// will need to change this in future
				HashSet<Unit> enemies;
				GridManager.GetUnitsInRange(CurrentTile, stats.visionRange, out enemies, UnitType.Player);

				if (enemies.Count > 0)
				{
					RaycastHit hit;

					LayerMask mask = ~LayerMask.GetMask("Grid", "Ignore Raycast", "UI", "Build");

                    Vector3 head = GetHeadPos();

                    foreach (var enemy in enemies)
					{
						var dir = enemy.transform.position + Vector3.up * 1.8f - head;

						dir.Normalize();

						if (Physics.Raycast(head, dir, out hit, 100, mask))
						{
							//Debug.DrawLine(head, hit.point, Color.green, 10);

       //                     //var dir2 = transform.position - hit.transform.position;

       //                     //Debug.Log(hit.transform.name);

							//Debug.DrawLine(hit.transform.position, transform.position, Color.red, 10);

							if (hit.transform.gameObject.layer == (int)Layer_Index.Friendly)
								active = true;
						}
					}

					if (active)
					{
						HashSet<Unit> friends;
						GridManager.GetUnitsInRange(CurrentTile, alertRadius, out friends, UnitType.Hostile);
						foreach (var unit in friends)
						{
							var agent = unit as Agent;
                            if (!agent.eventActivateOnly)
                                agent.Activated = true;
						}
					}
				}
			}
			return active;
		}
		set
		{
			active = value;
		}
	}

	public bool active;

	// Use this for initialization
	void Start ()
	{
        InitialiseAgent();
        patrol.SetUp(this);
        CopySkillTree();
        if (level.unitLevel > 0)
        {
            ForceLevelUp(level.unitLevel);
        }
	}

    public void InitialiseAgent()
    {
        Initialise();

        foreach (var ability in abilities)
        {
            if (ability.targets != AbilityBase.Targets.Ally)
                actions.Add(new ExecuteAbility(this, ability));
            else
                allyActions.Add(new AllyAbility(this, ability));
        }

        if (CurrentTile && CurrentTile.unit != this)
            CurrentTile.unit = this;

        //if (unitType == UnitType.Player)
        unitType = UnitType.Hostile;

        //actions = GetComponents<Action>();
        executingAction = false;
        
        Activated = startActivated;

        TurnManager.instance.OnStartTurn.AddListener(StartTurn);
    }
	
	// Update is called once per frame
	void Update ()
	{
		if (!isDead)
		{			
			// follows path if set to followpath
			if (followingPath)
			{
				FollowPath();
			}

			// To be removed in future
			if (unitType == UnitType.Ally || unitType == UnitType.Player)
				gameObject.layer = (int)Layer_Index.Friendly;
			else
				gameObject.layer = (int)Layer_Index.Hostile;
            
//			UpdateColor();
		}

		//if (!executingAction && activated && TurnManager.instance.currentTeam == UnitType.Hostile && !UAIManager.instance.WaitingOnCurrentAgent)
		//{
  //          //Causing frequent GC, needs to be called outside of alpha
		//	if (currentTarget && currentTarget.isDead)
		//	{
		//		currentTarget = null;
		//		currentAction = null;
		//	}
  //          AgentAction best = GetBestAction();

  //          if (best != currentAction)
  //          {
  //              currentAction = best;
  //          }
  //      }
	}

    void StartTurn(UnitType e)
    {
        if(e == unitType)
        {
            if (Activated)
            {
                HashSet<Unit> friends;
                GridManager.GetUnitsInRange(CurrentTile, (int)(alertRadius * .5f), out friends, UnitType.Hostile);
                foreach (var unit in friends)
                {
                    var agent = unit as Agent;
                    agent.Activated = true;
                }
            }
            
        }

    }

    public void SetBestAction()
    {
        AgentAction best = GetBestAction();

        if (best != currentAction)
        {
            currentAction = best;
        }
    }

	public AgentAction GetBestAction()
	{
		AgentAction action = null;
		float bestValue = Mathf.NegativeInfinity;
        float value;

        foreach (var a in actions)
		{
            // make static valid target list and remove dead units ect
            foreach (var target in TurnManager.instance.playerUnits)
            {
                value = a.Evaluate(target);
                if (action == null || value > bestValue)
                {
					action = a;
					bestValue = value;
					actionValue = bestValue;
                    currentTarget = target;
				}
				//Debug.Log(target.name + value);
			}
        }

        foreach (var a in actions)
        {
            // make static valid target list and remove dead units ect
            foreach (var target in TurnManager.instance.allyUnits)
            {
                value = a.Evaluate(target);
                if (action == null || value > bestValue)
                {
					action = a;
					bestValue = value;
					actionValue = bestValue;
					currentTarget = target;
				}
			}
        }

        foreach (var a in allyActions)
        {
            // make static valid target list and remove dead units ect
            foreach (var target in TurnManager.instance.enemyUnits)
            {
                if (!target.isDead)
                {
                    value = a.Evaluate(target);
                    if (action == null || value > bestValue)
                    {
						action = a;
						bestValue = value;
						actionValue = bestValue;
						currentTarget = target;
					}
                }
            }
        }

        return action;
	}

	public bool CheckSkillCooldown()
	{
		foreach (var ability in abilities)
			if (ability.CanUse(this))
				return true;

		return false;
	}

	// returns true while running
	public bool ExecuteAction()
	{
        if (executingAction)
            currentAction.UpdateAction(currentTarget);
        else
        {
            currentAction.Enter(currentTarget);
        }

		return executingAction;
	}
}
