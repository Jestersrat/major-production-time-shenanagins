﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConsiderTarget
{
	Self,
	Target
}

[CreateAssetMenu(fileName = "Consideration", menuName = "New Consideration", order = 1)]
public class UAIConsiderations : ScriptableObject
{
	public string consideration;
	public ConsiderTarget target;
	public AnimationCurve curve;
	
}
