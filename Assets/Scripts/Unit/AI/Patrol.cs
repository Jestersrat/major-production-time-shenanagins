﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Patrol
{
    [Tooltip("Will attempt to follow squad leader if one is set")]
    public Agent squadLeader;

    public List<Transform> patrolPoints = new List<Transform>();

    public List<CellInfo> patrolTiles = new List<CellInfo>();

    int index = 0;

    int loopBreak = 0;

    [Tooltip("Will pick random points in patrol points to go to if true")]
    public bool randomOrder = false;

    Agent self;

    /// <summary>
    /// Removes all empty points and populates patrolTiles based on patrolPoints
    /// </summary>
    public void SetUp(Agent self)
    {
        this.self = self;

        // Removes all null points
        patrolPoints.RemoveAll(o => !o);

        foreach (var t in patrolPoints)
        {
            patrolTiles.Add(GridManager.GetClosestEmpty(t.position));
        }

        if (randomOrder)
            index = Random.Range(0, patrolTiles.Count);

        if(randomOrder && patrolPoints.Count == 1)
        {
            Debug.LogError("random order selected with only 1 patrolPoint in " + self.name);
        }

    }

    public bool FollowPatrol()
    {
        if (squadLeader && !squadLeader.isDead)
        {
            if (self.path.Count <= 0 && self.action)
            {
                var dest = GridManager.GetClosestEmpty(squadLeader.transform.position);

                GeneratePath(dest);
            }

            if(self.path.Count > 0)
            {
                if(!self.path.Last.Value.fog.revealed)
                {

                    while(self.path.Count > 0 && !self.path.Last.Value.isEmpty)
                    {
                        self.path.RemoveLast();
                    }

                    //if (self.path.Count <= 0)
                    //    self.stats.actionPoints = 0;

                    if (self.path.Count > 0)
                    {
                        self.transform.position = self.path.Last.Value.transform.position;
                        self.SetCurrentTile(self.path.Last.Value);
                        self.transform.forward = self.path.Last.Value.transform.forward;
                        self.path.Clear();
                        self.stats.actionPoints = 0;
                    }

                    return true;
                }

                return self.FollowPath();
            }
        }

        return false;
    }

    public bool LeadPatrol()
    {
        if (patrolTiles.Count > 0)
        {
            if (self.path.Count <= 0 && self.action)
            {
                if (GridManager.Distance(self.CurrentTile.gridPos, patrolTiles[index].gridPos) <= 3)
                {
                    if (randomOrder)
                    {
                        int rand = Random.Range(0, patrolTiles.Count);

                        while (index == rand && patrolPoints.Count > 1)
                        {
                            rand = Random.Range(0, patrolTiles.Count);
                        }

                        index = rand;

                        var dest = GridManager.GetClosestEmpty(squadLeader.transform.position);

                        GeneratePath(dest);
                    }
                    else
                    {
                        index++;

                        if (index >= patrolTiles.Count)
                            index = 0;

                        var dest = GridManager.GetClosestEmpty(patrolTiles[index].transform.position);

                        GeneratePath(dest);
                    }
                }
                else
                {
                    var dest = GridManager.GetClosestEmpty(patrolTiles[index].transform.position);

                    GeneratePath(dest);
                }
            }

            if (self.path.Count > 0)
            {
                self.FollowPath();
                return true;
            }
        }

         return false;
    }

    void GeneratePath(CellInfo dest)
    {
        loopBreak++;

        var path = Path<Tile>.FindPath(self.CurrentTile.tile, dest.tile, Tile.distance, Tile.estimate);

        while (path != null && (path.TotalCost > self.MoveRange || !path.LastStep.Empty))
        {
            path = path.PreviousSteps;
        }

        if(path != null)
        {
            self.stats.actionPoints -= self.GetMoveCost((int)path.TotalCost);

            foreach(var tile in path)
            {
                self.path.AddFirst(tile.info);
            }

            loopBreak = 0;
        }
        else
        {
            // try to path to next patrol point
            if (++index >= patrolTiles.Count)
                index = 0;
        }

        // no paths can be found set ap to 0;
        if (loopBreak > 7)
        {
            self.stats.actionPoints = 0;
            Debug.LogError(self.name + "is unable to path to any patrol points");
        }
    }
}
