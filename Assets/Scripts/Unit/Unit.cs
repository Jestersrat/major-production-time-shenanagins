﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[System.Flags]public enum UnitType
{
	None,
	Player,
	Hostile = 2,
	Ally = 4,
}

[System.Serializable]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerUnitTooltip))]
[RequireComponent(typeof(FloatingHPInitializer))]
[RequireComponent(typeof(AudioSource))]
public class Unit : MonoBehaviour
{
    #region Unit Stats
    [Header("Unit Stats")]
    [SerializeField]
    public string unitName;

    public UnitLevel level = new UnitLevel();

    public Stats baseStats = new Stats();

    //[HideInInspector()]
    public Stats stats;

    public Stats lvlStats = new Stats();
    public Stats staticStatsMod = new Stats();
    public Stats multStatsMod = new Stats();

    public List<StatusEffects> statuses = new List<StatusEffects>();

    /// <summary>
    /// Returns true if unit has actionPoints and is not dead
    /// </summary>
    public bool action { get { return stats.actionPoints > 0 && !isDead; } }

    public int remainingActionPoints { get; set; }
    public int minAP = 0;
    [HideInInspector]
    // Used to update ap icons
    public int actionCost = 0;

    public UnitType unitType;

    #endregion

    #region Unit Abilities
    public bool hasUsableSkill
    {
        get
        {
            foreach (var ability in abilities)
            {
                if (ability.CanUse(this))
                {
                    return true;
                }
            }

            return false;
        }
    }

    [Tooltip("List of abilities Unit can use")]

    [Header("Unit Abilities")]
    public List<AbilityBase> abilityList;

    public List<AbilityBase> abilities = new List<AbilityBase>();

    public SkillTree skillTree;

    #endregion

    CharacterController controller;

    [Header("Objective Details")]
    public List<int> objectiveID = new List<int>();

    // to be fixed in future
    public bool isDead = false;// { get; protected set; }

    public FloatingHPInitializer healthBar;
    [Header("Unit height and grid details")]

    // Pathing / grid
    #region Grid & Pathfinding
    [Tooltip("Where to shoot raycasts from")]
    public float headPos = 2f;

    public Transform headTransform;

    public float escapeCost = 3;


    public bool beenRevealed;

    public CellInfo CurrentTile
    {
        get
        {
            return cTile;
        }

        protected set
        {
            if (cTile)
            {
                if (cTile.unit = this)
                    cTile.unit = null;

                if (isHuge)
                    ClearOversizeTiles();
            }

            cTile = value;

            if (cTile)
            {
                cTile.unit = this;
                if (isHuge)
                    SetOversizeTiles();

                if (UnitManager.instance.CurrentUnit == this)
                    ShowAvailableMoves();
            }
        }
    }

    // stores tile value for CurrentTile
    [SerializeField]
    CellInfo cTile;

    public HashSet<CellInfo> pathableRegion = new HashSet<CellInfo>();

    public CellInfo DestTile { get; set; }

    public Path<Tile> potentialPath;

    public LinkedList<CellInfo> path = new LinkedList<CellInfo>();

    public bool followingPath = false;

    #endregion

    #region Renderer stuff
    [Header("Colour stuff")]

    //Renderer rend;

    List<Renderer> renderers = new List<Renderer>();
    Material matSetter;
    static readonly int outline_ID = Shader.PropertyToID("_OutlineOn");

    static readonly int baseColor_ID = Shader.PropertyToID("_Emmisive_Color");

    static readonly int baseEmmisiveValue_ID = Shader.PropertyToID("_Emmisive_Value");

    public Renderer baseRenderer;

    Stack<Color> baseColorStack = new Stack<Color>();

    Stack<float> baseEmmisiveStack = new Stack<float>();

    public Sprite unitIcon;
    #endregion

    #region Audio Stuff

    public AudioSource audioSource;

    public AudioClip deathSound;

    #endregion

    #region CombatRecords
    public float damageDealt=0;
    public float kills=0;
    public float healthHealed=0;
    public float damageTaken = 0;
    public int daysInjured = 0;
    public string unitClass = "";
    #endregion

    public Animator[] animators;
    // a set of numbers use to weighted randomize certain animations
    public float[] blends = {0f, 0f, 0f, 0.2f, 0.5f, 0.8f, 1f, 1f, 1f};
    //UI element containing player unit Info
    public PlayerUnitUI unitUI = null;
    public int unitSaveID;

    [Header("Oversized Options")]
    public bool isHuge = false;
    public int hugeFactor = 0;
    public float shakeAmount = 1;
    public float shakeDuration = 0.5f;
    public float moveSpeedMod = 0.1f;
    public float stompDamage = 0;

    public HashSet<CellInfo> overSizeTileList = new HashSet<CellInfo>();

    public Vector3 forwardTarget;
        
    // Use this for initialization
    void Start()
    {
		Initialise();
	}

	protected void Initialise()
	{
        audioSource = GetComponent<AudioSource>();
        healthBar = GetComponent<FloatingHPInitializer>();

        animators = transform.GetComponentsInChildren<Animator>();

        if (GridManager.instance)
        {
            // Render component to change colour of unit
            foreach (var rend in GetComponentsInChildren<Renderer>())
            {
                if(rend.sharedMaterial.HasProperty(outline_ID))
                {
                    renderers.Add(rend);
                }
            }

            if (renderers.Count > 0)
            {
                matSetter = new Material(renderers[0].sharedMaterial);
            }



            // Ensures current tile and dest tile are null
            DestTile = null;

            // Sets current tile accord to current pos
            // May need to edit getgrid from world pos in future

            CurrentTile = GridManager.GetClosestEmpty(transform.position);

            foreach(Transform child in transform)
            {
                if(child.tag == "Head")
                {
                    headTransform = child;
                    headPos = headTransform.localPosition.magnitude;
                }
            }

            //yOffset = new Vector3(0, 0, 0);

            // Snapping unit to centre of current tile
            //var pos = CurrentTile.transform.position;// + yOffset;
            transform.position = CurrentTile.transform.position;

            // setting path to null;
            potentialPath = null;
            followingPath = false;

            // setting up reference to character controller
            controller = GetComponent<CharacterController>();
            
            TurnManager.instance.OnEndTurn.AddListener(OnEndTurn);
            TurnManager.instance.OnStartTurn.AddListener(OnStartTurn);

            // Making sure currentTile is pointing to unit
            if (CurrentTile && CurrentTile.unit != this)
                CurrentTile.unit = this;

            // Setting stat mods to default value
            ResetStatMods();
            
            abilities.Clear();
            // Loops through each ability in ability list and adds a copy into abilities
            foreach (var ability in abilityList)
            {
                if (ability)
                {
                    var a = Instantiate(ability);
                    a.name = ability.name;
                    abilities.Add(a);
                    a.Initialise();
                }
            }

            if (statuses.Count > 0)
            {
                List<StatusEffects> newList = new List<StatusEffects>();

                foreach (var statusEffect in statuses)
                {
                    var sCopy = Instantiate(statusEffect);
                    sCopy.name = statusEffect.name;
                    newList.Add(sCopy);
                    sCopy.Initialise(this, this, null, false);
                }

                statuses = newList;
            }

            stats = baseStats;

            // Sets layer of Unit and all children
            UpdateUnitLayer(transform, this);

            UpdateStats();
        }
    }

	// Update is called once per frame
	void Update()
	{
		if (isDead)
		{
			//rend.material.color = Color.black;

		}
		else
		{			
			// follows path if set to followpath
			if (followingPath)
			{
				FollowPath();
			}	
		}
	}

    public static void UpdateUnitLayer(Transform trans, Unit unit)
    {
        if (unit.unitType == UnitType.Ally || unit.unitType == UnitType.Player)
        {
            trans.gameObject.layer = (int)Layer_Index.Friendly;

            foreach (Transform child in trans.transform)
            {
                UpdateUnitLayer(child, unit);
            }
        }
        else
        {
            trans.gameObject.layer = (int)Layer_Index.Hostile;

            foreach (Transform child in trans.transform)
            {
                UpdateUnitLayer(child, unit);
            }
        }
    }

    #region Movement related functions
    public void ShowAvailableMoves()
	{
        if (path.Count <= 0)
            GridManager.instance.RangeWithCost(CurrentTile, MoveRange);
	}

	// Highlights and shows current path
	void ShowPath()
	{
        if (potentialPath != null)
        {
            foreach (var node in potentialPath)
            {
                node.info.SetColor(Color.blue);
            }
        }
	}

	// using a star pathing to generate path
	void GeneratePath()
	{
		// clears any highlighting if current path exists
		if (potentialPath != null)
		{
			foreach (var node in potentialPath)
			{
				node.info.SetDefault();
			}
		}

		// generates path if unit has current and dest tile
		if (CurrentTile && DestTile)
		{
			var newPath = Path<Tile>.FindPath(CurrentTile.tile, DestTile.tile, Tile.distance, Tile.estimate);

			// checks for valid path and within movement range
			if (newPath != null && newPath.TotalCost <= MoveRange)
			{
				potentialPath = newPath;
			}
            else
            {
                potentialPath = null;
            }
		}
	}

	// updates current destination tile if empty and generates and shows path
	public void UpdatePath(CellInfo grid)
	{
		if (unitType == UnitType.Player)
		{
			if (grid && !followingPath)
			{
				if (grid.isEmpty || grid.unit == this)
					DestTile = grid;

				GeneratePath();
                ShowAvailableMoves();
				ShowPath();
			}
		}
	}
 
	public bool FollowPath()
	{
		// Generates path to follow if one doesn't exist
		if (path.Count == 0 && potentialPath != null && potentialPath.TotalCost <= MoveRange)
		{
			path.Clear();			
			
			var cost = GetMoveCost((int)potentialPath.TotalCost);

            stats.actionPoints -= cost;

            foreach (var tile in potentialPath)
			{
				path.AddFirst(tile.info);
			}
			
			potentialPath = null;
		}

		if (path.Count > 0)
		{
            //Play move animation, randomize blend
            foreach (Animator anim in animators)
            {
                anim.SetFloat("Blend", blends[Random.Range(0,blends.Length)]);
                anim.SetBool("Moving", true);
            }
            // Current tile position
            Vector3 pos = transform.position;// - yOffset;

            //Vector3 pos = CurrentTile.transform.position;

			// Direction to next path node
			var dir = path.First.Value.transform.position - pos;

			//gameObject.transform.rotation=StaticFunctions.FaceObjectY(gameObject, path.First.Value.transform.position);
            //StartCoroutine(StaticFunctions.LerpObject(gameObject, StaticFunctions.FaceObjectY(gameObject, path.First.Value.transform.position)));

            var maxMove = dir.magnitude;
            
			dir.Normalize();

            if (dir != Vector3.zero)
            {
                transform.forward = dir;
            }

            float speedBoost = 1;

            if(!CurrentTile.fog.revealed && !path.First.Value.fog.revealed)
            {
                speedBoost = 10;
            }

            var moveAmount =  StaticValues.instance.unitMoveSpeed * speedBoost * Time.deltaTime;

            if (isHuge)
                moveAmount *= moveSpeedMod;

            moveAmount = Mathf.Clamp(moveAmount, 0, maxMove);

            transform.position += dir * moveAmount;

            // Distance check to determine if node has been reached
            var distance = Vector3.Distance(transform.position, path.First.Value.transform.position);

			if (distance <= 0.01f) 
			{
				// Destination Reached
				if (path.Count == 1)
				{
					followingPath = false;                    

                    CurrentTile = path.First.Value;

                    transform.position = CurrentTile.transform.position;

                    path.Clear();
                    //End movement
                    foreach(Animator anim in animators)
                    {
                        //anim.SetFloat("Blend", blends[Random.Range(0, blends.Length)]);
                        anim.SetBool("Moving", false);
                    }

					return false;
				}

				//CurrentTile = path.First.Value;
				if (path.First.Value.isEmpty)
					CurrentTile = path.First.Value;
				path.RemoveFirst();
			}
		}
		return true;
	}

    /// <summary>
    /// Returns current Movement range of unit given remaining actionPoints
    /// </summary>
    public int MoveRange { get { return Mathf.FloorToInt(stats.actionPoints * (MPA)); } }

    /// <summary>
    /// Returns tiles moved per action point
    /// Movespeed / 9
    /// </summary>
    public float MPA
    {
        get
        {
            return stats.moveSpeed / 9f;
        }
    }

    /// <summary>
    /// Returns AP cost to move x tiles
    /// </summary>
    /// <param name="tiles"></param>
    /// <returns></returns>
    public int GetMoveCost(int tiles)
    {
        return Mathf.CeilToInt((9f / stats.moveSpeed) * tiles);
    }

    /// <summary>
    /// Checks if unit has any remaining valid moves given movement range
    /// </summary>
    public bool HasValidMoves { get { return CurrentTile.tile.ValidMoveCount > 0; } }
    #endregion

    #region Set Color and death status

    public void ShowOutline()
    {
        if (matSetter)
        {
            matSetter.SetFloat(outline_ID, 1);
            foreach (var rend in renderers)
            {
                rend.sharedMaterial = matSetter;
            }
        }
    }

    public void HideOutline()
    {
        if (matSetter)
        {
            matSetter.SetFloat(outline_ID, 0);
            foreach (var rend in renderers)
            {
                rend.sharedMaterial = matSetter;
            }
        }
    }

 //   protected void SetActive()
	//{
	//	rend.material.color = activeColour;
	//}

	//protected void SetInactive()
	//{
	//	rend.material.color = inactiveColour;
		
	//}

	//protected void SetFinished()
	//{
	//	rend.material.color = finishedColour;
	//}

	//protected void SetDead()
	//{
 //       isDead = true;
 //       rend.material.color = Color.black;
	//}

	//public void SetYellow()
	//{
	//	rend.material.color = Color.yellow;
	//}

    #endregion

    #region Turn and Reset functions
    public void ResetActionPoints()
    {
        ResetUnit();
        if (!isDead)
        {
            stats.actionPoints = 9;
        }
    }

    // clears path and destTile when unit unselected
    public void ResetUnit()
    {
        if (isDead)
            return;

        potentialPath = null;
        DestTile = null;
        if (path.Count > 0)
        {
            CurrentTile = path.Last.Value;
        }
        path.Clear();
        followingPath = false;
        if (CurrentTile == null)
        {
            CurrentTile = GridManager.GetClosestEmpty(transform.position);
        }

        transform.position = CurrentTile.transform.position;
    }

    protected void OnStartTurn(UnitType u)
    {
        if (u == unitType)
        {
            ResetActionPoints();
            pathableRegion.Clear();
        }
        else
        {
            // Used to increase cost of leaving tiles adjacent to enemy units
            if (!isHuge)
                CurrentTile.tile.AddNeighbourExitCost(escapeCost);
            else
            {
                foreach ( var tile in GridManager.RingRangeUnobstructed(CurrentTile, hugeFactor + 1))
                {
                    tile.tile.AddExitCost(escapeCost);
                }
            }
        }

        ResetBaseColor();

        UpdateStats();

        if (stats.actionPoints < minAP)
        {
            stats.actionPoints = minAP;
        }
    }

	protected void OnEndTurn(UnitType u)
	{
		if(u == unitType)
		{
            TickStatus();
            TickCoolDowns();
            stats.actionPoints = 0;
            //SetFinished();            
            // Keeps track of which tiles have had cost increase to reset reliably
        }
	}
    #endregion

    public void ResetBaseColor()
    {
        if (unitType == UnitType.Player)
        {
            baseColorStack.Clear();
            baseEmmisiveStack.Clear();
            SetBaseColor(Color.blue);
            SetBaseEmmisiveValue(0.5f);
        }
        else
        {
            baseColorStack.Clear();
            baseEmmisiveStack.Clear();
            SetBaseColor(Color.red);
            SetBaseEmmisiveValue(0.5f);
        }
    }

    /// <summary>
    /// Applies damage to unit
    /// Passes in refence to unit which did the damage
    /// </summary>
    /// <param name="value"></param>
    /// <param name="damageDealer"></param>
    public void ApplyDamage(float value, Unit damageDealer, AbilityBase ability = null)
	{
        StartCoroutine(DamageDelay(.2f, value, damageDealer, ability));
	}

    IEnumerator DamageDelay(float delay, float value, Unit damageDealer, AbilityBase ability = null)
    {
        yield return new WaitForSecondsRealtime(delay);
        if (!isDead)
        {
            stats.HP -= value;
            if (value > 0)
            {
                damageDealer.damageDealt += value;
                damageTaken += value;
                foreach (Animator anim in animators)
                {
                    anim.SetFloat("Blend", blends[Random.Range(0, blends.Length)]);
                    anim.SetTrigger("TakeDamage");
                }
            }
            else
            {
                foreach (Animator anim in animators)
                {
                    anim.SetFloat("Blend", blends[Random.Range(0, blends.Length)]);
                    anim.SetTrigger("TakeNoDamage");
                }
                if (value < 0)
                {
                    healthHealed -= value;
                }
            }
            if (stats.HP > stats.maxHP)
                stats.HP = stats.maxHP;
        }

        if (stats.HP <= 0)
        {
            isDead = true;
            damageDealer.kills += 1;
            // Plays death sound if there is one
            if (deathSound)
            {
                audioSource.pitch = Random.Range(0.75f, 1.25f);
                audioSource.PlayOneShot(deathSound);
            }

            RemoveFromLists(true);

            foreach (var tile in overSizeTileList)
            {
                tile.unit = null;
            }

            GameManager.instance.AddEssence(level.essenceValue, transform.position);

            if (unitType == UnitType.Hostile)
            {
                float u = 1f / TurnManager.instance.playerUnits.Count;
                for (int i = 0; i < TurnManager.instance.playerUnits.Count; i++)
                {
                    TurnManager.instance.playerUnits[i].level.currentExp += level.expValue * u;
                }
            }
            level.expValue = 0;
            level.essenceValue = 0;

            var dir = transform.position - damageDealer.transform.position;

            if (ability)
            {
                dir = transform.position - ability.attackOrigin;
            }

            dir = (dir.normalized * 2 + Vector3.up).normalized;

            var ok = Mathf.Abs(stats.HP);

            ok /= stats.maxHP;

            var rb = GetComponent<Rigidbody>();

            rb.AddForce(dir * 50 + dir * ok * 1000);
        }
    }

	// Reduces all coolDowns by 1;
	protected void TickCoolDowns()
	{
		foreach(var ability in abilities)
		{
			ability.CoolDownTick(1);
		}
	}

	public float CurrentHealth
	{
		get
		{
			return stats.HP;
		}
	}

    public float HealthPercent
    {
        get
        {
            return stats.HP / stats.maxHP;
        }
    }    

    void RemoveFromLists(bool broadcastDeath = false)
    {       
        // Removes escape cost around unit when killed if one was applied
        if(TurnManager.instance.currentTeam != unitType)
        {
            if (cTile)
            {
                if (isHuge)
                {
                    foreach (var tile in GridManager.RingRangeUnobstructed(CurrentTile, hugeFactor + 1))
                    {
                        tile.tile.AddExitCost(-escapeCost);
                    }
                }
                else
                {
                    CurrentTile.tile.AddNeighbourExitCost(-escapeCost);
                }
            }

            // Recalculates avaliable moves for player
            //if (TurnManager.instance.currentTeam == UnitType.Player)
            //    if (UnitManager.instance.CurrentUnit)
            //        UnitManager.instance.CurrentUnit.ShowAvailableMoves();
        }

        if (broadcastDeath)
            UnitManager.OnUnitDeath.Invoke(this);

        // intentionally only removing unit from current tile
        // maintaining reference to current tile for some ability interactions
        if (cTile)
            cTile.unit = null;

        transform.BroadcastMessage("Dissolve", SendMessageOptions.DontRequireReceiver);
        gameObject.layer = 0;
        GetComponent<Rigidbody>().isKinematic = false;

        if (controller)
            controller.enabled = false;

        if (healthBar && healthBar.bar)
            healthBar.bar.enabled = false;

        GetComponent<PlayerUnitTooltip>().enabled = false;

        GetComponent<TooltipBasic>().enabled = false;

        if (GetComponent<ObjectHighlighter>() != null)
        {
            GetComponent<ObjectHighlighter>().enabled = false;
        }

        TurnManager.instance.OnEndTurn.RemoveListener(OnEndTurn);
        TurnManager.instance.OnStartTurn.RemoveListener(OnStartTurn);
        if (unitType == UnitType.Hostile)
        {
            TurnManager.instance.enemyUnits.Remove(GetComponent<Agent>());
        }
        else { TurnManager.instance.playerUnits.Remove(this); }
        TurnManager.instance.allUnits.Remove(this);

        if(TurnManager.instance.playerUnits.Count == 0)
        {
            MissionObjectives.instance.EndMission();
        }

        gameObject.layer = 16;
    }

    /// <summary>
    /// Ticks status and applies any heal and damage
    /// Removes and destroys any statuseffects with duration of 0
    /// </summary>
    void TickStatus()
    {
        for(int i = statuses.Count - 1; i >= 0; i--)
        {
            statuses[i].Tick();
            if (statuses[i].duration <= 0)
            {
                Destroy(statuses[i]);
                statuses.RemoveAt(i);
            }
        }

        UpdateStats();
    }

    /// <summary>
    /// Checks whether unit has enough exp to level up
    /// </summary>
    public bool LevelUp()
    {
        bool leveledUp = false;

        if (GameManager.instance.playerEssence > StaticValues.instance.expTable[level.unitLevel + 1] * 0.5f)
        {
            GameManager.instance.playerEssence -= StaticValues.instance.expTable[level.unitLevel + 1] * 0.5f;
            level.currentExp -= StaticValues.instance.expTable[level.unitLevel + 1];
            level.essenceValue += level.essenceValue / (level.unitLevel + 1);
            level.expValue += (level.expValue / (level.unitLevel + 1)) * 0.5f;
            level.unitLevel++;
            level.skillPoints++;

            baseStats += lvlStats;

            leveledUp = true;
        }

        return leveledUp;
    }

    /// <summary>
    /// Levels up unit by x amount
    /// </summary>
    /// <returns></returns>
    public bool ForceLevelUp(int amount)
    {
        int counter = 0;

        while(counter++ < amount)
        {
            level.essenceValue += level.essenceValue / (level.unitLevel + 1);
            level.expValue += (level.expValue / (level.unitLevel + 1)) * 0.5f;
            level.unitLevel++;

            level.skillPoints++;

            baseStats += lvlStats;
        }
        if (skillTree != null)
        {
            for (int i = 0; i < skillTree.sets.Count; i++)
            {
                if (skillTree.sets[i].requiredLevel <= level.unitLevel)
                {
                    if (level.skillPoints >= skillTree.sets[i].cost)
                        skillTree.ApplySet(this, skillTree.sets[i]);
                }
            }
        }

        ResetActionPoints();

        UpdateStats();

        return true;
    }

    public void SetCurrentTile(CellInfo tile)
    {
        CurrentTile = tile;
    }

    public void UpdateStats()
    {
        baseStats.HP = baseStats.maxHP * HealthPercent;
        baseStats.actionPoints = stats.actionPoints;
        stats = baseStats * multStatsMod + staticStatsMod;
    }

    public Vector3 GetHeadPos()
    {
        if (headTransform)
            return headTransform.position;

        return CurrentTile.transform.position + new Vector3(0, 1.5f, 0);
    }

    // Moves unit to closest empty tile
    public void SnapToClosestTile()
    {
        if(TurnManager.instance.currentTeam != unitType)
        {
            // removes escape cost before being pushed
            CurrentTile.tile.AddNeighbourExitCost(-escapeCost);
        }

        CurrentTile = GridManager.GetClosestEmpty(transform.position);       

        transform.position = CurrentTile.transform.position;

        if (TurnManager.instance.currentTeam != unitType)
        {
            // adds escape cost to new tiles
            CurrentTile.tile.AddNeighbourExitCost(escapeCost);
        }
    }

    public void SetOversizeTiles()
    {
        foreach(var tile in overSizeTileList)
        {
            tile.unit = null;
        }

        overSizeTileList = GridManager.RingRangeALL(cTile, hugeFactor, 1);

        foreach(var tile in overSizeTileList)
        {
            if (tile.unit && tile.unit != this)
            {
                // ref to unit before moving it
                var unit = tile.unit;

                unit.SnapToClosestTile();

                // applies stomp damage
                var damage = stats.Damage * stompDamage;
                damageDealt += damage;
                unit.damageTaken += damage;
                unit.ApplyDamage(Mathf.RoundToInt(damage), this);

                PoolMananger.instance.GetPooledText(((int)damage).ToString(), Color.red, unit.transform.position);
            }

            tile.unit = this;
        }

        if (RTSCamera.instance)
            RTSCamera.instance.CameraShake(shakeDuration, shakeAmount);
    }

    public void ClearOversizeTiles()
    {
        foreach (var tile in overSizeTileList)
        {
            tile.unit = null;
        }

        overSizeTileList.Clear();
    }

    void OnDestroy()
    {
        //Debug.Log(name + " destroyed");
        RemoveFromLists();
    }


    public void ResetStatMods()
    {
        staticStatsMod.Zero();
        multStatsMod.SetAll(1);
    }

	/// <summary>
	/// Generates a full list of all reachable tiles
	/// Stores list in pathableRegion
	/// </summary>
	public void GeneratePathableRegion()
	{
		pathableRegion = GridManager.GetPathableRegion(CurrentTile);
	}

    public void CopySkillTree()
    {
        if (skillTree)
        {
            var newtree = Instantiate(skillTree);
            newtree.name = skillTree.name;
            skillTree = newtree;

            skillTree.sets.Remove(null);
        }
    }

    public void SetBaseColor(Color col)
    {
        if (baseRenderer)
        {
            if (baseRenderer.material.GetColor(baseColor_ID) != col)
            {
                baseColorStack.Push(baseRenderer.material.GetColor(baseColor_ID));
                baseRenderer.material.SetColor(baseColor_ID, col);
            }
        }
    }

    public void SetBasePrevColor()
    {
        if (baseRenderer)
        {
            if (baseColorStack.Count > 0)
                baseRenderer.material.SetColor(baseColor_ID, baseColorStack.Pop());
            else
            {
                if (unitType == UnitType.Player)
                    baseRenderer.material.SetColor(baseColor_ID, Color.blue);
                else
                    baseRenderer.material.SetColor(baseColor_ID, Color.red);
            }
        }
    }

    public void SetBaseEmmisiveValue(float value)
    {
        if (baseRenderer)
        {
            if (baseRenderer.material.GetFloat(baseEmmisiveValue_ID) != value)
            {
                baseEmmisiveStack.Push(baseRenderer.material.GetFloat(baseEmmisiveValue_ID));

                baseRenderer.material.SetFloat(baseEmmisiveValue_ID, value);
            }
        }
    }

    public void SetBasePrevEmValue()
    {
        if (baseRenderer)
        {
            if (baseEmmisiveStack.Count > 0)
                baseRenderer.material.SetFloat(baseEmmisiveValue_ID, baseEmmisiveStack.Pop());
            else
                baseRenderer.material.SetFloat(baseEmmisiveValue_ID, 0.5f);
        }
    }
}