﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CantStartWithoutUnits : MonoBehaviour {
    public Button button; 
	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.instance.missionUnits.Count < 1)
        {
            button.interactable = false;
        }
        else
        {
            button.interactable = true;
        }
	}
}
