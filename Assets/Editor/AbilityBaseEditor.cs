﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AbilityBase))] [CanEditMultipleObjects]
public class AbilityBaseEditor : Editor
{
	public override void OnInspectorGUI()
	{
		var e = target as AbilityBase;

		string name;
		name = EditorGUILayout.DelayedTextField("Name", e.name);

		if (name != e.name)
		{
			string assetPath = AssetDatabase.GetAssetPath(e.GetInstanceID());
			AssetDatabase.RenameAsset(assetPath, name);
			AssetDatabase.SaveAssets();
		}
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("damageMod"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("damage"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("accuracy"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("critChance"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("critMod"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("hasCharges"));
		if (e.hasCharges)
			EditorGUILayout.PropertyField(serializedObject.FindProperty("charges"));

		EditorGUILayout.PropertyField(serializedObject.FindProperty("statusEffects"), true);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("hitEffects"), true);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("makeTargetCaster"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("abilityType"));
        if (e.abilityType == AbilityBase.AbilityType.Projectile)
        {
            var p = serializedObject.FindProperty("projectilePrefab");
            EditorGUILayout.PropertyField(p);
            e.projectileSpeed = EditorGUILayout.FloatField(new GUIContent("Projectile Speed", "How fast the projectile moves"), e.projectileSpeed);
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("apCost"));

		EditorGUILayout.PropertyField(serializedObject.FindProperty("coolDown"));

		EditorGUILayout.PropertyField(serializedObject.FindProperty("targets"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("range"), true);

        //var test = serializedObject.FindProperty("range");

        //var rangeValues = new Vector2(e.range.min, e.range.max);

        //EditorGUILayout.Vector2Field("Range", rangeValues);

        //EditorGUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("Range", GUILayout.Width(EditorGUIUtility.currentViewWidth * .34f));
        //EditorGUILayout.FloatField("Min", e.range.min, GUILayout.Width(EditorGUIUtility.currentViewWidth * .2f));
        //EditorGUILayout.FloatField("Max", e.range.max, GUILayout.Width(EditorGUIUtility.currentViewWidth * .2f));

        //EditorGUILayout.EndHorizontal();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("requireLOS"));

		//EditorGUILayout.PropertyField(serializedObject.FindProperty("hexTarget"));

		EditorGUILayout.PropertyField(serializedObject.FindProperty("areaOfEffect"));
		if(e.areaOfEffect == AbilityBase.AreaOfEffect.AreaOfEffect)
			EditorGUILayout.PropertyField(serializedObject.FindProperty("aoeSize"));
		if(e.areaOfEffect == AbilityBase.AreaOfEffect.Chaining)
		{
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bounces"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bounceDelay"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("canReHit"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bounceRange"));
		}
		EditorGUILayout.PropertyField(serializedObject.FindProperty("clickLayer"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("shakeOnHit"));

        if(e.shakeOnHit)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shakeAmount"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shakeDuration"));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("abilityColor"));

		EditorGUILayout.PropertyField(serializedObject.FindProperty("particles"));

        if (e.particles != null)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("particlePosition"));
        }

		EditorGUILayout.PropertyField(serializedObject.FindProperty("icon"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("castSound"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("hitSound"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("description"));

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		EditorUtility.SetDirty(target);
	}
}
