﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SkillTree))]
public class SkillTreePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var p = property.serializedObject.FindProperty("unitType");

        if(p != null && p.intValue == (int)UnitType.Player)
        {
            EditorGUI.ObjectField(position, property, label);
        }
        else if(p == null)
        {
            EditorGUI.ObjectField(position, property, label);
        }
    }
}