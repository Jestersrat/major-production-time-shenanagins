﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(UnitSpawner))]
public class UnitSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var e = target as UnitSpawner;

        EditorGUILayout.PropertyField(serializedObject.FindProperty("spawnLimit"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("spawnBudget"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("repeat"));

        if(e.repeat > 0)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("repeatBudget"));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("triggerOnly"));

        if(!e.triggerOnly)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("spawnDelay"));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("spawnOrder"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("unitTeam"));

        if(EditorGUILayout.PropertyField(serializedObject.FindProperty("unitsSpawned"), true))
        {
            //var obj = serializedObject.FindProperty("unitsSpawned");

            foreach (var set in e.SpawnList)
            {
                EditorGUILayout.BeginHorizontal();
                set.unit = EditorGUILayout.ObjectField(set.unit, typeof(Unit), true) as Unit;
                set.level = EditorGUILayout.IntField("Level", set.level);
                set.spawnCost = EditorGUILayout.IntField("Spawn Cost", set.spawnCost);
                EditorGUILayout.EndHorizontal();
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
