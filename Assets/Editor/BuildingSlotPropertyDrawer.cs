﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomPropertyDrawer(typeof(BuildingSlot))]
public class BuildingSlotPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        // Getting status property
        var status = property.FindPropertyRelative("status");

        // Drawing status property
        EditorGUI.PropertyField(position, status, true);

        var newPost = position;

        newPost.y += EditorGUIUtility.singleLineHeight;

        newPost.height *= 0.5f;

        if (status.intValue == (int)SlotStatus.Unlocked)
        {
            var cost = property.FindPropertyRelative("cost");
            cost.floatValue = EditorGUI.FloatField(newPost, cost.name, cost.floatValue);
        }
        else
        {
            var building = property.FindPropertyRelative("building");
            EditorGUI.PropertyField(newPost, building);
        }

        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {

        return EditorGUIUtility.singleLineHeight * 2;
    }
}
