﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SkillTree))]
public class SkillTreeEditor : Editor
{
    int currentIndex = 0;

    public override void OnInspectorGUI()
    {
        var e = target as SkillTree;

        if (EditorGUILayout.PropertyField(serializedObject.FindProperty("sets")))
        {
            EditorGUI.indentLevel = EditorGUI.indentLevel + 1;

            var count = EditorGUILayout.DelayedIntField("Size", e.sets.Capacity);

            while (count < e.sets.Capacity)
            {
                e.sets.RemoveAt(e.sets.Count - 1);
                e.sets.Capacity--;
            }

            while (count > e.sets.Capacity)
            {
                e.sets.Capacity++;
                e.sets.Add(null);
            }

            var sets = serializedObject.FindProperty("sets");

            for (int i = 0; i < e.sets.Capacity; ++i)
            {
                GUI.SetNextControlName("Set" + i);

                e.sets[i] = EditorGUILayout.ObjectField("Element " + i , e.sets[i], typeof(SkillSet), true) as SkillSet;          

                if (e.sets[i] != null && i < e.sets.Count)
                {
                    var setI = sets.GetArrayElementAtIndex(i);
                   
                    if (setI != null && setI.objectReferenceValue)
                    {
                        var p = new SerializedObject(setI.objectReferenceValue);

                        if (Event.current.type == EventType.layout)
                        {
                            if (GUI.GetNameOfFocusedControl() == "Set" + i)
                            {
                                currentIndex = i;
                            }
                        }

                        if (currentIndex == i)
                        {
                            EditorGUI.indentLevel = EditorGUI.indentLevel + 1;

                            EditorGUILayout.PropertyField(p.FindProperty("abilities"), true);

                            EditorGUILayout.PropertyField(p.FindProperty("requiredLevel"), true);

                            EditorGUILayout.PropertyField(p.FindProperty("cost"), true);

                            EditorGUILayout.PropertyField(p.FindProperty("hasStatsMod"));

                            if (e.sets[i].hasStatsMod)
                            {
                                EditorGUILayout.PropertyField(p.FindProperty("statsMod"), true);
                            }

                            EditorGUILayout.PropertyField(p.FindProperty("blocks"), true);

                            p.ApplyModifiedPropertiesWithoutUndo();

                            EditorGUI.indentLevel = EditorGUI.indentLevel - 1;
                        }

                        //p.ApplyModifiedPropertiesWithoutUndo();
                        p.ApplyModifiedProperties();
                    }
                }
            }

            EditorGUI.indentLevel = EditorGUI.indentLevel - 1;
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("setsLearnt"), true);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("setsBlocked"), true);

        EditorUtility.SetDirty(target);
        //serializedObject.ApplyModifiedPropertiesWithoutUndo();
        serializedObject.ApplyModifiedProperties();
    }
}
