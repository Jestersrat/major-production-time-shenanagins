﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AbilityObstacle))]
[CanEditMultipleObjects]
public class AbilityObstacleEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var e = target as AbilityObstacle;

        string name;
        name = EditorGUILayout.DelayedTextField("Name", e.name);

        if (name != e.name)
        {
            string assetPath = AssetDatabase.GetAssetPath(e.GetInstanceID());
            AssetDatabase.RenameAsset(assetPath, name);
            AssetDatabase.SaveAssets();
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("obstacleObject"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("duration"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("canTargetTileWithUnit"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("noPushBack"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("damageMod"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("damage"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("range"), true);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("requireLOS"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("hasCharges"));

        if (e.hasCharges)
            EditorGUILayout.PropertyField(serializedObject.FindProperty("charges"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("apCost"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("coolDown"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("statusEffects"), true);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("hitEffects"), true);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("shakeOnHit"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("targets"));

        if (e.shakeOnHit)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shakeAmount"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shakeDuration"));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("abilityColor"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("particles"));

        if (e.particles != null)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("particlePosition"));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("icon"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("castSound"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("hitSound"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("description"));

        serializedObject.ApplyModifiedPropertiesWithoutUndo();

        EditorUtility.SetDirty(target);
    }
}