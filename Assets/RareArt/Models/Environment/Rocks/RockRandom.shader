// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32838,y:32701,varname:node_3138,prsc:2|diff-172-OUT,spec-2799-OUT,gloss-6965-OUT,normal-5521-RGB;n:type:ShaderForge.SFN_Tex2d,id:717,x:32104,y:32514,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:node_717,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ebe4b3eadf926f54c8fe181df5f84ddc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4134,x:32255,y:32439,varname:node_4134,prsc:2|A-1341-RGB,B-717-RGB;n:type:ShaderForge.SFN_Tex2d,id:3649,x:31888,y:32514,ptovrint:False,ptlb:Main_Tex,ptin:_Main_Tex,varname:node_3649,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:45793f91a8dace34e8ec531252f5e47c,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Add,id:172,x:32467,y:32539,varname:node_172,prsc:2|A-4134-OUT,B-1697-OUT;n:type:ShaderForge.SFN_Multiply,id:1697,x:32104,y:32679,varname:node_1697,prsc:2|A-3649-RGB,B-2260-RGB;n:type:ShaderForge.SFN_Color,id:2260,x:31888,y:32679,ptovrint:False,ptlb:Main_Color,ptin:_Main_Color,varname:node_2260,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8529412,c2:0.8529412,c3:0.8529412,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5521,x:32467,y:32704,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_5521,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:cb76aaabebfd0904d9a7eab5472ac836,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:6520,x:32104,y:32852,ptovrint:False,ptlb:Specular_Tex,ptin:_Specular_Tex,varname:node_6520,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:995,x:32034,y:33112,ptovrint:False,ptlb:Roughness_Tex,ptin:_Roughness_Tex,varname:node_995,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:7795,x:32256,y:33145,varname:node_7795,prsc:2|A-995-RGB,B-4920-OUT;n:type:ShaderForge.SFN_Slider,id:4920,x:31877,y:33294,ptovrint:False,ptlb:Roughness,ptin:_Roughness,varname:node_4920,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Add,id:2799,x:32467,y:32950,varname:node_2799,prsc:2|A-4495-OUT,B-7795-OUT;n:type:ShaderForge.SFN_Multiply,id:4495,x:32310,y:32950,varname:node_4495,prsc:2|A-6520-RGB,B-6983-OUT;n:type:ShaderForge.SFN_Slider,id:6983,x:31967,y:33031,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_6983,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:6965,x:32310,y:32880,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_6965,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6965812,max:1;n:type:ShaderForge.SFN_Tex2d,id:1341,x:31888,y:32332,ptovrint:False,ptlb:ColorRandomizer,ptin:_ColorRandomizer,varname:node_1341,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,tex:0f4a78305c135dd4383395154715d7f8,ntxv:0,isnm:False|UVIN-7015-OUT;n:type:ShaderForge.SFN_Append,id:7015,x:31725,y:32332,varname:node_7015,prsc:2|A-2748-OUT,B-2748-OUT;n:type:ShaderForge.SFN_Add,id:5405,x:31411,y:32229,varname:node_5405,prsc:2|A-5004-X,B-5004-Y,C-5004-Z;n:type:ShaderForge.SFN_Multiply,id:2748,x:31569,y:32296,varname:node_2748,prsc:2|A-5405-OUT,B-6258-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:5004,x:31237,y:32210,varname:node_5004,prsc:2;n:type:ShaderForge.SFN_Pi,id:6258,x:31444,y:32357,varname:node_6258,prsc:2;proporder:717-3649-2260-5521-6520-6983-995-4920-6965-1341;pass:END;sub:END;*/

Shader "Shader Forge/RockRandom" {
    Properties {
        _Mask ("Mask", 2D) = "white" {}
        _Main_Tex ("Main_Tex", 2D) = "black" {}
        _Main_Color ("Main_Color", Color) = (0.8529412,0.8529412,0.8529412,1)
        _Normal ("Normal", 2D) = "bump" {}
        _Specular_Tex ("Specular_Tex", 2D) = "white" {}
        _Specular ("Specular", Range(0, 1)) = 0
        _Roughness_Tex ("Roughness_Tex", 2D) = "white" {}
        _Roughness ("Roughness", Range(0, 1)) = 1
        _Gloss ("Gloss", Range(0, 1)) = 0.6965812
        [NoScaleOffset]_ColorRandomizer ("ColorRandomizer", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform sampler2D _Main_Tex; uniform float4 _Main_Tex_ST;
            uniform float4 _Main_Color;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Specular_Tex; uniform float4 _Specular_Tex_ST;
            uniform sampler2D _Roughness_Tex; uniform float4 _Roughness_Tex_ST;
            uniform float _Roughness;
            uniform float _Specular;
            uniform float _Gloss;
            uniform sampler2D _ColorRandomizer;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _Specular_Tex_var = tex2D(_Specular_Tex,TRANSFORM_TEX(i.uv0, _Specular_Tex));
                float4 _Roughness_Tex_var = tex2D(_Roughness_Tex,TRANSFORM_TEX(i.uv0, _Roughness_Tex));
                float3 specularColor = ((_Specular_Tex_var.rgb*_Specular)+(_Roughness_Tex_var.rgb*_Roughness));
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float node_2748 = ((objPos.r+objPos.g+objPos.b)*3.141592654);
                float2 node_7015 = float2(node_2748,node_2748);
                float4 _ColorRandomizer_var = tex2D(_ColorRandomizer,node_7015);
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                float4 _Main_Tex_var = tex2D(_Main_Tex,TRANSFORM_TEX(i.uv0, _Main_Tex));
                float3 diffuseColor = ((_ColorRandomizer_var.rgb*_Mask_var.rgb)+(_Main_Tex_var.rgb*_Main_Color.rgb));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform sampler2D _Main_Tex; uniform float4 _Main_Tex_ST;
            uniform float4 _Main_Color;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Specular_Tex; uniform float4 _Specular_Tex_ST;
            uniform sampler2D _Roughness_Tex; uniform float4 _Roughness_Tex_ST;
            uniform float _Roughness;
            uniform float _Specular;
            uniform float _Gloss;
            uniform sampler2D _ColorRandomizer;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _Specular_Tex_var = tex2D(_Specular_Tex,TRANSFORM_TEX(i.uv0, _Specular_Tex));
                float4 _Roughness_Tex_var = tex2D(_Roughness_Tex,TRANSFORM_TEX(i.uv0, _Roughness_Tex));
                float3 specularColor = ((_Specular_Tex_var.rgb*_Specular)+(_Roughness_Tex_var.rgb*_Roughness));
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float node_2748 = ((objPos.r+objPos.g+objPos.b)*3.141592654);
                float2 node_7015 = float2(node_2748,node_2748);
                float4 _ColorRandomizer_var = tex2D(_ColorRandomizer,node_7015);
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                float4 _Main_Tex_var = tex2D(_Main_Tex,TRANSFORM_TEX(i.uv0, _Main_Tex));
                float3 diffuseColor = ((_ColorRandomizer_var.rgb*_Mask_var.rgb)+(_Main_Tex_var.rgb*_Main_Color.rgb));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
