// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.06,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33102,y:32791,varname:node_4013,prsc:2|diff-6727-OUT,normal-9097-RGB,emission-6756-OUT,clip-4675-OUT;n:type:ShaderForge.SFN_Tex2d,id:434,x:32092,y:33211,varname:node_434,prsc:2,tex:271f5ee3273dd7f4fae6e204d4f8c4bf,ntxv:0,isnm:False|UVIN-3903-OUT,TEX-7498-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7498,x:31906,y:33043,ptovrint:False,ptlb:Dissolve_Texture,ptin:_Dissolve_Texture,varname:node_7498,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:271f5ee3273dd7f4fae6e204d4f8c4bf,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Append,id:3903,x:31906,y:33204,varname:node_3903,prsc:2|A-8570-OUT,B-8230-OUT;n:type:ShaderForge.SFN_Vector1,id:8230,x:31717,y:33326,varname:node_8230,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2d,id:6918,x:31761,y:33519,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_6918,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:5370,x:31260,y:33698,ptovrint:False,ptlb:Dissolve_Amount,ptin:_Dissolve_Amount,varname:node_5370,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:4675,x:31936,y:33519,varname:node_4675,prsc:2|A-6918-R,B-1751-OUT;n:type:ShaderForge.SFN_RemapRange,id:1751,x:31761,y:33673,varname:node_1751,prsc:2,frmn:0,frmx:1,tomn:-0.9,tomx:0.6|IN-1601-OUT;n:type:ShaderForge.SFN_OneMinus,id:1601,x:31578,y:33698,varname:node_1601,prsc:2|IN-5370-OUT;n:type:ShaderForge.SFN_RemapRange,id:8445,x:31379,y:33313,varname:node_8445,prsc:2,frmn:0,frmx:1,tomn:-4,tomx:4|IN-4675-OUT;n:type:ShaderForge.SFN_Clamp01,id:6313,x:31524,y:33272,varname:node_6313,prsc:2|IN-8445-OUT;n:type:ShaderForge.SFN_OneMinus,id:8570,x:31717,y:33194,varname:node_8570,prsc:2|IN-6313-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5847,x:32245,y:33344,varname:node_5847,prsc:2,cc1:2,cc2:-1,cc3:-1,cc4:-1|IN-434-RGB;n:type:ShaderForge.SFN_Color,id:7649,x:32245,y:33201,ptovrint:False,ptlb:Dissolve_Color,ptin:_Dissolve_Color,varname:node_7649,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.006896496,c4:1;n:type:ShaderForge.SFN_Multiply,id:7308,x:32501,y:33263,varname:node_7308,prsc:2|A-7649-RGB,B-5847-OUT,C-9564-OUT;n:type:ShaderForge.SFN_Tex2d,id:1213,x:32745,y:32448,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_1213,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:79b7148db725e384cbe55fcb0b8ed467,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:6727,x:32933,y:32652,varname:node_6727,prsc:2|A-1213-RGB,B-9264-RGB;n:type:ShaderForge.SFN_Color,id:9264,x:32745,y:32632,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9264,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:8096,x:32167,y:32680,ptovrint:False,ptlb:Emmisive_Color,ptin:_Emmisive_Color,varname:node_8096,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:4700,x:32167,y:32912,ptovrint:False,ptlb:Emmisive_Mask,ptin:_Emmisive_Mask,varname:node_4700,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,tex:7c8525f5bf4ab544dae72cc40fddf8ea,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4688,x:32416,y:32929,varname:node_4688,prsc:2|A-8096-RGB,B-4700-RGB,C-3380-OUT;n:type:ShaderForge.SFN_Add,id:6756,x:32665,y:33103,varname:node_6756,prsc:2|A-4688-OUT,B-7308-OUT;n:type:ShaderForge.SFN_Tex2d,id:9097,x:32547,y:32783,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_9097,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,tex:7a78e738d8397db49894c5ddb633aab7,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Slider,id:3380,x:32064,y:33102,ptovrint:False,ptlb:Emmisive_Value,ptin:_Emmisive_Value,varname:node_3380,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5,max:5;n:type:ShaderForge.SFN_ValueProperty,id:9564,x:32245,y:33529,ptovrint:False,ptlb:Global_Dissolve_Multiplier,ptin:_Global_Dissolve_Multiplier,varname:node_9564,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;proporder:9264-1213-6918-7498-7649-5370-8096-4700-9097-3380-9564;pass:END;sub:END;*/

Shader "Shader Forge/CharacterBaseShader" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Noise ("Noise", 2D) = "white" {}
        _Dissolve_Texture ("Dissolve_Texture", 2D) = "white" {}
        _Dissolve_Color ("Dissolve_Color", Color) = (0,1,0.006896496,1)
        _Dissolve_Amount ("Dissolve_Amount", Range(0, 1)) = 0
        _Emmisive_Color ("Emmisive_Color", Color) = (1,0,0,1)
        [NoScaleOffset]_Emmisive_Mask ("Emmisive_Mask", 2D) = "black" {}
        [NoScaleOffset]_Normal ("Normal", 2D) = "bump" {}
        _Emmisive_Value ("Emmisive_Value", Range(0, 5)) = 5
        _Global_Dissolve_Multiplier ("Global_Dissolve_Multiplier", Float ) = 5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Dissolve_Texture; uniform float4 _Dissolve_Texture_ST;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Dissolve_Amount;
            uniform float4 _Dissolve_Color;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            uniform float4 _Emmisive_Color;
            uniform sampler2D _Emmisive_Mask;
            uniform sampler2D _Normal;
            uniform float _Emmisive_Value;
            uniform float _Global_Dissolve_Multiplier;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,i.uv0));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_4675 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_4675 - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float3 diffuseColor = (_Diffuse_var.rgb*_Color.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 _Emmisive_Mask_var = tex2D(_Emmisive_Mask,i.uv0);
                float2 node_3903 = float2((1.0 - saturate((node_4675*8.0+-4.0))),0.0);
                float4 node_434 = tex2D(_Dissolve_Texture,TRANSFORM_TEX(node_3903, _Dissolve_Texture));
                float3 emissive = ((_Emmisive_Color.rgb*_Emmisive_Mask_var.rgb*_Emmisive_Value)+(_Dissolve_Color.rgb*node_434.rgb.b*_Global_Dissolve_Multiplier));
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Dissolve_Texture; uniform float4 _Dissolve_Texture_ST;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Dissolve_Amount;
            uniform float4 _Dissolve_Color;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            uniform float4 _Emmisive_Color;
            uniform sampler2D _Emmisive_Mask;
            uniform sampler2D _Normal;
            uniform float _Emmisive_Value;
            uniform float _Global_Dissolve_Multiplier;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,i.uv0));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_4675 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_4675 - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float3 diffuseColor = (_Diffuse_var.rgb*_Color.rgb);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Dissolve_Amount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_4675 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_4675 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
