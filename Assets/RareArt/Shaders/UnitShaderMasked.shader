// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:0,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:2,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.06,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:32719,y:32712,varname:node_2865,prsc:2|diff-7059-OUT,spec-9672-OUT,gloss-358-OUT,normal-5964-RGB,emission-8861-OUT,difocc-4887-OUT,spcocc-4887-OUT,clip-5887-OUT,olwid-3730-OUT,olcol-9103-RGB;n:type:ShaderForge.SFN_Tex2d,id:5964,x:31782,y:33137,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Slider,id:358,x:31957,y:32892,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_358,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:31671,y:32831,ptovrint:False,ptlb:MetallicValue,ptin:_MetallicValue,varname:_Metallic_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:9036,x:31489,y:34084,varname:node_9036,prsc:2|A-9110-RGB,B-6417-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6417,x:31307,y:34062,varname:node_6417,prsc:2,cc1:2,cc2:-1,cc3:-1,cc4:-1|IN-7776-RGB;n:type:ShaderForge.SFN_Color,id:9110,x:31307,y:33913,ptovrint:False,ptlb:Dissolve_Color,ptin:_Dissolve_Color,varname:node_9110,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7776,x:31123,y:33999,varname:node_7776,prsc:2,tex:271f5ee3273dd7f4fae6e204d4f8c4bf,ntxv:0,isnm:False|UVIN-5383-OUT,TEX-7323-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7323,x:30948,y:34051,ptovrint:False,ptlb:Dissolve_Pattern,ptin:_Dissolve_Pattern,varname:node_7323,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,tex:271f5ee3273dd7f4fae6e204d4f8c4bf,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Append,id:5383,x:30948,y:33906,varname:node_5383,prsc:2|A-7060-OUT,B-828-OUT;n:type:ShaderForge.SFN_OneMinus,id:7060,x:30739,y:33906,varname:node_7060,prsc:2|IN-75-OUT;n:type:ShaderForge.SFN_Clamp01,id:75,x:30554,y:33906,varname:node_75,prsc:2|IN-8507-OUT;n:type:ShaderForge.SFN_RemapRange,id:8507,x:30363,y:33906,varname:node_8507,prsc:2,frmn:0,frmx:1,tomn:-4,tomx:4|IN-5887-OUT;n:type:ShaderForge.SFN_Add,id:5887,x:31090,y:34290,varname:node_5887,prsc:2|A-5925-R,B-389-OUT;n:type:ShaderForge.SFN_RemapRange,id:389,x:30895,y:34351,varname:node_389,prsc:2,frmn:0,frmx:1,tomn:-0.9,tomx:0.6|IN-3242-OUT;n:type:ShaderForge.SFN_Tex2d,id:5925,x:30786,y:34172,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_5925,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_OneMinus,id:3242,x:30730,y:34340,varname:node_3242,prsc:2|IN-1457-OUT;n:type:ShaderForge.SFN_Slider,id:1457,x:29398,y:33728,ptovrint:False,ptlb:Dissolve_Amount,ptin:_Dissolve_Amount,varname:node_1457,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_If,id:8128,x:31357,y:33553,varname:node_8128,prsc:2|A-1199-OUT,B-9274-OUT,GT-6089-OUT,EQ-6089-OUT,LT-2223-OUT;n:type:ShaderForge.SFN_Color,id:9103,x:31992,y:34206,ptovrint:False,ptlb:Outline_Color,ptin:_Outline_Color,varname:node_9103,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:9274,x:31112,y:33573,varname:node_9274,prsc:2,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:1199,x:31112,y:33504,ptovrint:False,ptlb:OutlineOn,ptin:_OutlineOn,varname:node_1199,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Vector1,id:2223,x:31112,y:33717,varname:node_2223,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:6089,x:31112,y:33647,ptovrint:False,ptlb:Outline_Width,ptin:_Outline_Width,varname:node_6089,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.01;n:type:ShaderForge.SFN_If,id:3730,x:31688,y:33397,varname:node_3730,prsc:2|A-1457-OUT,B-2223-OUT,GT-2223-OUT,EQ-8128-OUT,LT-8128-OUT;n:type:ShaderForge.SFN_Vector1,id:828,x:30739,y:34035,varname:node_828,prsc:2,v1:0;n:type:ShaderForge.SFN_Add,id:8861,x:31641,y:34101,varname:node_8861,prsc:2|A-9036-OUT,B-3254-OUT;n:type:ShaderForge.SFN_Tex2d,id:3377,x:30810,y:34673,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_3377,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_ComponentMask,id:3060,x:31828,y:32630,varname:node_3060,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7542-RGB;n:type:ShaderForge.SFN_Tex2d,id:7542,x:31636,y:32630,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:node_7542,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:9672,x:32001,y:32740,varname:node_9672,prsc:2|A-3060-OUT,B-1813-OUT;n:type:ShaderForge.SFN_Tex2d,id:2090,x:31593,y:34290,ptovrint:False,ptlb:Occlusion,ptin:_Occlusion,varname:node_2090,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_ComponentMask,id:4887,x:31815,y:34152,varname:node_4887,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-2090-RGB;n:type:ShaderForge.SFN_Multiply,id:3254,x:31144,y:34672,varname:node_3254,prsc:2|A-1296-OUT,B-3377-RGB,C-7-RGB;n:type:ShaderForge.SFN_Color,id:7,x:30971,y:34733,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_7,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:1296,x:30971,y:34638,ptovrint:False,ptlb:EmissionValue,ptin:_EmissionValue,varname:node_1296,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Tex2d,id:4573,x:30768,y:31843,ptovrint:False,ptlb:Albedo,ptin:_Albedo,varname:node_4573,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9931,x:30233,y:32609,ptovrint:False,ptlb:Mask1,ptin:_Mask1,varname:node_9931,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:9404,x:30566,y:32477,ptovrint:False,ptlb:RedValueMask1,ptin:_RedValueMask1,varname:node_9404,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:9927,x:30371,y:32474,ptovrint:False,ptlb:RedColorMask1,ptin:_RedColorMask1,varname:node_9927,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:9773,x:30566,y:32540,varname:node_9773,prsc:2|A-9927-RGB,B-9931-R;n:type:ShaderForge.SFN_Multiply,id:4996,x:31204,y:32523,varname:node_4996,prsc:2|A-9773-OUT,B-314-OUT;n:type:ShaderForge.SFN_Lerp,id:314,x:30998,y:32523,varname:node_314,prsc:2|A-9931-R,B-4573-RGB,T-9404-OUT;n:type:ShaderForge.SFN_Multiply,id:8695,x:30572,y:32729,varname:node_8695,prsc:2|A-9931-G,B-6988-RGB;n:type:ShaderForge.SFN_Color,id:6988,x:30380,y:32785,ptovrint:False,ptlb:GreenColorMask1,ptin:_GreenColorMask1,varname:node_6988,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1999999,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:2430,x:30998,y:32654,varname:node_2430,prsc:2|A-9931-G,B-4573-RGB,T-834-OUT;n:type:ShaderForge.SFN_Slider,id:834,x:30572,y:32878,ptovrint:False,ptlb:GreenValueMask1,ptin:_GreenValueMask1,varname:node_834,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:7792,x:31204,y:32654,varname:node_7792,prsc:2|A-8695-OUT,B-2430-OUT;n:type:ShaderForge.SFN_Add,id:3290,x:31453,y:32545,varname:node_3290,prsc:2|A-4996-OUT,B-7792-OUT,C-829-OUT;n:type:ShaderForge.SFN_Vector1,id:7509,x:34012,y:33198,varname:node_7509,prsc:2,v1:0;n:type:ShaderForge.SFN_Slider,id:1024,x:30566,y:32258,ptovrint:False,ptlb:BlueValueMask1,ptin:_BlueValueMask1,cmnt:Mask 1,varname:_Mask1RedValue_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:8282,x:30370,y:32254,ptovrint:False,ptlb:BlueColorMask1,ptin:_BlueColorMask1,varname:_Mask1RedColor_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.04827571,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:572,x:30566,y:32321,varname:node_572,prsc:2|A-8282-RGB,B-9931-B;n:type:ShaderForge.SFN_Multiply,id:829,x:31204,y:32304,varname:node_829,prsc:2|A-572-OUT,B-9171-OUT;n:type:ShaderForge.SFN_Lerp,id:9171,x:30998,y:32350,varname:node_9171,prsc:2|A-9931-B,B-4573-RGB,T-1024-OUT;n:type:ShaderForge.SFN_Add,id:7059,x:31768,y:31782,varname:node_7059,prsc:2|A-8108-OUT,B-3343-OUT,C-3290-OUT;n:type:ShaderForge.SFN_Tex2d,id:4943,x:30222,y:31862,ptovrint:False,ptlb:Mask2,ptin:_Mask2,varname:_Mask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:441,x:30555,y:31730,ptovrint:False,ptlb:RedValueMask2,ptin:_RedValueMask2,varname:_RedValueMask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:736,x:30360,y:31727,ptovrint:False,ptlb:RedColorMask2,ptin:_RedColorMask2,varname:_RedColorMask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:2178,x:30555,y:31793,varname:node_2178,prsc:2|A-736-RGB,B-4943-R;n:type:ShaderForge.SFN_Multiply,id:9176,x:31193,y:31776,varname:node_9176,prsc:2|A-2178-OUT,B-3685-OUT;n:type:ShaderForge.SFN_Lerp,id:3685,x:30987,y:31776,varname:node_3685,prsc:2|A-4943-R,B-4573-RGB,T-441-OUT;n:type:ShaderForge.SFN_Multiply,id:1603,x:30561,y:31982,varname:node_1603,prsc:2|A-4943-G,B-308-RGB;n:type:ShaderForge.SFN_Lerp,id:2003,x:30987,y:31907,varname:node_2003,prsc:2|A-4943-G,B-4573-RGB,T-6340-OUT;n:type:ShaderForge.SFN_Multiply,id:9105,x:31193,y:31907,varname:node_9105,prsc:2|A-1603-OUT,B-2003-OUT;n:type:ShaderForge.SFN_Add,id:3343,x:31442,y:31798,varname:node_3343,prsc:2|A-9176-OUT,B-9105-OUT,C-3103-OUT;n:type:ShaderForge.SFN_Slider,id:373,x:30555,y:31511,ptovrint:False,ptlb:BlueValueMask2,ptin:_BlueValueMask2,cmnt:Mask 1,varname:_BlueValueMask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:5830,x:30359,y:31507,ptovrint:False,ptlb:BlueColorMask2,ptin:_BlueColorMask2,varname:_BlueColorMask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.04827571,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:7216,x:30555,y:31574,varname:node_7216,prsc:2|A-5830-RGB,B-4943-B;n:type:ShaderForge.SFN_Multiply,id:3103,x:31193,y:31557,varname:node_3103,prsc:2|A-7216-OUT,B-7528-OUT;n:type:ShaderForge.SFN_Lerp,id:7528,x:30987,y:31603,varname:node_7528,prsc:2|A-4943-B,B-4573-RGB,T-373-OUT;n:type:ShaderForge.SFN_Color,id:308,x:30369,y:32038,ptovrint:False,ptlb:GreenColorMask2,ptin:_GreenColorMask2,varname:_GreenColorMask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1999999,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Slider,id:6340,x:30561,y:32131,ptovrint:False,ptlb:GreenValueMask2,ptin:_GreenValueMask2,varname:_GreenValueMask2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:9391,x:30224,y:31139,ptovrint:False,ptlb:Mask3,ptin:_Mask3,varname:_Mask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:True,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:5685,x:30557,y:31007,ptovrint:False,ptlb:RedValueMask3,ptin:_RedValueMask3,varname:_RedValueMask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Color,id:1706,x:30362,y:31004,ptovrint:False,ptlb:RedColorMask3,ptin:_RedColorMask3,varname:_RedColorMask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:7461,x:30557,y:31070,varname:node_7461,prsc:2|A-1706-RGB,B-9391-R;n:type:ShaderForge.SFN_Multiply,id:6944,x:31195,y:31053,varname:node_6944,prsc:2|A-7461-OUT,B-4275-OUT;n:type:ShaderForge.SFN_Lerp,id:4275,x:30989,y:31053,varname:node_4275,prsc:2|A-9391-R,B-4573-RGB,T-5685-OUT;n:type:ShaderForge.SFN_Multiply,id:4992,x:30563,y:31259,varname:node_4992,prsc:2|A-9391-G,B-2303-RGB;n:type:ShaderForge.SFN_Lerp,id:1085,x:30989,y:31184,varname:node_1085,prsc:2|A-9391-G,B-4573-RGB,T-4890-OUT;n:type:ShaderForge.SFN_Multiply,id:5753,x:31195,y:31184,varname:node_5753,prsc:2|A-4992-OUT,B-1085-OUT;n:type:ShaderForge.SFN_Add,id:8108,x:31444,y:31075,varname:node_8108,prsc:2|A-6944-OUT,B-5753-OUT,C-370-OUT;n:type:ShaderForge.SFN_Slider,id:7597,x:30557,y:30788,ptovrint:False,ptlb:BlueValueMask3,ptin:_BlueValueMask3,cmnt:Mask 1,varname:_BlueValueMask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:2627,x:30361,y:30784,ptovrint:False,ptlb:BlueColorMask3,ptin:_BlueColorMask3,varname:_BlueColorMask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.04827571,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:8929,x:30557,y:30851,varname:node_8929,prsc:2|A-2627-RGB,B-9391-B;n:type:ShaderForge.SFN_Multiply,id:370,x:31195,y:30834,varname:node_370,prsc:2|A-8929-OUT,B-4578-OUT;n:type:ShaderForge.SFN_Lerp,id:4578,x:30989,y:30880,varname:node_4578,prsc:2|A-9391-B,B-4573-RGB,T-7597-OUT;n:type:ShaderForge.SFN_Color,id:2303,x:30371,y:31315,ptovrint:False,ptlb:GreenColorMask3,ptin:_GreenColorMask3,varname:_GreenColorMask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1999999,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Slider,id:4890,x:30563,y:31408,ptovrint:False,ptlb:GreenValueMask3,ptin:_GreenValueMask3,varname:_GreenValueMask3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;proporder:4573-5964-358-7542-1813-2090-7323-9110-1457-5925-9103-1199-6089-3377-1296-7-9931-9927-9404-6988-834-8282-1024-4943-736-441-5830-373-308-6340-9391-1706-5685-2627-7597-2303-4890;pass:END;sub:END;*/

Shader "Shader Forge/UnitShaderMasked" {
    Properties {
        [NoScaleOffset]_Albedo ("Albedo", 2D) = "white" {}
        [NoScaleOffset]_BumpMap ("Normal Map", 2D) = "black" {}
        _Gloss ("Gloss", Range(0, 1)) = 1
        [NoScaleOffset]_Metallic ("Metallic", 2D) = "black" {}
        _MetallicValue ("MetallicValue", Range(0, 1)) = 0
        [NoScaleOffset]_Occlusion ("Occlusion", 2D) = "black" {}
        [HideInInspector][NoScaleOffset]_Dissolve_Pattern ("Dissolve_Pattern", 2D) = "white" {}
        _Dissolve_Color ("Dissolve_Color", Color) = (1,0,0,1)
        _Dissolve_Amount ("Dissolve_Amount", Range(0, 1)) = 0
        [HideInInspector][NoScaleOffset]_Noise ("Noise", 2D) = "white" {}
        _Outline_Color ("Outline_Color", Color) = (1,1,1,1)
        _OutlineOn ("OutlineOn", Float ) = 0
        _Outline_Width ("Outline_Width", Float ) = 0.01
        [NoScaleOffset]_Emission ("Emission", 2D) = "black" {}
        _EmissionValue ("EmissionValue", Float ) = 3
        [HDR]_EmissionColor ("EmissionColor", Color) = (0,0,0,1)
        _Mask1 ("Mask1", 2D) = "white" {}
        _RedColorMask1 ("RedColorMask1", Color) = (1,0,0,1)
        _RedValueMask1 ("RedValueMask1", Range(0, 1)) = 0
        _GreenColorMask1 ("GreenColorMask1", Color) = (0.1999999,1,0,1)
        _GreenValueMask1 ("GreenValueMask1", Range(0, 1)) = 0
        _BlueColorMask1 ("BlueColorMask1", Color) = (0,0.04827571,1,1)
        _BlueValueMask1 ("BlueValueMask1", Range(0, 1)) = 0
        [NoScaleOffset]_Mask2 ("Mask2", 2D) = "white" {}
        _RedColorMask2 ("RedColorMask2", Color) = (1,0,0,1)
        _RedValueMask2 ("RedValueMask2", Range(0, 1)) = 0
        _BlueColorMask2 ("BlueColorMask2", Color) = (0,0.04827571,1,1)
        _BlueValueMask2 ("BlueValueMask2", Range(0, 1)) = 0
        _GreenColorMask2 ("GreenColorMask2", Color) = (0.1999999,1,0,1)
        _GreenValueMask2 ("GreenValueMask2", Range(0, 1)) = 0
        [NoScaleOffset]_Mask3 ("Mask3", 2D) = "white" {}
        _RedColorMask3 ("RedColorMask3", Color) = (1,0,0,1)
        _RedValueMask3 ("RedValueMask3", Range(0, 1)) = 1
        _BlueColorMask3 ("BlueColorMask3", Color) = (0,0.04827571,1,1)
        _BlueValueMask3 ("BlueValueMask3", Range(0, 1)) = 0
        _GreenColorMask3 ("GreenColorMask3", Color) = (0.1999999,1,0,1)
        _GreenValueMask3 ("GreenValueMask3", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _Noise;
            uniform float _Dissolve_Amount;
            uniform float4 _Outline_Color;
            uniform float _OutlineOn;
            uniform float _Outline_Width;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float node_2223 = 0.0;
                float node_3730_if_leA = step(_Dissolve_Amount,node_2223);
                float node_3730_if_leB = step(node_2223,_Dissolve_Amount);
                float node_8128_if_leA = step(_OutlineOn,0.5);
                float node_8128_if_leB = step(0.5,_OutlineOn);
                float node_8128 = lerp((node_8128_if_leA*node_2223)+(node_8128_if_leB*_Outline_Width),_Outline_Width,node_8128_if_leA*node_8128_if_leB);
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*lerp((node_3730_if_leA*node_8128)+(node_3730_if_leB*node_2223),node_8128,node_3730_if_leA*node_3730_if_leB),1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _Noise_var = tex2D(_Noise,i.uv0);
                float node_5887 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_5887 - 0.5);
                return fixed4(_Outline_Color.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _BumpMap;
            uniform float _Gloss;
            uniform float _MetallicValue;
            uniform float4 _Dissolve_Color;
            uniform sampler2D _Dissolve_Pattern;
            uniform sampler2D _Noise;
            uniform float _Dissolve_Amount;
            uniform sampler2D _Emission;
            uniform sampler2D _Metallic;
            uniform sampler2D _Occlusion;
            uniform float4 _EmissionColor;
            uniform float _EmissionValue;
            uniform sampler2D _Albedo;
            uniform sampler2D _Mask1; uniform float4 _Mask1_ST;
            uniform float _RedValueMask1;
            uniform float4 _RedColorMask1;
            uniform float4 _GreenColorMask1;
            uniform float _GreenValueMask1;
            uniform float _BlueValueMask1;
            uniform float4 _BlueColorMask1;
            uniform sampler2D _Mask2;
            uniform float _RedValueMask2;
            uniform float4 _RedColorMask2;
            uniform float _BlueValueMask2;
            uniform float4 _BlueColorMask2;
            uniform float4 _GreenColorMask2;
            uniform float _GreenValueMask2;
            uniform sampler2D _Mask3;
            uniform float _RedValueMask3;
            uniform float4 _RedColorMask3;
            uniform float _BlueValueMask3;
            uniform float4 _BlueColorMask3;
            uniform float4 _GreenColorMask3;
            uniform float _GreenValueMask3;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _BumpMap_var = tex2D(_BumpMap,i.uv0);
                float3 normalDirection = _BumpMap_var.rgb;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 _Noise_var = tex2D(_Noise,i.uv0);
                float node_5887 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_5887 - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 1.0 - _Gloss; // Convert roughness to gloss
                float perceptualRoughness = _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _Occlusion_var = tex2D(_Occlusion,i.uv0);
                float node_4887 = _Occlusion_var.rgb.r;
                float3 specularAO = node_4887;
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float4 _Metallic_var = tex2D(_Metallic,i.uv0);
                float node_9672 = (_Metallic_var.rgb.r*_MetallicValue);
                float3 specularColor = float3(node_9672,node_9672,node_9672);
                float specularMonochrome;
                float4 _Mask3_var = tex2D(_Mask3,i.uv0);
                float4 _Albedo_var = tex2D(_Albedo,i.uv0);
                float4 _Mask2_var = tex2D(_Mask2,i.uv0);
                float4 _Mask1_var = tex2D(_Mask1,TRANSFORM_TEX(i.uv0, _Mask1));
                float3 diffuseColor = ((((_RedColorMask3.rgb*_Mask3_var.r)*lerp(float3(_Mask3_var.r,_Mask3_var.r,_Mask3_var.r),_Albedo_var.rgb,_RedValueMask3))+((_Mask3_var.g*_GreenColorMask3.rgb)*lerp(float3(_Mask3_var.g,_Mask3_var.g,_Mask3_var.g),_Albedo_var.rgb,_GreenValueMask3))+((_BlueColorMask3.rgb*_Mask3_var.b)*lerp(float3(_Mask3_var.b,_Mask3_var.b,_Mask3_var.b),_Albedo_var.rgb,_BlueValueMask3)))+(((_RedColorMask2.rgb*_Mask2_var.r)*lerp(float3(_Mask2_var.r,_Mask2_var.r,_Mask2_var.r),_Albedo_var.rgb,_RedValueMask2))+((_Mask2_var.g*_GreenColorMask2.rgb)*lerp(float3(_Mask2_var.g,_Mask2_var.g,_Mask2_var.g),_Albedo_var.rgb,_GreenValueMask2))+((_BlueColorMask2.rgb*_Mask2_var.b)*lerp(float3(_Mask2_var.b,_Mask2_var.b,_Mask2_var.b),_Albedo_var.rgb,_BlueValueMask2)))+(((_RedColorMask1.rgb*_Mask1_var.r)*lerp(float3(_Mask1_var.r,_Mask1_var.r,_Mask1_var.r),_Albedo_var.rgb,_RedValueMask1))+((_Mask1_var.g*_GreenColorMask1.rgb)*lerp(float3(_Mask1_var.g,_Mask1_var.g,_Mask1_var.g),_Albedo_var.rgb,_GreenValueMask1))+((_BlueColorMask1.rgb*_Mask1_var.b)*lerp(float3(_Mask1_var.b,_Mask1_var.b,_Mask1_var.b),_Albedo_var.rgb,_BlueValueMask1)))); // Need this for specular when using metallic
                diffuseColor = EnergyConservationBetweenDiffuseAndSpecular(diffuseColor, specularColor, specularMonochrome);
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular) * specularAO;
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                indirectDiffuse *= node_4887; // Diffuse AO
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float2 node_5383 = float2((1.0 - saturate((node_5887*8.0+-4.0))),0.0);
                float4 node_7776 = tex2D(_Dissolve_Pattern,node_5383);
                float4 _Emission_var = tex2D(_Emission,i.uv0);
                float3 emissive = ((_Dissolve_Color.rgb*node_7776.rgb.b)+(_EmissionValue*_Emission_var.rgb*_EmissionColor.rgb));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _BumpMap;
            uniform float _Gloss;
            uniform float _MetallicValue;
            uniform float4 _Dissolve_Color;
            uniform sampler2D _Dissolve_Pattern;
            uniform sampler2D _Noise;
            uniform float _Dissolve_Amount;
            uniform sampler2D _Emission;
            uniform sampler2D _Metallic;
            uniform float4 _EmissionColor;
            uniform float _EmissionValue;
            uniform sampler2D _Albedo;
            uniform sampler2D _Mask1; uniform float4 _Mask1_ST;
            uniform float _RedValueMask1;
            uniform float4 _RedColorMask1;
            uniform float4 _GreenColorMask1;
            uniform float _GreenValueMask1;
            uniform float _BlueValueMask1;
            uniform float4 _BlueColorMask1;
            uniform sampler2D _Mask2;
            uniform float _RedValueMask2;
            uniform float4 _RedColorMask2;
            uniform float _BlueValueMask2;
            uniform float4 _BlueColorMask2;
            uniform float4 _GreenColorMask2;
            uniform float _GreenValueMask2;
            uniform sampler2D _Mask3;
            uniform float _RedValueMask3;
            uniform float4 _RedColorMask3;
            uniform float _BlueValueMask3;
            uniform float4 _BlueColorMask3;
            uniform float4 _GreenColorMask3;
            uniform float _GreenValueMask3;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _BumpMap_var = tex2D(_BumpMap,i.uv0);
                float3 normalDirection = _BumpMap_var.rgb;
                float4 _Noise_var = tex2D(_Noise,i.uv0);
                float node_5887 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_5887 - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 1.0 - _Gloss; // Convert roughness to gloss
                float perceptualRoughness = _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float4 _Metallic_var = tex2D(_Metallic,i.uv0);
                float node_9672 = (_Metallic_var.rgb.r*_MetallicValue);
                float3 specularColor = float3(node_9672,node_9672,node_9672);
                float specularMonochrome;
                float4 _Mask3_var = tex2D(_Mask3,i.uv0);
                float4 _Albedo_var = tex2D(_Albedo,i.uv0);
                float4 _Mask2_var = tex2D(_Mask2,i.uv0);
                float4 _Mask1_var = tex2D(_Mask1,TRANSFORM_TEX(i.uv0, _Mask1));
                float3 diffuseColor = ((((_RedColorMask3.rgb*_Mask3_var.r)*lerp(float3(_Mask3_var.r,_Mask3_var.r,_Mask3_var.r),_Albedo_var.rgb,_RedValueMask3))+((_Mask3_var.g*_GreenColorMask3.rgb)*lerp(float3(_Mask3_var.g,_Mask3_var.g,_Mask3_var.g),_Albedo_var.rgb,_GreenValueMask3))+((_BlueColorMask3.rgb*_Mask3_var.b)*lerp(float3(_Mask3_var.b,_Mask3_var.b,_Mask3_var.b),_Albedo_var.rgb,_BlueValueMask3)))+(((_RedColorMask2.rgb*_Mask2_var.r)*lerp(float3(_Mask2_var.r,_Mask2_var.r,_Mask2_var.r),_Albedo_var.rgb,_RedValueMask2))+((_Mask2_var.g*_GreenColorMask2.rgb)*lerp(float3(_Mask2_var.g,_Mask2_var.g,_Mask2_var.g),_Albedo_var.rgb,_GreenValueMask2))+((_BlueColorMask2.rgb*_Mask2_var.b)*lerp(float3(_Mask2_var.b,_Mask2_var.b,_Mask2_var.b),_Albedo_var.rgb,_BlueValueMask2)))+(((_RedColorMask1.rgb*_Mask1_var.r)*lerp(float3(_Mask1_var.r,_Mask1_var.r,_Mask1_var.r),_Albedo_var.rgb,_RedValueMask1))+((_Mask1_var.g*_GreenColorMask1.rgb)*lerp(float3(_Mask1_var.g,_Mask1_var.g,_Mask1_var.g),_Albedo_var.rgb,_GreenValueMask1))+((_BlueColorMask1.rgb*_Mask1_var.b)*lerp(float3(_Mask1_var.b,_Mask1_var.b,_Mask1_var.b),_Albedo_var.rgb,_BlueValueMask1)))); // Need this for specular when using metallic
                diffuseColor = EnergyConservationBetweenDiffuseAndSpecular(diffuseColor, specularColor, specularMonochrome);
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _Noise;
            uniform float _Dissolve_Amount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float2 uv1 : TEXCOORD2;
                float2 uv2 : TEXCOORD3;
                float4 posWorld : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 _Noise_var = tex2D(_Noise,i.uv0);
                float node_5887 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                clip(node_5887 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Gloss;
            uniform float _MetallicValue;
            uniform float4 _Dissolve_Color;
            uniform sampler2D _Dissolve_Pattern;
            uniform sampler2D _Noise;
            uniform float _Dissolve_Amount;
            uniform sampler2D _Emission;
            uniform sampler2D _Metallic;
            uniform float4 _EmissionColor;
            uniform float _EmissionValue;
            uniform sampler2D _Albedo;
            uniform sampler2D _Mask1; uniform float4 _Mask1_ST;
            uniform float _RedValueMask1;
            uniform float4 _RedColorMask1;
            uniform float4 _GreenColorMask1;
            uniform float _GreenValueMask1;
            uniform float _BlueValueMask1;
            uniform float4 _BlueColorMask1;
            uniform sampler2D _Mask2;
            uniform float _RedValueMask2;
            uniform float4 _RedColorMask2;
            uniform float _BlueValueMask2;
            uniform float4 _BlueColorMask2;
            uniform float4 _GreenColorMask2;
            uniform float _GreenValueMask2;
            uniform sampler2D _Mask3;
            uniform float _RedValueMask3;
            uniform float4 _RedColorMask3;
            uniform float _BlueValueMask3;
            uniform float4 _BlueColorMask3;
            uniform float4 _GreenColorMask3;
            uniform float _GreenValueMask3;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _Noise_var = tex2D(_Noise,i.uv0);
                float node_5887 = (_Noise_var.r+((1.0 - _Dissolve_Amount)*1.5+-0.9));
                float2 node_5383 = float2((1.0 - saturate((node_5887*8.0+-4.0))),0.0);
                float4 node_7776 = tex2D(_Dissolve_Pattern,node_5383);
                float4 _Emission_var = tex2D(_Emission,i.uv0);
                o.Emission = ((_Dissolve_Color.rgb*node_7776.rgb.b)+(_EmissionValue*_Emission_var.rgb*_EmissionColor.rgb));
                
                float4 _Mask3_var = tex2D(_Mask3,i.uv0);
                float4 _Albedo_var = tex2D(_Albedo,i.uv0);
                float4 _Mask2_var = tex2D(_Mask2,i.uv0);
                float4 _Mask1_var = tex2D(_Mask1,TRANSFORM_TEX(i.uv0, _Mask1));
                float3 diffColor = ((((_RedColorMask3.rgb*_Mask3_var.r)*lerp(float3(_Mask3_var.r,_Mask3_var.r,_Mask3_var.r),_Albedo_var.rgb,_RedValueMask3))+((_Mask3_var.g*_GreenColorMask3.rgb)*lerp(float3(_Mask3_var.g,_Mask3_var.g,_Mask3_var.g),_Albedo_var.rgb,_GreenValueMask3))+((_BlueColorMask3.rgb*_Mask3_var.b)*lerp(float3(_Mask3_var.b,_Mask3_var.b,_Mask3_var.b),_Albedo_var.rgb,_BlueValueMask3)))+(((_RedColorMask2.rgb*_Mask2_var.r)*lerp(float3(_Mask2_var.r,_Mask2_var.r,_Mask2_var.r),_Albedo_var.rgb,_RedValueMask2))+((_Mask2_var.g*_GreenColorMask2.rgb)*lerp(float3(_Mask2_var.g,_Mask2_var.g,_Mask2_var.g),_Albedo_var.rgb,_GreenValueMask2))+((_BlueColorMask2.rgb*_Mask2_var.b)*lerp(float3(_Mask2_var.b,_Mask2_var.b,_Mask2_var.b),_Albedo_var.rgb,_BlueValueMask2)))+(((_RedColorMask1.rgb*_Mask1_var.r)*lerp(float3(_Mask1_var.r,_Mask1_var.r,_Mask1_var.r),_Albedo_var.rgb,_RedValueMask1))+((_Mask1_var.g*_GreenColorMask1.rgb)*lerp(float3(_Mask1_var.g,_Mask1_var.g,_Mask1_var.g),_Albedo_var.rgb,_GreenValueMask1))+((_BlueColorMask1.rgb*_Mask1_var.b)*lerp(float3(_Mask1_var.b,_Mask1_var.b,_Mask1_var.b),_Albedo_var.rgb,_BlueValueMask1))));
                float4 _Metallic_var = tex2D(_Metallic,i.uv0);
                float node_9672 = (_Metallic_var.rgb.r*_MetallicValue);
                float3 specColor = float3(node_9672,node_9672,node_9672);
                float specularMonochrome = max(max(specColor.r, specColor.g),specColor.b);
                diffColor *= (1.0-specularMonochrome);
                float roughness = _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
