﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputLocker : MonoBehaviour {

    public void DisableInputLock()
    {
        FindObjectOfType<UnitManager>().DisableInputLock();
    }
}
